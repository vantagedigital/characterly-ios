//
//  ManageClassViewController.m
//  Character.ly
//
//  Created by Adeel Nasir on 08/09/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "ManageClassViewController.h"
#import "NavViewController.h"
#import "MessageWindow.h"
#import "ManageViewController.h"
#import "ClassroomHomeViewController.h"

@interface ManageClassViewController (){
    NSMutableArray *loginResponse;
    NSMutableArray *classCodes;
    UIView *dimView;
    UIActivityIndicatorView *spinner;
}

@property (assign, nonatomic) BOOL isEducator;
@property (strong, nonatomic) NSString *mId;
@property (strong, nonatomic) NSArray *data;
@property (strong, nonatomic) NSArray *classImagePathList;
@property (weak, nonatomic) IBOutlet UILabel *fullNameText;
@property (weak, nonatomic) IBOutlet UILabel *titleText;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addClassHeightConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *classTableHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containViewHeight;
@property (assign, nonatomic) NSInteger currentSelectedClass;

@end

@implementation ManageClassViewController
#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    
    NavViewController *myViewController2 = [[NavViewController alloc] initForAutoLayout];
    [self.view addSubview:myViewController2];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:[UIApplication sharedApplication].statusBarFrame.size.height];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
    [myViewController2 autoSetDimension:ALDimensionHeight toSize:200];
    myViewController2.parent = self;
    
    // Do any additional setup after loading the view.
    _imageButton.layer.borderColor = [UIColor blackColor].CGColor;
    _imageButton.layer.borderWidth = 0.5f;
    
    //_containerView.layer.borderColor = [UIColor blackColor].CGColor;
    //_containerView.layer.borderWidth = 0.5f;
    
//    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 700, 0);
    
    [_tableView setShowsHorizontalScrollIndicator:NO];
    [_tableView setShowsVerticalScrollIndicator:NO];
    _btnAddClass.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _btnAddClass.layer.borderWidth = 1.0f;
    
    [self.tableView setSeparatorColor:[UIColor clearColor]];
    
    NSDictionary *userInfo = [[UserCache sharedInstance] getUserInfo];
    
    self.isEducator = ![userInfo[@"educator"] isKindOfClass:[NSNull class]];
    if (self.isEducator) {
        self.btnAddClass.hidden = NO;
        self.data = userInfo[@"educator"][@"classrooms"];
    } else {
        self.btnAddClass.hidden = YES;
        self.data = userInfo[@"student"][@"classrooms"];
    }
    NSLog(@"classrooms data: %@", self.data);
    
    NSString *fullNameText = concat(userInfo[@"name"], @" ");
    fullNameText = concat(fullNameText, userInfo[@"surname"]);
    self.fullNameText.text = fullNameText;
    
    if (self.isEducator) {
        self.titleText.text = @"Teacher";
        self.mId = userInfo[@"educator"][@"id"];
    } else {
        self.titleText.text = @"Student";
        self.mId = userInfo[@"student"][@"id"];
    }
    
    if(![userInfo[@"imagePath"] isKindOfClass:[NSNull class]]){
        [self.userImage sd_setImageWithURL:userInfo[@"imagePath"]];
    }
    
    if(self.data.count == 0){
        self.classTableHeight.constant = 500;
    }else{
        self.classTableHeight.constant = 350 + self.data.count * 330;
    }
    
    self.containViewHeight.constant = self.classTableHeight.constant + 100;
}

- (void)updateUI {
    if (self.isEducator) {
        
        self.data = [[UserCache sharedInstance] getUserInfo][@"educator"][@"classrooms"];
        NSLog(@"classrooms data: %@", self.data);
    } else {
        self.data = [[UserCache sharedInstance] getUserInfo][@"student"][@"classrooms"];
        NSLog(@"classrooms data: %@", self.data);
    }
    
    [self.tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
//    [self updateClassroomIndex];
}

- (void)updateClassroomIndex {
    NSString *url = self.isEducator ? @"services/app/Classroom/GetByEducatorId?id=" : @"services/app/Classroom/GetByStudentId?id=";
    url = concat(url, self.mId);
    
    [NetworkHelper placeGetRequest:nil relativeUrl:url withSuccess:^(NSDictionary *jsonObject) {
        // cache data
        [self updateCache:jsonObject[@"result"]];
        [self updateUI];
    } andFailure:^(NSString *string) {
        NSLog(@"updateClassroomIndex Failure:%@", string);
        //               [self showErrorMessage:string];
    }];
}


- (void)updateCache:(NSArray *)result {
    NSMutableDictionary *userInfo = [[UserCache sharedInstance] getUserInfo].mutableCopy;
    if (self.isEducator) {
        NSMutableDictionary *dic = ((NSDictionary *)userInfo[@"educator"]).mutableCopy;
        [dic setObject:result forKey:@"classrooms"];
        [userInfo setObject:dic forKey:@"educator"];
        
    } else {
        NSMutableDictionary *dic = ((NSDictionary *)userInfo[@"student"]).mutableCopy;
        [dic setObject:result forKey:@"classrooms"];
        [userInfo setObject:dic forKey:@"student"];
    }
    [[UserCache sharedInstance] setCurrentUser:userInfo];
}

- (void)setClassroomImage:(NSString *)url imageView:(UIImageView *)classroomImageView number:(NSInteger) number {
    
    [NetworkHelper placeGetRequest:nil relativeUrl:url withSuccess:^(NSDictionary *jsonObject) {
        // cache data
        NSLog(@"class image data: %@", jsonObject);
        [classroomImageView sd_setImageWithURL: jsonObject[@"result"][number][@"path"]];
    } andFailure:^(NSString *string) {
        NSLog(@"updateClassroomIndex Failure:%@", string);
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 330;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.data count];
}
-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *) cell forRowAtIndexPath:(NSIndexPath *)indexPath // replace "postTableViewCell" with your cell
{
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;
    UITableViewCell *cell;
    
    CellIdentifier = @"Cell1";
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    UILabel *classCode = (UILabel *)[cell viewWithTag:700];
    UILabel *classTitle = (UILabel *)[cell viewWithTag:701];
    UILabel *gradeName = (UILabel *)[cell viewWithTag:702];
    UILabel *studentsCount = (UILabel *)[cell viewWithTag:703];
    
    UIImageView *classroomImage = (UIImageView *)[cell viewWithTag:704];

    NSString *url = @"services/app/FileAttachment/GetByEntityId?id=";
    url = concat(url, self.data[indexPath.row][@"id"]);

//    [self setClassroomImage: url imageView: classroomImage number: indexPath.row];
    
    classCode.text = self.data[indexPath.row][@"classCode"];
    classTitle.text = self.data[indexPath.row][@"name"];
    NSNull *nullGrade = [[NSNull alloc] init];
    if(self.data[indexPath.row][@"grade"] == nullGrade){
        
    }else{
        gradeName.text = self.data[indexPath.row][@"grade"][@"name"];
    }
    studentsCount.text = @"Students:";
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = [indexPath row];
    self.currentSelectedClass = row;
    
    [self performSegueWithIdentifier:@"classhome" sender:self];
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"classhome"])
    {
        // Get reference to the destination view controller
        ClassroomHomeViewController *vc = [segue destinationViewController];
        vc.data = self.data[self.currentSelectedClass];
    }
}
- (IBAction)addClassTap:(id)sender {
    // TODO enable after fixing add class vc.
    [self performSegueWithIdentifier:@"addclass" sender:self];
}

@end
