//
//  ClassroomHomeViewController.m
//  Character.ly
//
//  Created by Adeel Nasir on 08/09/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "ClassroomHomeViewController.h"
#import "NavViewController.h"
#import "ManageViewController.h"
#import "ClassTimelineViewController.h"
#import "AddStudentViewController.h"
#import "ClassroomInviteViewController.h"
#import "StudentEvaluationsViewController.h"
#import "DGActivityIndicatorView.h"
#import "DropDownCell.h"

@interface ClassroomHomeViewController ()<UITextFieldDelegate> {
    UIView *dimView;
    DGActivityIndicatorView *activityIndicatorView;
    int i;
}
@property (weak, nonatomic) IBOutlet UILabel *classCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *classNameLabel;
@property (strong, nonatomic) NSMutableArray *students, *evaluations;
@property (strong, nonatomic) NSDictionary *selectedStudent;
@property (assign, nonatomic) NSInteger currentSortSelection;
@property (strong, nonatomic) NSMutableArray <NSString *> *sortOrders;
@property (weak, nonatomic) IBOutlet UITextField *txtSort;
@property (weak, nonatomic) IBOutlet UITextField *txtSearch;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *studentTableHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeightConstraint;

@end

@implementation ClassroomHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.students = [NSMutableArray array];
    self.evaluations = [NSMutableArray array];
    
    [self.selectSortButton.layer setBorderWidth: 0.5];
    [self.selectSortButton.layer setBorderColor:[[UIColor grayColor] CGColor]];
    
    self.sortTable.delegate = self;
    self.sortTable.dataSource = self;
    self.sortTable.hidden = YES;
    self.sortTable.editing = NO;
    
    self.txtSearch.placeholder = @"Students";
    
    self.sortOrders = @[@"Descending",
                        @"Ascending"
                        ].mutableCopy;
    
    self.txtSort.text = self.sortOrders[self.currentSortSelection];
    
    
    [self getClassroomStudents];
    [self getClassroomEvaluations];
    
    
    self.navigationController.navigationBar.hidden = YES;
    
    NavViewController *myViewController2 = [[NavViewController alloc] initForAutoLayout];
    [self.view addSubview:myViewController2];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:[UIApplication sharedApplication].statusBarFrame.size.height];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
    [myViewController2 autoSetDimension:ALDimensionHeight toSize:200];
    myViewController2.parent = self;
    
    [_scroller setScrollEnabled:YES];
    [_scroller setContentSize:CGSizeMake(375, 1400)];
    i = 0;
    // Do any additional setup after loading the view.
//    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 1100, 0);
     self.tableView.separatorColor = [UIColor clearColor];
    
    UIView *bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, _btnStudent.frame.size.height - 1.0f, _btnStudent.frame.size.width, 1)];
    bottomBorder.backgroundColor = [UIColor orangeColor];
    
    _sortField.hidden = YES;
    
    [_btnStudent addSubview:bottomBorder];
    
    [self.tableView setSeparatorColor:[UIColor clearColor]];
    
    
    [_tableView setShowsHorizontalScrollIndicator:NO];
    [_tableView setShowsVerticalScrollIndicator:NO];

    
    
    self.classCodeLabel.text = self.data[@"classCode"];
    self.classNameLabel.text = self.data[@"name"];
     
}

- (void)getClassroomStudents {
    NSString *url = concat(@"services/app/Student/GetByClassroomId?id=", self.data[@"id"]);
    [NetworkHelper placeGetRequest:nil relativeUrl:url withSuccess:^(NSDictionary *jsonObject) {
        [self.students removeAllObjects];
        [self.students addObjectsFromArray:jsonObject[@"result"]];
        [self.tableView reloadData];
        
    } andFailure:^(NSString *string) {
        NSLog(@"getStudents Failure:%@", string);
        //               [self showErrorMessage:string];
    }];
}

- (void)getClassroomEvaluations {
    NSString *url = concat(@"services/app/Evaluation/GetByClassroomId?id=", self.data[@"id"]);
    [NetworkHelper placeGetRequest:nil relativeUrl:url withSuccess:^(NSDictionary *jsonObject) {
        [self.evaluations removeAllObjects];
        [self.evaluations addObjectsFromArray:jsonObject[@"result"]];
        [self.tableView reloadData];
        
    } andFailure:^(NSString *string) {
        NSLog(@"getStudents Failure:%@", string);
        //               [self showErrorMessage:string];
    }];
}

- (void)showLoader {
    if (!dimView) {
        dimView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
        dimView.backgroundColor = [UIColor blackColor];
        dimView.alpha = 0.7f;
        dimView.userInteractionEnabled = NO;
    }
    [self.view addSubview:dimView];
    CGRect newFrame = dimView.frame;
    
    newFrame.size.width = 1200;
    newFrame.size.height = 1500;
    [dimView setFrame:newFrame];
    [dimView setCenter:CGPointMake(187.0f, 333.0f)];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeLineScale tintColor:[UIColor whiteColor] size:40.0f];
    activityIndicatorView.center = self.view.center;
    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
}

- (void)showErrorMessage:(NSString *)string {
    if (!string) {
        string = @"";
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Character.ly"
                                                    message:string
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)hideLoader {
    [dimView removeFromSuperview];
    [activityIndicatorView stopAnimating];
}

- (void)getProfileImage:(NSDictionary *)studentData imageView:(UIImageView *)profileImage {
    
    NSNull *nullImage = [[NSNull alloc] init];
    if(studentData[@"profilePicture"] == nullImage){
        NSLog(@"There is no profile image.");
    }else{
        if(studentData[@"profilePicture"]){
            NSLog(@"profile image path: %@", studentData[@"profilePicture"][@"path"]);
            
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:studentData[@"profilePicture"][@"path"]]];
                if(data == nil)
                    return;
                dispatch_async(dispatch_get_main_queue(), ^{
                    profileImage.image = [UIImage imageWithData:data];
                });
            });
        }
    }
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(tableView == _tableView){
        
        NSInteger count = self.students.count;
        if (i == 1) {
            count = self.evaluations.count;
        }
        return count + 2;
    }else if(tableView == _sortTable){
        return self.sortOrders.count;
    }else{
        return 0;
    }
}
-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *) cell forRowAtIndexPath:(NSIndexPath *)indexPath // replace "postTableViewCell" with your cell
{
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.studentTableHeightConstraint.constant = 900 + self.students.count * 134;
    self.mainViewHeightConstraint.constant = self.studentTableHeightConstraint.constant + 700;
    
    if(tableView == _tableView){
        
        NSInteger count = self.students.count;
        if (i == 1) {
            count = self.evaluations.count;
        }
        static NSString *CellIdentifier;
        UITableViewCell *cell;
        if(indexPath.row < count){
            if (i==0)
            {
                CellIdentifier = @"Cell";
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                
                UIImageView *profileImage = (UIImageView *)[cell viewWithTag:899];
                UILabel *firstNameLabel = (UILabel *)[cell viewWithTag:900];
                UILabel *lastNameLabel = (UILabel *)[cell viewWithTag:901];
                
                firstNameLabel.text = self.students[indexPath.row][@"name"];
                lastNameLabel.text = self.students[indexPath.row][@"surname"];
                [self getProfileImage:self.students[indexPath.row] imageView:profileImage];
                
            }
            else if (i==1)
            {
                CellIdentifier = @"Cell1";
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                
                UILabel *titleLabel = (UILabel *)[cell viewWithTag:902];
                UILabel *dateLabel = (UILabel *)[cell viewWithTag:903];
                UILabel *studentNameLabel = (UILabel *)[cell viewWithTag:904];
                
                titleLabel.text = self.evaluations[indexPath.row][@"name"];
                dateLabel.text = @"11/23/2017";
                studentNameLabel.text = @"";
            }
        }
        else  if(indexPath.row == count){
            CellIdentifier = @"Cell2";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            UIButton *recipeNameButton3 = (UIButton *)[cell viewWithTag:201];
            recipeNameButton3.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
            recipeNameButton3.layer.borderWidth = 0.5f;
            
            UIButton *recipeNameButton4 = (UIButton *)[cell viewWithTag:202];
            recipeNameButton4.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
            recipeNameButton4.layer.borderWidth = 0.5f;
            
            UIButton *recipeNameButton5 = (UIButton *)[cell viewWithTag:203];
            recipeNameButton5.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
            recipeNameButton5.layer.borderWidth = 0.5f;
            
            UIButton *recipeNameButton6 = (UIButton *)[cell viewWithTag:204];
            recipeNameButton6.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
            recipeNameButton6.layer.borderWidth = 0.5f;
            
            UILabel *educatorName = (UILabel *)[cell viewWithTag:906];
            NSDictionary *userInfo = [[UserCache sharedInstance] getUserInfo];
            NSString *name = concat(userInfo[@"name"], @" ");
            name = concat(name, userInfo[@"surname"]);
            educatorName.text = name;
            
            UILabel *jobName = (UILabel *)[cell viewWithTag:907];
            if([userInfo[@"educator"] isKindOfClass:[NSNull class]]){
                jobName.text = @"Student";
            }else{
                jobName.text = @"Educator";
            }
            
            UIImageView *userImageView = (UIImageView *)[cell viewWithTag:908];
            if(![userInfo[@"imagePath"] isKindOfClass:[NSNull class]]){
                [userImageView sd_setImageWithURL:userInfo[@"imagePath"]];
            }            
            UIButton *timelineButton = (UIButton *)[cell viewWithTag:202];
            UIButton *addStudentButton = (UIButton *)[cell viewWithTag:203];
            UIButton *inviteButton = (UIButton *)[cell viewWithTag:204];
            
            [timelineButton addTarget: self
                               action: @selector(timelineButtonTap)
                     forControlEvents: UIControlEventTouchUpInside];
            [addStudentButton addTarget: self
                                 action: @selector(addStudentButtonTap)
                       forControlEvents: UIControlEventTouchUpInside];
            [inviteButton addTarget: self
                             action: @selector(inviteButtonTap)
                   forControlEvents: UIControlEventTouchUpInside];
        }
        else  if(indexPath.row == count + 1){
            CellIdentifier = @"Cell3";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            UIButton *recipeNameButton3 = (UIButton *)[cell viewWithTag:205];
            recipeNameButton3.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
            recipeNameButton3.layer.borderWidth = 0.5f;
            
            UIButton *recipeNameButton4 = (UIButton *)[cell viewWithTag:206];
            recipeNameButton4.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
            recipeNameButton4.layer.borderWidth = 0.5f;
            
            UIButton *manageClassButton = (UIButton *)[cell viewWithTag:205];
            UIButton *deleteButton = (UIButton *)[cell viewWithTag:206];
            
            [manageClassButton addTarget: self
                                  action: @selector(manageClassButtonTap)
                        forControlEvents: UIControlEventTouchUpInside];
            [deleteButton addTarget: self
                             action: @selector(deleteButtonTap)
                   forControlEvents: UIControlEventTouchUpInside];
        }
        
        // Configure the cell...
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        return cell;
    }else if(tableView == _sortTable){
        
        DropDownCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sortCell"];
        cell.dropDownButton.tag = indexPath.row;
        [cell.dropDownButton setTitle: self.sortOrders[indexPath.row] forState: normal];
        [cell.dropDownText setFont:[UIFont systemFontOfSize:17]];
        return cell;
        
    }else{
        return nil;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == _tableView){
        
        NSUInteger row = indexPath.row;
        NSInteger count = self.students.count;
        
        if (i == 1) {
            count = self.evaluations.count;
        }
        
        if(indexPath.row < count){
            if(i==0){
                return 134;
                
            }
            else {
                return 170;
            }
        }
        else  if(indexPath.row == count){
            return 525;
        }
        else  if(indexPath.row == count+1){
            return 250;
        }
        else {
            return 0;
        }
    }else{
        return 40;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView == _tableView){
        
        NSInteger row = [indexPath row];
        if (i == 0) {
            NSLog(@"student selected");
            if(row <= self.students.count - 1 && self.students.count > 0){
                self.selectedStudent = self.students[row];
                [self performSegueWithIdentifier:@"classroomprofile" sender:self];
            }
        } else if (i == 1) {
            NSLog(@"evaluation selected");
            
            [self getContentSchedule:self.evaluations[row][@"contentId"]];
//            [self getEvaluation:self.evaluations[row][@"contentId"]];
        }
    }else if(tableView == _sortTable){
        
    }
}


- (void)timelineButtonTap {
    [self performSegueWithIdentifier:@"timeline" sender:self];
}
- (void)addStudentButtonTap {
    [self performSegueWithIdentifier:@"addstudent" sender:self];
}
- (void)inviteButtonTap {
    [self performSegueWithIdentifier:@"invite" sender:self];
}
- (void)manageClassButtonTap {
    [self performSegueWithIdentifier:@"addclass" sender:self];
}
- (void)deleteButtonTap {
    [self showLoader];
    NSString *url = concat(@"services/app/Classroom/Delete?id=", self.data[@"id"]);
    [NetworkHelper placeDeleteRequest:nil relativeUrl:url withSuccess:^(NSDictionary *jsonObject) {
        [self hideLoader];
        [self showErrorMessage:@"Successfully Deleted Class"];
        [self.navigationController popViewControllerAnimated:YES];
        
    } andFailure:^(NSString *string) {
        [self hideLoader];
        if (!string) {
            string = @"error deleting class";
        }
        [self showErrorMessage:string];
    }];
}


-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation

*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"addclass"]) {
        // Get reference to the destination view controller
        ManageViewController *vc = [segue destinationViewController];
        vc.data = self.data;
    } else if ([[segue identifier] isEqualToString:@"timeline"]) {
        // Get reference to the destination view controller
        ClassTimelineViewController *vc = [segue destinationViewController];
        vc.data = self.data;
    } else if ([[segue identifier] isEqualToString:@"addstudent"]) {
        // Get reference to the destination view controller
        AddStudentViewController *vc = [segue destinationViewController];
        vc.data = self.data;
    } else if ([[segue identifier] isEqualToString:@"invite"]) {
        // Get reference to the destination view controller
        ClassroomInviteViewController *vc = [segue destinationViewController];
        vc.data = self.data;
    } else if ([[segue identifier] isEqualToString:@"classroomprofile"]) {
        // Get reference to the destination view controller
        StudentEvaluationsViewController *vc = [segue destinationViewController];
        vc.studentData = self.selectedStudent;
        vc.classroomData = self.data;
    }
}

- (IBAction)actionStudent:(id)sender {
    
//    UIView *bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0, _btnStudent.frame.size.height - 1.0f, _btnStudent.frame.size.width, 1)];
//    bottomBorder.backgroundColor = [UIColor orangeColor];
//
//    [_btnStudent addSubview:bottomBorder];
//
//    UIView *bottomBorder2 = [[UIView alloc] initWithFrame:CGRectMake(0, _btnEvalutions.frame.size.height - 1.0f, _btnEvalutions.frame.size.width, 1)];
//    bottomBorder2.backgroundColor = [UIColor whiteColor];
//
//    [_btnEvalutions addSubview:bottomBorder2];
    _sortField.hidden = YES;
    i = 0;
    self.txtSearch.placeholder = @"Students";
    [_tableView reloadData];
}

- (void)getContentSchedule: (NSString *)contentId {
    
    [NetworkHelper placeGetRequest:nil relativeUrl:concat(@"services/app/ContentSchedule/GetByContentId?id=", contentId) withSuccess:^(NSDictionary *jsonObject) {
        NSLog(@"this is content schedule: %@", jsonObject);
        NSMutableArray *content = jsonObject[@"result"];
        NSLog(@"content's count: %li", content.count);
    } andFailure:^(NSString *string) {
        NSLog(@"getUserInfo Failure:%@", string);
    }];
    
}

- (void)getEvaluation: (NSString *)contentId {
    
    [NetworkHelper placeGetRequest:nil relativeUrl:concat(@"services/app/Evaluation/GetByContentId?id=", contentId) withSuccess:^(NSDictionary *jsonObject) {
        NSLog(@"this is evaluation data: %@", jsonObject);
    } andFailure:^(NSString *string) {
        NSLog(@"getUserInfo Failure:%@", string);
    }];
}

- (void)searchStudent:(NSString *)value {
    
    NSDictionary *dataToSend = @{
                                 @"searchText" : value,
                                 @"sortOrder" : @(self.currentSortSelection),
                                 @"maxResultCount" : @"20",
                                 @"skipCount" : @"0"};
    
    [self showLoader];
    [NetworkHelper placePostRequest:dataToSend relativeUrl:@"services/app/Student/Search" withSuccess:^(NSDictionary *jsonObject) {
        [self hideLoader];
        if (((NSArray *)jsonObject[@"result"]).count == 0) {
            [self showErrorMessage:[NSString stringWithFormat:@"No results found for:  %@", value]];
        }
        NSLog(@"student search result: %@", jsonObject);
        [self.students removeAllObjects];
        [self.students addObjectsFromArray:jsonObject[@"result"]];
        [self.tableView reloadData];
    } andFailure:^(NSString *string) {
        [self hideLoader];
        [self showErrorMessage:string];
    }];
}

- (void)searchEvaluation:(NSString *)value {
    
    NSDictionary *dataToSend = @{
                                 @"searchText" : value,
                                 @"sortOrder" : @(self.currentSortSelection),
                                 @"maxResultCount" : @"20",
                                 @"skipCount" : @"0"};
    
    [self showLoader];
    [NetworkHelper placePostRequest:dataToSend relativeUrl:@"services/app/Evaluation/Search" withSuccess:^(NSDictionary *jsonObject) {
        [self hideLoader];
        if (((NSArray *)jsonObject[@"result"]).count == 0) {
            [self showErrorMessage:[NSString stringWithFormat:@"No results found for:  %@", value]];
        }
        NSLog(@"evaluation search result: %@", jsonObject);
        [self.evaluations removeAllObjects];
        [self.evaluations addObjectsFromArray:jsonObject[@"result"]];
        [self.tableView reloadData];
    } andFailure:^(NSString *string) {
        [self hideLoader];
        [self showErrorMessage:string];
    }];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    NSLog(@"return pressed");
    if (i == 0) {
        [self searchStudent:textField.text];
    } else {
        [self searchEvaluation:textField.text];
    }
    return NO;
}

- (IBAction)selectSortButton:(id)sender {
    
    if(self.sortTable.hidden == YES){

        [UIView transitionWithView: self.sortTable
                          duration: 0.35f
                           options: UIViewAnimationOptionTransitionCurlDown
                        animations: ^(void)
         {
             self.sortTable.hidden = NO;
         }
                        completion: nil];

    }else{

        [UIView transitionWithView: self.sortTable
                          duration: 0.35f
                           options: UIViewAnimationOptionTransitionCurlUp
                        animations: ^(void)
         {
             self.sortTable.hidden = YES;
         }
                        completion: nil];
    }
}

- (IBAction)actionSortButton:(UIButton *)sender {
    
    self.txtSort.text = self.sortOrders[sender.tag];
    self.currentSortSelection = sender.tag;
    
    [UIView transitionWithView: self.sortTable
                      duration: 0.35f
                       options: UIViewAnimationOptionTransitionCurlUp
                    animations: ^(void)
     {
         self.sortTable.hidden = YES;
     }
                    completion: nil];
    
    sender.backgroundColor = [UIColor whiteColor];
    [sender setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
}

- (IBAction)changeBackgroundColor:(UIButton *)sender {
    
    sender.backgroundColor = [UIColor colorWithRed:95.0f/255.0f green:96.0f/255.0f blue:171.0f/255.0f alpha:1.0f];
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
}

- (IBAction)actionEcaluations:(id)sender {
    
    _sortField.hidden = NO;
    i = 1;
    self.txtSearch.placeholder = @"Evaluations";
    [_tableView reloadData];
}




@end
