//
//  WebAPIs.h
//  Character.ly
//
//  Created by Adeel Nasir on 08/10/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebAPIs : NSObject
-(NSMutableArray*)Login:(NSString*)username:(NSString*)password:(NSString*)remember;
@end
