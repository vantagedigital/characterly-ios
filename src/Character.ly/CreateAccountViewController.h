//
//  CreateAccountViewController.h
//  Character.ly
//
//  Created by Adeel Nasir on 08/08/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateAccountViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblCreate;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblPasscode;
@property (weak, nonatomic) IBOutlet UITextField *txtPasscode;
@property (weak, nonatomic) IBOutlet UILabel *lblResend;
@property (weak, nonatomic) IBOutlet UILabel *lblGet;
@property (weak, nonatomic) IBOutlet UIButton *btnEnter;
- (IBAction)actionBack:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UIButton *btnGetNew;
- (IBAction)enterAction:(id)sender;

- (IBAction)showInformation:(id)sender;
- (IBAction)emailDismiss:(id)sender;
- (IBAction)passwordDismiss:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightFromTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingFromSide;

@end
