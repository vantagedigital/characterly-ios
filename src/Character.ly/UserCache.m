//
//  UserCache.m
//  TakeAnL
//
//  Created by Jesse A on 23/09/2017.
//  Copyright © 2017 Jesse A. All rights reserved.
//

#import "UserCache.h"

NSString *const kPrefsName = @"CharacterlyUserCache";

NSString *const KEY_ACCESS_TOKEN = @"access_token";
NSString *const KEY_ENCRYPTED_ACCESS_TOKEN = @"encrypted_access_token";
NSString *const KEY_USER_INFO = @"user_info";
NSString *const KEY_CONTENT_INFO = @"content_info";
NSString *const KEY_USER_ID = @"user_id";

UserCache *_userCache;

@implementation UserCache

+ (UserCache *)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _userCache = [[UserCache alloc] init];
    });
    return _userCache;
}

- (NSMutableDictionary *)generalPrefs {
    return [[self prefs:kPrefsName] mutableCopy];
}

- (NSMutableDictionary *)prefs:(NSString *)name {
    NSMutableDictionary * prefs = [[NSUserDefaults standardUserDefaults] objectForKey:name];
    
    if (!prefs) {
        prefs = [NSMutableDictionary dictionary];
        [self savePrefs:prefs withName:name];
    }
    return prefs;
}

- (void)saveGeneralPrefs:(NSMutableDictionary *)dic {
    [self savePrefs:dic withName:kPrefsName];
}

- (void)savePrefs:(NSMutableDictionary *)dic withName:(NSString *)name {
    [[NSUserDefaults standardUserDefaults] setObject:dic forKey:name];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)isLoggedIn {
    return !IsEmpty([self getAccessToken]);
}

- (NSString *)getAccessToken {
    return [self generalPrefs][KEY_ACCESS_TOKEN];
}

- (void)setAccessToken:(NSString *)value {
    NSMutableDictionary *prefs = [self generalPrefs];
    value = value == nil ? value : concat(@"Bearer ", value);
    prefs[KEY_ACCESS_TOKEN] = value;
    [self saveGeneralPrefs:prefs];
}

- (NSString *)getEncryptedAccessToken {
    return [self generalPrefs][KEY_ENCRYPTED_ACCESS_TOKEN];
}

- (void)setEncryptedAccessToken:(NSString *)value {
    NSMutableDictionary *prefs = [self generalPrefs];
    prefs[KEY_ENCRYPTED_ACCESS_TOKEN] = value;
    [self saveGeneralPrefs:prefs];
}

- (NSString *)getUserId {
    return [self generalPrefs][KEY_USER_ID];
}

- (void)setUserId:(NSString *)value {
    NSMutableDictionary *prefs = [self generalPrefs];
    prefs[KEY_USER_ID] = value;
    [self saveGeneralPrefs:prefs];
}

- (NSDictionary *)getUserInfo {
    NSData *data = [self generalPrefs][KEY_USER_INFO];
    NSError* error;
    NSDictionary* json = [NSDictionary dictionary];
    @try {
        json = [NSJSONSerialization JSONObjectWithData:data
                                               options:kNilOptions
                                                 error:&error];
    } @catch (NSException *exception) {}
    return json;
}

- (void)setCurrentUser:(NSDictionary *)value {
    NSMutableDictionary *prefs = [self generalPrefs];
    NSData *data = [NSJSONSerialization dataWithJSONObject:value options:kNilOptions error:nil];
    prefs[KEY_USER_INFO] = data;
    [self saveGeneralPrefs:prefs];
}

- (NSDictionary *)getContentInfo {
    NSData *data = [self generalPrefs][KEY_CONTENT_INFO];
    NSError* error;
    NSDictionary* json = [NSDictionary dictionary];
    @try {
        json = [NSJSONSerialization JSONObjectWithData:data
                                               options:kNilOptions
                                                 error:&error];
    } @catch (NSException *exception) {

    }
    
    return json;
}

- (void)setContentInfo:(NSDictionary *)value {
    NSMutableDictionary *prefs = [self generalPrefs];
    NSData *data = [NSJSONSerialization dataWithJSONObject:value options:kNilOptions error:nil];
    prefs[KEY_CONTENT_INFO] = data;
    [self saveGeneralPrefs:prefs];
}

- (void)clear {
    [self setEncryptedAccessToken:nil];
    [self setAccessToken:nil];
    [self setUserId:nil];
}

@end
