//
//  ViewController.h
//  Character.ly
//
//  Created by Adeel Nasir on 08/08/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BEMCheckBox.h"

@interface ViewController : UIViewController<UITextFieldDelegate>{
    BOOL checked1;
}
@property (weak, nonatomic) IBOutlet BEMCheckBox *checkRemember;
@property (weak, nonatomic) IBOutlet UITextField *txtUsername;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UILabel *lblForgot;
@property (weak, nonatomic) IBOutlet UILabel *lblCreateaccount;
@property (weak, nonatomic) IBOutlet UILabel *lblUser;
@property (weak, nonatomic) IBOutlet UILabel *lblPassword;
- (IBAction)Login:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnForgot;
@property (strong, nonatomic) IBOutlet UIButton *btnCreate;
- (IBAction)actionBack:(id)sender;
- (IBAction)usernameDismiss:(id)sender;
- (IBAction)passwordDissmiss:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightFromTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingFromSide;

@end

