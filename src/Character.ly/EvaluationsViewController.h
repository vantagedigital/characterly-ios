//
//  EvaluationsViewController.h
//  Character.ly
//
//  Created by Adeel Nasir on 22/08/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TLTagsControl.h"
#import "DropDownCell.h"
#import "Evaluation1ViewController.h"


@interface EvaluationsViewController:UIViewController<TLTagsControlDelegate,UITableViewDelegate,UITableViewDataSource>{
    
}
@property (strong, nonatomic) IBOutlet UIScrollView *scoller;
@property (strong, nonatomic) IBOutlet TLTagsControl *defaultEditingTagControl;
@property (strong, nonatomic) IBOutlet UITextField *txtSort;
@property (strong, nonatomic) IBOutlet UITextField *txtGrades;
@property (strong, nonatomic) IBOutlet UITextField *txtClass;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *classButton;
@property (weak, nonatomic) IBOutlet UIButton *gradeButton;
@property (weak, nonatomic) IBOutlet UIButton *sortButton;
@property (weak, nonatomic) IBOutlet UITableView *sortTable;
@property (weak, nonatomic) IBOutlet UITableView *gradeTable;
@property (weak, nonatomic) IBOutlet UITableView *classTable;

@end
