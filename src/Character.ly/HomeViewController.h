//
//  HomeViewController.h
//  Character.ly
//
//  Created by Adeel Nasir on 10/08/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <JTCalendar/JTCalendar.h>

@interface HomeViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,JTCalendarDelegate> {

}
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *menuView;
- (IBAction)openMenu:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *menuLessons;

@property (weak, nonatomic) IBOutlet UIButton *btnCloseMenu;
- (IBAction)closeMenu:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *menuClassrooms;
@property (weak, nonatomic) IBOutlet UIButton *menuEvalucations;
@property (weak, nonatomic) IBOutlet JTCalendarMenuView *calendarMenuView;
@property (weak, nonatomic) IBOutlet JTHorizontalCalendarView *calendarContentView;
@property (strong, nonatomic) JTCalendarManager *calendarManager;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *calendarContentViewHeight;
@property (weak, nonatomic) IBOutlet UIButton *calendarButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scroller;

@end
