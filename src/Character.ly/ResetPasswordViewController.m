//
//  ResetPasswordViewController.m
//  Character.ly
//
//  Created by Adeel Nasir on 03/11/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "ResetPasswordViewController.h"
#import "MessageWindow.h"
#import "DGActivityIndicatorView.h"

@interface ResetPasswordViewController (){
    NSMutableArray *loginResponse;
    UIView *dimView;
    DGActivityIndicatorView *activityIndicatorView;
}

@end

@implementation ResetPasswordViewController
#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.hidden = YES;
    
    [self.view setBackgroundColor: RGB(247, 248, 250)];
    
    [self.txtPassword setBackgroundColor:RGB(251, 251, 251)];
    [self.txtReType setBackgroundColor:RGB(251, 251, 251)];
    
    self.lblDesc.text = [NSString stringWithFormat:@"Please enter a new password for your %@ account", self.email];
    
    
    UIView *padView = [[UIView alloc] initWithFrame:CGRectMake(10, 110, 10, 0)];
    _txtPassword.leftView = padView;
    _txtPassword.leftViewMode = UITextFieldViewModeAlways;
    
    
    UIView *padView1 = [[UIView alloc] initWithFrame:CGRectMake(10, 110, 10, 0)];
    _txtReType.leftView = padView1;
    _txtReType.leftViewMode = UITextFieldViewModeAlways;
    
    _txtPassword.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtPassword.layer.borderWidth = 1.0f;
    
    _txtReType.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtReType.layer.borderWidth = 1.0f;
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)actionBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)changePassword:(id)sender {
    NSLog(@"in action");
    NSLog(@"This is access token: %@", [[UserCache sharedInstance] getAccessToken]);
    NSMutableArray *ns = [self Login:_email :_txtPassword.text :_txtReType.text :_authToken];
    
}

-(void) returnTextColor {
    
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds *NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        _txtReType.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
        _txtReType.backgroundColor = [self colorWithHexString:@"f7f8fa"];
        _txtPassword.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
        _txtPassword.backgroundColor = [self colorWithHexString:@"f7f8fa"];
    });
}

-(void) displayResetError:(UITextField *)errorTextField {
    errorTextField.layer.borderColor = [self colorWithHexString:@"e82a16"].CGColor;
    errorTextField.backgroundColor = [self colorWithHexString:@"ffc0ba"];
}

- (void)showErrorMessage:(NSString *)messageText {
    
    [dimView removeFromSuperview];
    [activityIndicatorView stopAnimating];
    
    MessageWindow *message = [[MessageWindow alloc] init];
    
    //  [myViewController2.view addSubView:mySubView];
    // add any other views to myViewController2's view
    [message removeFromSuperview];
    
    [self.view addSubview:message];
    
    [message setMessage:messageText];
    
    [message setAlpha:1.0f];
    
    //fade in
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionTransitionFlipFromTop
                     animations:^{
                         
                         [message setAlpha:1.0f];
                         message.frame = CGRectMake(_mainView.frame.origin.x + _leadingFromSide.constant, _mainView.frame.origin.y + _heightFromTop.constant, _txtPassword.frame.size.width, _txtPassword.frame.size.height);
                         
                     } completion:^(BOOL finished) {
                         
                         //fade out
                         [UIView animateWithDuration:0.3
                                               delay:2.0
                                             options:UIViewAnimationOptionTransitionCurlUp
                                          animations:^{
                                              
                                              [message setAlpha:0.0f];
                                              //                                 message.frame = CGRectMake(30.0, 140.0, 350, 0);
                                              //
                                              
                                              
                                          } completion:^(BOOL finished) {
                                              [message setMessage:@""];
                                          }];
                         
                     }];
}

-(NSMutableArray*)Login:(NSString*)username :(NSString*)password :(NSString*)repassword :(NSString*)token{
    if([password isEqualToString:@""]){
        
        [self showErrorMessage:@"Please fill password field"];
        [self displayResetError:_txtPassword];
        [self returnTextColor];
    }
    else if([repassword isEqualToString:@""]){
        
        [self showErrorMessage:@"Please fill retype password field"];
        [self displayResetError:_txtReType];
        [self returnTextColor];
    }
    if(![password isEqualToString:repassword]){
        
        [self showErrorMessage:@"Password did not match"];
        [self displayResetError:_txtReType];
        [self returnTextColor];
    }
    else {
        dimView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
        dimView.backgroundColor = [UIColor blackColor];
        dimView.alpha = 0.7f;
        dimView.userInteractionEnabled = NO;
        [self.view addSubview:dimView];
        CGRect newFrame = dimView.frame;
        
        newFrame.size.width = 1200;
        newFrame.size.height = 1500;
        [dimView setFrame:newFrame];
        [dimView setCenter:CGPointMake(187.0f, 333.0f)];
        activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeLineScale tintColor:[UIColor whiteColor] size:40.0f];
        activityIndicatorView.center = self.view.center;
        [self.view addSubview:activityIndicatorView];
        [activityIndicatorView startAnimating];
        loginResponse = NULL;
        loginResponse = [[NSMutableArray alloc]init];
        
        NSDictionary *dataToSend = @{
                                     @"emailAddress" : username,
                                     @"token" : token,
                                     @"password" : password
                                     };
        
        [NetworkHelper placePostRequest:dataToSend relativeUrl:@"services/app/Account/ResetPassword" withSuccess:^(NSDictionary *jsonObject) {
            // cache data
            
            if(jsonObject[@"result"][@"errors"] == (NSDictionary *)[NSNull null]){
                
                [self showErrorMessage: jsonObject[@"result"][@"message"]];
                [self.navigationController popToRootViewControllerAnimated:YES];
                
            }else{
                
                [self showErrorMessage: jsonObject[@"result"][@"message"]];
            }

        } andFailure:^(NSString *string) {
            NSLog(@"resetFailure:");
            [self showErrorMessage:string];
            [self returnTextColor];
        }];
    }
    
    return loginResponse;
    
}



@end
