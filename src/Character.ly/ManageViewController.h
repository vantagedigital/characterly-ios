//
//  ManageViewController.h
//  Character.ly
//
//  Created by Adeel Nasir on 10/09/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "ColorButton.h"

@interface ManageViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *btnImage;
@property (strong, nonatomic) IBOutlet UIButton *btnColor;
@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *ColorSelectView;
@property (weak, nonatomic) IBOutlet UIView *imageSelectView;
@property (weak, nonatomic) IBOutlet UIImageView *chosenImage;
@property (strong, nonatomic) IBOutletCollection(ColorButton) NSArray *colorButtonGroup;

- (IBAction)actionImage:(id)sender;
- (IBAction)actionColor:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnCancel;
@property (strong, nonatomic) IBOutlet UIButton *btnAdd;
@property (strong, nonatomic) IBOutlet UITextField *txtClassName;
@property (strong, nonatomic) IBOutlet UITextField *txtGrade;
@property (strong, nonatomic) IBOutlet UITextField *txtSchool;
@property (strong, nonatomic) IBOutlet UITextField *txtCode;
@property (strong, nonatomic) IBOutlet UITextField *txtSearch;
@property (strong, nonatomic) IBOutlet UITextField *txtSort;
@property (strong, nonatomic) IBOutlet UIView *headerView;

@property (strong, nonatomic) NSDictionary *data;

@property (weak, nonatomic) IBOutlet UITableView *gradeTable;
@property (weak, nonatomic) IBOutlet UITableView *sortTable;


@end
