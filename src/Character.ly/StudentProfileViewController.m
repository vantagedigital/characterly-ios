//
//  StudentProfileViewController.m
//  Character.ly
//
//  Created by Adeel Nasir on 14/08/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "StudentProfileViewController.h"
#import "NavViewController.h"
#import "BadgeDetailView.h"


@interface StudentProfileViewController (){
    
   
}
@property (strong, nonatomic) NSMutableArray *notifications;

@property (weak, nonatomic) IBOutlet UILabel *studentName;
@property (weak, nonatomic) IBOutlet UILabel *studentId;

@property (weak, nonatomic) IBOutlet UILabel *gradeLabel;
@property (weak, nonatomic) IBOutlet UILabel *homeroomLabel;
@property (weak, nonatomic) IBOutlet UILabel *dobLabel;
@property (weak, nonatomic) IBOutlet UILabel *genderLabel;
@property (weak, nonatomic) IBOutlet UILabel *schoolAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;

@property (weak, nonatomic) IBOutlet UILabel *fatherNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *fatherAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *fatherEmailLabel;
@property (weak, nonatomic) IBOutlet UILabel *fatherPhoneNumber;

@property (weak, nonatomic) IBOutlet UILabel *motherNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *motherAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *motherEmailLabel;
@property (weak, nonatomic) IBOutlet UILabel *motherPhoneNumber;

@property (weak, nonatomic) IBOutlet UILabel *notificationsCountLabel;

@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;

@property (weak, nonatomic) IBOutlet UIImageView *badge1ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *badge2ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *badge4ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *badge3ImageView;
@property (weak, nonatomic) IBOutlet UILabel *badge1Name;
@property (weak, nonatomic) IBOutlet UILabel *badge2Name;
@property (weak, nonatomic) IBOutlet UILabel *badge3Name;
@property (weak, nonatomic) IBOutlet UILabel *badge4Name;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *progress;

@end

@implementation StudentProfileViewController
#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = YES;
    
    NavViewController *myViewController2 = [[NavViewController alloc] initForAutoLayout];
    [self.view addSubview:myViewController2];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:[UIApplication sharedApplication].statusBarFrame.size.height];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
    [myViewController2 autoSetDimension:ALDimensionHeight toSize:200];
    myViewController2.parent = self;
    
    self.notifications = [NSMutableArray array];
    
    [NetworkHelper placeGetRequest:nil relativeUrl:@"services/app/SystemNotification/GetMyNotifications" withSuccess:^(NSDictionary *jsonObject) {
        self.notifications = jsonObject[@"result"];
        [self.tblNotifications reloadData];
        self.notificationsCountLabel.text = [NSString stringWithFormat:@"%ld", (long)self.notifications.count];
    } andFailure:^(NSString *string) {
        NSLog(@"GetMyNotifications Failure:%@", string);
        //               [self showErrorMessage:string];
    }];
    
    self.gradeLabel.text = @"3rd Grade";
    self.homeroomLabel.text = @"Green Giants";
    self.dobLabel.text = @"04/06/2009";
    self.genderLabel.text = @"Female";
    self.schoolAddressLabel.text = @"";
    
    [self configureBadges];
    
    NSDictionary *userInfo = [[UserCache sharedInstance] getUserInfo];
    
    if (IsEmpty(userInfo[@"imagePath"])) {
        [NetworkHelper placeGetRequest:nil relativeUrl:concat(@"services/app/FileAttachment/GetByEntityId?id=", userInfo[@"id"]) withSuccess:^(NSDictionary *model) {
            @try {
                NSArray *arr = model[@"result"];
                NSString *path = arr[arr.count - 1][@"path"];
                
                NSMutableDictionary *dic = userInfo.mutableCopy;
                [dic setObject:path forKey:@"imagePath"];
                [[UserCache sharedInstance] setCurrentUser:dic];
                
                [self updateProfilePic];
            } @catch(NSException *e) {
                NSLog(@"error saving profile image");
            }
        } andFailure:^(NSString *string) {
            NSLog(@"getUserInfo Failure:%@", string);
            //               [self showErrorMessage:string];
        }];
    } else {
        [self updateProfilePic];
    }
    
    @try {
        [self resetUI];
        if (!IsEmpty(userInfo[@"student"][@"parents"])) {
            [self configureFather:((NSArray *)userInfo[@"student"][@"parents"])[0]];
            
            if (((NSArray *)userInfo[@"student"][@"parents"]).count > 1) {
                [self configureMother:((NSArray *)userInfo[@"student"][@"parents"])[1]];
            }
        }
    } @catch (NSException *e) {
        
    }

    self.emailLabel.text = userInfo[@"emailAddress"];
    self.phoneLabel.text = @"555-555-5555";
    
    NSString *firstName = userInfo[@"name"];
    firstName = concat(firstName, @" ");
    NSString *lastName = userInfo[@"surname"];
    self.studentName.text = concat(firstName, lastName);
    self.studentId.text = concat(@"Student ID: ", userInfo[@"id"]);
    
    // Do any additional setup after loading the view
    [_scroller setScrollEnabled:YES];
    [_scroller setContentSize:CGSizeMake(375, 4550)];
   
    
//    UIImage *image = [UIImage imageNamed:@"characterly.png"];
//    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:image];
    _tblNotifications.hidden = YES;
    
   // [self.topView setBackgroundColor:RGB(240, 85, 61)];
    
    //_btnEdit.layer.borderColor = [UIColor colorWithRed:34 green:34 blue:34 alpha:1].CGColor;
    _btnEdit.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _btnEdit.layer.borderWidth = 1.0f;

    _fatherView.layer.borderWidth = 1.0f;
    _fatherView.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    
    
    _motherView.layer.borderWidth = 1.0f;
    _motherView.layer.borderColor =  [self colorWithHexString:@"e6e6e6"].CGColor;
  //  box.layer.borderColor = [UIColor colorWithRed:247.0 green:248.0 blue:250.0 alpha:0.2].CGColor;
    
//    UIColor *borderColor = [UIColor colorWithRed:230.0 green:230.0 blue:230.0 alpha:1.0];
//    [_motherView.layer setBorderColor:borderColor.CGColor];
    
    [_tblNotifications setShowsHorizontalScrollIndicator:NO];
    [_tblNotifications setShowsVerticalScrollIndicator:NO];
    
    [self.tblNotifications setSeparatorColor:[UIColor clearColor]];
    
    
    
    
    
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]
                                             initWithTarget:self action:@selector(ClickEventOnImage:)];
    [tapRecognizer setNumberOfTouchesRequired:1];
    [tapRecognizer setDelegate:self];
    //Don't forget to set the userInteractionEnabled to YES, by default It's NO.
    _badge1.userInteractionEnabled = YES;
    [_badge1 addGestureRecognizer:tapRecognizer];
    
    UITapGestureRecognizer *tapRecognizer2 = [[UITapGestureRecognizer alloc]
                                             initWithTarget:self action:@selector(ClickEventOnImage:)];
    [tapRecognizer2 setNumberOfTouchesRequired:1];
    [tapRecognizer2 setDelegate:self];
    //Don't forget to set the userInteractionEnabled to YES, by default It's NO.
    _badge2.userInteractionEnabled = YES;
    [_badge2 addGestureRecognizer:tapRecognizer2];
    
    UITapGestureRecognizer *tapRecognizer4 = [[UITapGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(ClickEventOnImage:)];
    [tapRecognizer4 setNumberOfTouchesRequired:1];
    [tapRecognizer4 setDelegate:self];
    //Don't forget to set the userInteractionEnabled to YES, by default It's NO.
    _badge4.userInteractionEnabled = YES;
    [_badge4 addGestureRecognizer:tapRecognizer4];
    
    
    UITapGestureRecognizer *tapRecognizer3 = [[UITapGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(ClickEventOnImage:)];
    [tapRecognizer3 setNumberOfTouchesRequired:1];
    [tapRecognizer3 setDelegate:self];
    //Don't forget to set the userInteractionEnabled to YES, by default It's NO.
    _badge3.userInteractionEnabled = YES;
    [_badge3 addGestureRecognizer:tapRecognizer3];
    
    
    if ([UserCache sharedInstance].isNotifications) {
        [self openNotifications:nil];
    }else{
        [self openProfile:nil];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self updateProfilePic];
}

- (void)updateProfilePic {
    [self.profileImageView sd_setImageWithURL:[NSURL URLWithString:[[UserCache sharedInstance] getUserInfo][@"imagePath"]]];
}

- (void)resetUI {
    self.fatherNameLabel.text = @"";
    self.fatherAddressLabel.text = @"";
    self.fatherEmailLabel.text = @"";
    self.fatherPhoneNumber.text = @"";

    self.motherNameLabel.text = @"";
    self.motherAddressLabel.text = @"";
    self.motherEmailLabel.text = @"";
    self.motherPhoneNumber.text = @"";
}

- (void)configureFather:(NSDictionary *)father {
    self.fatherNameLabel.text = father[@"fullName"];
    self.fatherAddressLabel.text = @"";
    self.fatherEmailLabel.text = father[@"emailAddress"];
    self.fatherPhoneNumber.text = @"";
}

- (void)configureMother:(NSDictionary *)mother {
    self.motherNameLabel.text = mother[@"fullName"];
    self.motherAddressLabel.text = @"";
    self.motherEmailLabel.text = mother[@"emailAddress"];
    self.motherPhoneNumber.text = @"";
}

- (void)configureBadges {
    self.progress.hidden = NO;
    self.badge1.hidden = YES;
    self.badge1Name.hidden = YES;
    self.badge2.hidden = YES;
    self.badge2Name.hidden = YES;
    self.badge3.hidden = YES;
    self.badge3Name.hidden = YES;
    self.badge4.hidden = YES;
    self.badge4Name.hidden = YES;
    
    NSString *url = concat(@"services/app/Badge/GetByUserId?id=", [[UserCache sharedInstance] getUserInfo][@"id"]);
    [NetworkHelper placeGetRequest:nil relativeUrl:url withSuccess:^(NSDictionary *jsonObject) {
        self.progress.hidden = YES;
        @try {
            NSArray *arr = jsonObject[@"result"];
            for (int i = 0; i < arr.count; i++) {
                NSDictionary *badge = arr[i];
                UIImageView *badgeView = [self getBadgeFor:i];
                badgeView.hidden = NO;
                UILabel *badgeName = [self getBadgeNameFor:i];
                badgeName.hidden = NO;
                badgeName.text = badge[@"pillar"][@"name"];
                // TODO: set badge counter to 0
            }
            
        } @catch(NSException *e) {
            
        }
    } andFailure:^(NSString *string) {
        self.progress.hidden = YES;
        NSLog(@"getStudents Failure:%@", string);
        //               [self showErrorMessage:string];
    }];
}

- (UIImageView *)getBadgeFor:(NSInteger)i {
    if (i == 0) {
        return self.badge1;
    } else if (i == 1) {
        return self.badge2;
    } else if (i == 2) {
        return self.badge3;
    } else {
        return self.badge4;
    }
}

- (UILabel *)getBadgeNameFor:(NSInteger)i {
    if (i == 0) {
        return self.badge1Name;
    } else if (i == 1) {
        return self.badge2Name;
    } else if (i == 2) {
        return self.badge3Name;
    } else {
        return self.badge4Name;
    }
}
- (IBAction)seeAllBadgesTap:(id)sender {
    NSLog(@"see all badges tap");
    // TODO: launch BadgeListVC.h and finish the UI.
}

- (void) ClickEventOnImage:(id) sender
{
    NSLog(@"badge clicked");
    BadgeDetailView *myViewController2 = [[BadgeDetailView alloc] init];
    
    //  [myViewController2.view addSubView:mySubView];
    // add any other views to myViewController2's view
    
    [self.view addSubview:myViewController2];
    
    myViewController2.frame = CGRectMake(0, 0, self.view.frame.size.width, 730);
    
}
-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}
- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.notifications.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        // Configure the cell...
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
    NSDictionary *data = self.notifications[indexPath.row];
    cell.textLabel.text = data[@"title"];
    NSString *creationTimeString = data[@"creationTime"];
    
    cell.backgroundColor = RGB(217, 217, 255);
    cell.textLabel.textColor = RGB(95, 96, 171);
    
    cell.detailTextLabel.textColor = RGB(107, 124, 147);
    cell.detailTextLabel.text = [self getTimeElapsedString:[self getNSDateFromString:creationTimeString]];
    
    UIFont *myFont = [ UIFont fontWithName: @"Helvetica" size: 16.0 ];
    cell.textLabel.font  = myFont;
    cell.detailTextLabel.font = myFont;
        return cell;
    
}

- (NSDate *)getNSDateFromString:(NSString *)isoDateString {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    // Always use this locale when parsing fixed format date strings
    NSLocale *posix = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:posix];
    return [formatter dateFromString:isoDateString];
}

- (NSString *)getTimeElapsedString:(NSDate *)timeToCheck {
    NSDate *now = [NSDate date];
    long timeDiffMilliSecs = [now timeIntervalSince1970]
    - [timeToCheck timeIntervalSince1970];
    long timeDiffSecs = timeDiffMilliSecs / 1000;
    long timeDiffMins = timeDiffSecs / 60;
    long timeDiffHours = timeDiffMins / 60;
    long timeDiffDays = timeDiffHours / 24;
    long timeDiffMonths = timeDiffDays / 30;
    NSString *timeString = @"";
    if (timeDiffSecs <= 60) {
        timeString = [NSString stringWithFormat:@"%@s", @(timeDiffSecs)];;
    } else if (timeDiffMins <= 60) {
        timeString = [NSString stringWithFormat:@"%@m", @(timeDiffMins)];
    } else if (timeDiffHours <= 24) {
        timeString = [NSString stringWithFormat:@"%@h", @(timeDiffHours)];
    } else if (timeDiffDays <= 30) {
        timeString = [NSString stringWithFormat:@"%@d", @(timeDiffDays)];
    } else {
        timeString = [NSString stringWithFormat:@"%@mo", @(timeDiffMonths)];
    }
    
    return timeString;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)openProfile:(id)sender {
    _tblNotifications.hidden = YES;
    _profileView.hidden = NO;
    _scroller.scrollEnabled = YES;
    [self.btnProfile setBackgroundColor:RGB(255, 255, 255)];
    [self.btnNotifications setBackgroundColor:RGB(96, 96, 171)];
    [self.btnNotifications setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  //  [_btnProfile setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
 //   [_btnNotifications setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnProfile setTitleColor:RGB(96, 96, 171) forState:UIControlStateNormal];
    
}

- (IBAction)openNotifications:(id)sender {
    _tblNotifications.hidden = NO;
    _profileView.hidden = YES;
    _scroller.scrollEnabled = NO;
    [self.btnNotifications setBackgroundColor:RGB(255, 255, 255)];
    [self.btnProfile setBackgroundColor:RGB(96, 96, 171)];
    [self.btnProfile setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnNotifications setTitleColor:RGB(96, 96, 171) forState:UIControlStateNormal];
    
 //   [_btnNotifications setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
   //  [_btnProfile setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}



@end
