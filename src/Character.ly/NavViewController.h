//
//  NavViewController.h
//  Character.ly
//
//  Created by Macbook Pro on 14/09/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavViewController : UIView {
  
}


- (IBAction)openMenu:(id)sender;
- (IBAction)openSearch:(id)sender;

- (void)hideBackButton;

@property (strong, nonatomic) IBOutlet UIView *viewA;
@property (strong, nonatomic) UIViewController *parent;

@end
