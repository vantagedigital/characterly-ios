//
//  CreateAccountViewController.m
//  Character.ly
//
//  Created by Adeel Nasir on 08/08/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "CreateAccountViewController.h"
#import "AccountInformationView.h"
#import "MessageWindow.h"
#import "DGActivityIndicatorView.h"

@interface CreateAccountViewController (){
    UIView *dimView;
    DGActivityIndicatorView *activityIndicatorView;
}

@end

@implementation CreateAccountViewController
#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     self.navigationController.navigationBar.hidden = YES;
    
    [self.view setBackgroundColor: RGB(247, 248, 250)];
    [self.btnEnter setBackgroundColor:RGB(0, 188, 240)];
   [self.txtPasscode setBackgroundColor:RGB(251, 251, 251)];
//    [self.txtPassword setBackgroundColor:RGB(251, 251, 251)];
    [self.lblPasscode setTextColor:RGB(107, 124, 147)];
    [self.lblCreate setTextColor:RGB(107, 124, 147)];
    [self.lblGet setTextColor:RGB(107, 124, 147)];
    [self.lblResend setTextColor:RGB(107, 124, 147)];
    [self.lblDesc setTextColor:RGB(107, 124, 147)];
    
    [self.lblResend setUserInteractionEnabled:YES];
    _lblResend.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tapGesture = \
    [[UITapGestureRecognizer alloc]
     initWithTarget:self action:@selector(didTapLabelWithGesture:)];
    [_lblResend addGestureRecognizer:tapGesture];
    
    
    
   
    
    _btnGetNew.layer.borderColor = [UIColor whiteColor].CGColor;
    _btnGetNew.layer.borderWidth = 1.0f;
    
    
    UIView *padView = [[UIView alloc] initWithFrame:CGRectMake(10, 110, 10, 0)];
    _txtEmail.leftView = padView;
    _txtEmail.leftViewMode = UITextFieldViewModeAlways;
    
    
    UIView *padView1 = [[UIView alloc] initWithFrame:CGRectMake(10, 110, 10, 0)];
    _txtPasscode.leftView = padView1;
    _txtPasscode.leftViewMode = UITextFieldViewModeAlways;
    
    _txtEmail.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtEmail.layer.borderWidth = 1.0f;
    
    _txtPasscode.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtPasscode.layer.borderWidth = 1.0f;
    
}

- (void)didTapLabelWithGesture:(UITapGestureRecognizer *)tapGesture {
    [self performSegueWithIdentifier:@"rc" sender:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actionBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)enterAction:(id)sender {
    dimView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
    dimView.backgroundColor = [UIColor blackColor];
    dimView.alpha = 0.7f;
    dimView.userInteractionEnabled = NO;
    [self.view addSubview:dimView];
    CGRect newFrame = dimView.frame;
    
    newFrame.size.width = 1200;
    newFrame.size.height = 1500;
    [dimView setFrame:newFrame];
    [dimView setCenter:CGPointMake(187.0f, 333.0f)];
    
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeLineScale tintColor:[UIColor whiteColor] size:40.0f];
    activityIndicatorView.center = self.view.center;
    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    
    [self Create:_txtPasscode.text :_txtEmail.text];
}

- (void) returnTextColor {
    
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds *NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        _txtEmail.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
        _txtEmail.backgroundColor = [self colorWithHexString:@"f7f8fa"];
        _txtPasscode.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
        _txtPasscode.backgroundColor = [self colorWithHexString:@"f7f8fa"];
    });
}

- (void) displayCreatError:(UITextField *)errorTextField {
    errorTextField.layer.borderColor = [self colorWithHexString:@"e82a16"].CGColor;
    errorTextField.backgroundColor = [self colorWithHexString:@"ffc0ba"];
}

-(void)Create:(NSString*)code :(NSString*)email {
    
    BOOL isValid = [self validEmail:email];
    
    if([email isEqualToString:@""]){
        [self showErrorMessage:@"Please fill email field"];
        [self displayCreatError:_txtEmail];
        [self returnTextColor];
    } else if([code isEqualToString:@""]){
        [self showErrorMessage:@"Please fill code field"];
        [self displayCreatError:_txtPasscode];
        [self returnTextColor];
    } else if(!isValid){
        [self showErrorMessage:@"Please enter valid email address"];
        [self displayCreatError:_txtEmail];
        [self returnTextColor];
    } else {
        
        NSDictionary *dataToSend = @{
                                     @"Code" : code,
                                     @"EmailAddress" : email};
        
        [NetworkHelper placePostRequest:dataToSend relativeUrl:@"services/app/Invite/Validate" withSuccess:^(NSDictionary *jsonObject) {
            [self showErrorMessage:@"Successfully registered"];
            [self performSegueWithIdentifier:@"createPasswordSegue" sender:self];
        } andFailure:^(NSString *string) {
            NSLog(@"registerFailure:");
            [self showErrorMessage:string];
            [self displayCreatError:_txtEmail];
            [self displayCreatError:_txtPasscode];
            [self returnTextColor];
        }];
        
        
    }
}

- (IBAction)showInformation:(id)sender {
    
    AccountInformationView *myViewController2 = [[AccountInformationView alloc] init];
    
    //  [myViewController2.view addSubView:mySubView];
    // add any other views to myViewController2's view
    
    [self.view addSubview:myViewController2];
    
     myViewController2.frame = CGRectMake(0, 0, myViewController2.frame.size.width, myViewController2.frame.size.height);
   
    
    
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [_txtEmail resignFirstResponder];
    [_txtPasscode resignFirstResponder];
    
    return false;
}
- (BOOL) validEmail:(NSString*) emailString {
    
    if([emailString length]==0){
        return NO;
    }
    
    NSString *regExPattern = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    
    NSLog(@"%i", regExMatches);
    if (regExMatches == 0) {
        return NO;
    } else {
        return YES;
    }
}
- (IBAction)emailDismiss:(id)sender {
    [_txtEmail endEditing:YES];
    [_txtPasscode endEditing:YES];
    [_txtEmail resignFirstResponder];
    [_txtPasscode resignFirstResponder];
}

- (IBAction)passwordDismiss:(id)sender {
}
-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

- (void)showErrorMessage:(NSString *)messageText {
    [dimView removeFromSuperview];
    [activityIndicatorView stopAnimating];
    MessageWindow *message = [[MessageWindow alloc] init];
    
    [message removeFromSuperview];
    
    [self.view addSubview:message];
    
    [message setMessage:messageText];
    
    [message setAlpha:1.0f];
    
    //fade in
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionTransitionFlipFromTop
                     animations:^{
                         
                         [message setAlpha:1.0f];
                         message.frame = CGRectMake(_mainView.frame.origin.x + _leadingFromSide.constant, _mainView.frame.origin.y + _heightFromTop.constant, _txtEmail.frame.size.width, _txtEmail.frame.size.height);
                         
                     } completion:^(BOOL finished) {
                         
                         //fade out
                         [UIView animateWithDuration:0.3
                                               delay:2.0
                                             options:UIViewAnimationOptionTransitionCurlUp
                                          animations:^{
                                              
                                              [message setAlpha:0.0f];
                                              
                                          } completion:^(BOOL finished) {
                                              [message setMessage:@""];
                                          }];
                         
                     }];
}

@end
