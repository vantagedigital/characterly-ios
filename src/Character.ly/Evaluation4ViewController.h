//
//  Evaluation4ViewController.h
//  Character.ly
//
//  Created by Adeel Nasir on 16/08/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BEMCheckBox.h"

@interface Evaluation4ViewController : UIViewController{
    BOOL checked1;
    BOOL checked2;
    BOOL checked3;
}

@property (strong, nonatomic) IBOutlet UIButton *check1;
@property (strong, nonatomic) IBOutlet UIScrollView *scroller;
@property (strong, nonatomic) IBOutlet UIButton *check2;
@property (strong, nonatomic) IBOutlet UIButton *check3;
@property (strong, nonatomic) IBOutletCollection(BEMCheckBox) NSArray *checkBoxGroup;
- (IBAction)pressCheck1:(id)sender;
- (IBAction)pressCheck2:(id)sender;

- (IBAction)pressCheck3:(id)sender;

@end
