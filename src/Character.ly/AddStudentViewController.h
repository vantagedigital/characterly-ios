//
//  AddStudentViewController.h
//  Character.ly
//
//  Created by Macbook Pro on 02/10/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddStudentViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>{
    
}

@property (weak, nonatomic) IBOutlet UIButton *selectClassButton;
@property (weak, nonatomic) IBOutlet UITableView *selectClassTable;
@property (weak, nonatomic) IBOutlet UILabel *classroomNameLabel;

@property (strong, nonatomic) IBOutlet UIButton *btnCancel;
@property (strong, nonatomic) IBOutlet UIButton *btnAdd;
@property (weak, nonatomic) IBOutlet UIButton *btnUploadCSV;
@property (strong, nonatomic) IBOutlet UITextField *txtClassName;
@property (strong, nonatomic) IBOutlet UITextField *txtGrade;
@property (strong, nonatomic) IBOutlet UITextField *txtSchool;
@property (strong, nonatomic) IBOutlet UITextField *txtCode;
@property (strong, nonatomic) IBOutlet UITextField *txtSearch;
@property (weak, nonatomic) IBOutlet UITextField *txtUploadCSV;
@property (strong, nonatomic) IBOutlet UITextField *txtSort;
@property (strong, nonatomic) IBOutlet UIView *headerView;
- (IBAction)actionAdd:(id)sender;
- (IBAction)txtCodeDismiss:(id)sender;
- (IBAction)txtSchoolDismiss:(id)sender;
- (IBAction)txtCodesDismiss:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *userTypeLabel;
@property (weak, nonatomic) IBOutlet UIButton *inviteButton;
@property (weak, nonatomic) IBOutlet UIButton *addStudentButton;

@property (strong, nonatomic) NSDictionary *data;



@end
