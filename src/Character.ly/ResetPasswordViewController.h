//
//  ResetPasswordViewController.h
//  Character.ly
//
//  Created by Adeel Nasir on 03/11/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResetPasswordViewController : UIViewController{
    
}
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *authToken;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
@property (strong, nonatomic) IBOutlet UITextField *txtReType;
- (IBAction)actionReset:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightFromTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingFromSide;

@end
