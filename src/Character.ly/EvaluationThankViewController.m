//
//  EvaluationThankViewController.m
//  Character.ly
//
//  Created by Adeel Nasir on 26/09/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "EvaluationThankViewController.h"
#import "NavViewController.h"

@interface EvaluationThankViewController ()

@end

@implementation EvaluationThankViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.hidden = YES;
    NavViewController *myViewController2 = [[NavViewController alloc] init];
    
    //  [myViewController2.view addSubView:mySubView];
    // add any other views to myViewController2's view
    
    [self.view addSubview:myViewController2];
    
    myViewController2.frame = self.navigationController.navigationBar.frame;
    myViewController2.frame = CGRectMake(myViewController2.frame.origin.x, [UIApplication sharedApplication].statusBarFrame.size.height, myViewController2.frame.size.width, myViewController2.frame.size.height);
    
    [myViewController2 hideBackButton];
}
- (IBAction)returnToLessonsTap:(id)sender {
    [self.navigationController popToViewController:self.navigationController.viewControllers[self.navigationController.viewControllers.count-3] animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
