//
//  ForgotPasswordViewController.m
//  Character.ly
//
//  Created by Adeel Nasir on 08/08/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "MessageWindow.h"
#import "DGActivityIndicatorView.h"
#import "ResetPasswordViewController.h"


@interface ForgotPasswordViewController (){
    NSMutableArray *loginResponse;
    UIView *dimView;
    DGActivityIndicatorView *activityIndicatorView;
    NSString *email;
    NSString *authToken;
}

@end

@implementation ForgotPasswordViewController
#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     self.navigationController.navigationBar.hidden = YES;
    
    [self.view setBackgroundColor: RGB(247, 248, 250)];
    [self.btnChange setBackgroundColor:RGB(0, 188, 240)];
    [self.txtPassword setBackgroundColor:RGB(251, 251, 251)];
    [self.txtRetype setBackgroundColor:RGB(251, 251, 251)];
    [self.lblForgot setTextColor:RGB(107, 124, 147)];
    [self.lblDesc setTextColor:RGB(107, 124, 147)];
    [self.lblPassword setTextColor:RGB(107, 124, 147)];
    [self.lblRetype setTextColor:RGB(107, 124, 147)];
    
    
    UIView *padView = [[UIView alloc] initWithFrame:CGRectMake(10, 110, 10, 0)];
    _txtPassword.leftView = padView;
    _txtPassword.leftViewMode = UITextFieldViewModeAlways;
    
    
    UIView *padView1 = [[UIView alloc] initWithFrame:CGRectMake(10, 110, 10, 0)];
    _txtRetype.leftView = padView1;
    _txtRetype.leftViewMode = UITextFieldViewModeAlways;
    
    _txtPassword.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtPassword.layer.borderWidth = 1.0f;
    
    _txtRetype.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtRetype.layer.borderWidth = 1.0f;
    
    
    _btnLogin.layer.borderColor = [UIColor whiteColor].CGColor;
    _btnLogin.layer.borderWidth = 1.0f;
    
    _btnCreate.layer.borderColor = [UIColor whiteColor].CGColor;
    _btnCreate.layer.borderWidth = 1.0f;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actionBack:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)changePassword:(id)sender {
    NSLog(@"in action");
    NSMutableArray *ns = [self Login:_txtPassword.text];
}

- (void) returnTextColor {
    
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds *NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        _txtPassword.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
        _txtPassword.backgroundColor = [self colorWithHexString:@"f7f8fa"];
    });
}

- (void) displayChangeError:(UITextField *)errorTextField {
    errorTextField.layer.borderColor = [self colorWithHexString:@"e82a16"].CGColor;
    errorTextField.backgroundColor = [self colorWithHexString:@"ffc0ba"];
}

-(NSMutableArray*)Login:(NSString*)username {
    BOOL isValid = [self validEmail:username];
    if([username isEqualToString:@""]){
        [self showErrorMessage:@"Please fill in email field"];
        [self displayChangeError:_txtPassword];
        [self returnTextColor];
    } else if(!isValid){
        [self showErrorMessage:@"Please enter valid email address"];
        [self displayChangeError:_txtPassword];
        [self returnTextColor];
    } else {
        [self showLoader];
        loginResponse = NULL;
        loginResponse = [[NSMutableArray alloc]init];

        
        NSDictionary *dataToSend = @{
                                     @"emailAddress" : username
                                     };
        email = username;
        
        [NetworkHelper placePostRequest:dataToSend relativeUrl:@"services/app/Account/ResetPasswordRequest" withSuccess:^(NSDictionary *jsonObject) {
            // cache data
             [self showErrorMessage:@"Successfully sent recovery email"];
            [self hideLoader];
            // TODO launch reset pass
            
            NSLog(@"This is response jsonData: %@", jsonObject);
            authToken = jsonObject[@"result"][@"authToken"];
            [self performSegueWithIdentifier:@"resetpass" sender:self];
           
        } andFailure:^(NSString *string) {
            NSLog(@"resetFailure:");
            [self showErrorMessage:string];
            [self displayChangeError:_txtPassword];
            [self returnTextColor];
        }];
    }
  
    return loginResponse;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"resetpass"])
    {
        // Get reference to the destination view controller
        ResetPasswordViewController *vc = [segue destinationViewController];
        vc.email = email;
        vc.authToken = authToken;
    }
}

- (void)showErrorMessage:(NSString *)string {
    
    [dimView removeFromSuperview];
    [activityIndicatorView stopAnimating];
    MessageWindow *message = [[MessageWindow alloc] init];
    
    //  [myViewController2.view addSubView:mySubView];
    // add any other views to myViewController2's view
    [message removeFromSuperview];
    
    [self.view addSubview:message];
    
    [message setMessage:string];
    
    [message setAlpha:1.0f];
    
    //fade in
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionTransitionFlipFromTop
                     animations:^{
                         
                         [message setAlpha:1.0f];
                         message.frame = CGRectMake(_mainView.frame.origin.x + _leadingFromSide.constant, _mainView.frame.origin.y + _heightFromTop.constant, _txtPassword.frame.size.width, _txtPassword.frame.size.height);
                         
                     } completion:^(BOOL finished) {
                         
                         //fade out
                         [UIView animateWithDuration:0.3
                                               delay:2.0
                                             options:UIViewAnimationOptionTransitionCurlUp
                                          animations:^{
                                              
                                              [message setAlpha:0.0f];
                                              //                                 message.frame = CGRectMake(30.0, 140.0, 350, 0);
                                              //
                                              
                                              
                                          } completion:^(BOOL finished) {
                                              [message setMessage:@""];
                                          }];
                         
                     }];
}

- (void)showLoader {
    if (!dimView) {
        dimView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
        dimView.backgroundColor = [UIColor blackColor];
        dimView.alpha = 0.7f;
        dimView.userInteractionEnabled = NO;
    }
    [self.view addSubview:dimView];
    CGRect newFrame = dimView.frame;
    
    newFrame.size.width = 1200;
    newFrame.size.height = 1500;
    [dimView setFrame:newFrame];
    [dimView setCenter:CGPointMake(187.0f, 333.0f)];
    
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeLineScale tintColor:[UIColor whiteColor] size:40.0f];
    activityIndicatorView.center = self.view.center;
    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
}

- (void)hideLoader {
    [dimView removeFromSuperview];
    [activityIndicatorView stopAnimating];
}

- (BOOL) validEmail:(NSString*) emailString {
    
    if([emailString length]==0){
        return NO;
    }
    
    NSString *regExPattern = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    
    NSLog(@"%i", regExMatches);
    if (regExMatches == 0) {
        return NO;
    } else {
        return YES;
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [_txtPassword resignFirstResponder];
    
    return false;
}
- (IBAction)emailDismiss:(id)sender {
    [_txtPassword endEditing:YES];
    [_txtPassword resignFirstResponder];
}
@end
