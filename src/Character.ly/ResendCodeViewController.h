//
//  ResendCodeViewController.h
//  Character.ly
//
//  Created by Adeel Nasir on 10/08/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResendCodeViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblRecover;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet UILabel *lblEnterCode;
@property (weak, nonatomic) IBOutlet UILabel *lblNewCode;
- (IBAction)actionBack:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnEnterCode;
@property (strong, nonatomic) IBOutlet UIButton *btnNewCode;
- (IBAction)enterCode:(id)sender;
- (IBAction)sendAction:(id)sender;
- (IBAction)emailDismiss:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightFromTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingFromSide;

@end
