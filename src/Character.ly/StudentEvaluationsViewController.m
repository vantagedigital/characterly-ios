//
//  StudentEvaluationsViewController.m
//  Character.ly
//
//  Created by Adeel Nasir on 10/09/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "StudentEvaluationsViewController.h"
#import "NavViewController.h"
#import "BadgeDetailView.h"
#import "EditStudentViewController.h"

@interface StudentEvaluationsViewController ()
@property (weak, nonatomic) IBOutlet UILabel *header;
@property (weak, nonatomic) IBOutlet UILabel *classPath;

@property (weak, nonatomic) IBOutlet UILabel *gradeLabel;
@property (weak, nonatomic) IBOutlet UILabel *homeroomLabel;
@property (weak, nonatomic) IBOutlet UILabel *dobLabel;
@property (weak, nonatomic) IBOutlet UILabel *genderLabel;
@property (weak, nonatomic) IBOutlet UILabel *schoolAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *userTypeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UIButton *inviteButton;
@property (weak, nonatomic) IBOutlet UIButton *addStudentButton;

@property (weak, nonatomic) IBOutlet UILabel *fatherNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *fatherAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *fatherEmailLabel;
@property (weak, nonatomic) IBOutlet UILabel *fatherNumberLabel;

@property (weak, nonatomic) IBOutlet UILabel *motherNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *motherAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *motherEmailLabel;
@property (weak, nonatomic) IBOutlet UILabel *motherNumberLabel;

@property (weak, nonatomic) IBOutlet UILabel *studentName;
@property (weak, nonatomic) IBOutlet UILabel *studentId;
@property (strong, nonatomic) NSMutableArray *evaluations;


@end

@implementation StudentEvaluationsViewController
#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    
    NavViewController *myViewController2 = [[NavViewController alloc] initForAutoLayout];
    [self.view addSubview:myViewController2];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:[UIApplication sharedApplication].statusBarFrame.size.height];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
    [myViewController2 autoSetDimension:ALDimensionHeight toSize:200];
    myViewController2.parent = self;
    
    NSDictionary *userInfo = [[UserCache sharedInstance] getUserInfo];
    NSLog(@"user data: %@", userInfo);
    
    if(![userInfo[@"imagePath"] isKindOfClass:[NSNull class]]){
        [self.userImageView sd_setImageWithURL:[NSURL URLWithString:userInfo[@"imagePath"]]];
    }
    
    if(![userInfo[@"educator"] isKindOfClass:[NSNull class]]){
        self.userTypeLabel.text = @"Teacher";
    }else{
        self.userTypeLabel.text = @"Student";
    }
    NSString *name = concat(userInfo[@"name"], @" ");
    self.userNameLabel.text = concat(name, userInfo[@"surname"]);
    
    _addStudentButton.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _addStudentButton.layer.borderWidth = 1.0f;
    
    _inviteButton.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _inviteButton.layer.borderWidth = 1.0f;
    
    self.evaluations = [NSMutableArray array];

    [self getClassroomEvaluations];
    [self getParentData:self.studentData];
    [self getProfileImage:self.studentData];
    
    NSLog(@"current student data: %@", self.studentData);
    
    NSString *fullName = [NSString stringWithFormat:@"%@ %@", self.studentData[@"name"], self.studentData[@"surname"]];
    self.header.text = fullName;
    self.classPath.text = [NSString stringWithFormat:@"Classroom / %@", fullName];
    
    
    
    self.gradeLabel.text = @"3rd Grade";
    self.homeroomLabel.text = @"Green Giants";
    self.dobLabel.text = @"04/06/2009";
    self.genderLabel.text = @"Female";
    self.schoolAddressLabel.text = @"";
    
    @try {
        [self resetUI];
        if (!IsEmpty(self.studentData[@"parents"])) {
            [self configureFather:((NSArray *)self.studentData[@"parents"])[0]];
            
            if (((NSArray *)self.studentData[@"parents"]).count > 1) {
                [self configureMother:((NSArray *)self.studentData[@"parents"])[1]];
            }
        }
    } @catch (NSException *e) {
        
    }
    
    self.emailLabel.text = self.studentData[@"emailAddress"];
    self.phoneNumberLabel.text = @"555-555-55555";
    
    self.studentName.text = self.studentData[@"fullName"];
    self.studentName.text = concat(@"Name: ", self.studentData[@"fullName"]);
    self.studentId.text = concat(@"Student Id: ", [self.studentData[@"userId"]stringValue]);
    
    NSLog(@"This is userId: %@", self.studentData[@"userId"]);
    
    //  [myViewController2.view addSubView:mySubView];
    // add any other views to myViewController2's view
    
    // Do any additional setup after loading the view
    [_scroller setScrollEnabled:YES];
    [_scroller setContentSize:CGSizeMake(375, 4550)];
    
    UIView *padView = [[UIView alloc] initWithFrame:CGRectMake(10, 110, 10, 0)];
    _txtEvaluation.leftView = padView;
    _txtEvaluation.leftViewMode = UITextFieldViewModeAlways;
    
    
    //    UIImage *image = [UIImage imageNamed:@"characterly.png"];
    //    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:image];
    _tblNotifications.hidden = YES;
    
    // [self.topView setBackgroundColor:RGB(240, 85, 61)];
    
    //_btnEdit.layer.borderColor = [UIColor colorWithRed:34 green:34 blue:34 alpha:1].CGColor;
    _btnEdit.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _btnEdit.layer.borderWidth = 1.0f;
    
    _fatherView.layer.borderWidth = 1.0f;
    _fatherView.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    
    
    
    _motherView.layer.borderWidth = 1.0f;
    _motherView.layer.borderColor =  [self colorWithHexString:@"e6e6e6"].CGColor;
    //  box.layer.borderColor = [UIColor colorWithRed:247.0 green:248.0 blue:250.0 alpha:0.2].CGColor;
    
    //    UIColor *borderColor = [UIColor colorWithRed:230.0 green:230.0 blue:230.0 alpha:1.0];
    //    [_motherView.layer setBorderColor:borderColor.CGColor];
    
    [_tblNotifications setShowsHorizontalScrollIndicator:NO];
    [_tblNotifications setShowsVerticalScrollIndicator:NO];
    
    [self.tblNotifications setSeparatorColor:[UIColor clearColor]];
    
    
    
    
    
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]
                                             initWithTarget:self action:@selector(ClickEventOnImage:)];
    [tapRecognizer setNumberOfTouchesRequired:1];
    [tapRecognizer setDelegate:self];
    //Don't forget to set the userInteractionEnabled to YES, by default It's NO.
    _badge1.userInteractionEnabled = YES;
    [_badge1 addGestureRecognizer:tapRecognizer];
    
    UITapGestureRecognizer *tapRecognizer2 = [[UITapGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(ClickEventOnImage:)];
    [tapRecognizer2 setNumberOfTouchesRequired:1];
    [tapRecognizer2 setDelegate:self];
    //Don't forget to set the userInteractionEnabled to YES, by default It's NO.
    _badge2.userInteractionEnabled = YES;
    [_badge2 addGestureRecognizer:tapRecognizer2];
    
    UITapGestureRecognizer *tapRecognizer4 = [[UITapGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(ClickEventOnImage:)];
    [tapRecognizer4 setNumberOfTouchesRequired:1];
    [tapRecognizer4 setDelegate:self];
    //Don't forget to set the userInteractionEnabled to YES, by default It's NO.
    _badge4.userInteractionEnabled = YES;
    [_badge4 addGestureRecognizer:tapRecognizer4];
    
    
    UITapGestureRecognizer *tapRecognizer3 = [[UITapGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(ClickEventOnImage:)];
    [tapRecognizer3 setNumberOfTouchesRequired:1];
    [tapRecognizer3 setDelegate:self];
    //Don't forget to set the userInteractionEnabled to YES, by default It's NO.
    _badge3.userInteractionEnabled = YES;
    [_badge3 addGestureRecognizer:tapRecognizer3];
    
    
    
    
    
    
}

- (void)getClassroomEvaluations {
    NSString *url = concat(@"services/app/Evaluation/GetByClassroomId?id=", self.classroomData[@"id"]);
    [NetworkHelper placeGetRequest:nil relativeUrl:url withSuccess:^(NSDictionary *jsonObject) {
        self.evaluations = jsonObject[@"result"];
        [self.tblNotifications reloadData];
        
    } andFailure:^(NSString *string) {
        NSLog(@"getStudents Failure:%@", string);
        //               [self showErrorMessage:string];
    }];
}

- (void)getParentData:(NSDictionary *)studentData {
    
    NSString *url = concat(@"services/app/Parent/GetByBranchId?id=", studentData[@"branchId"]);
    [NetworkHelper placeGetRequest:nil relativeUrl:url withSuccess:^(NSDictionary *jsonObject) {
        
        NSLog(@"This is parent data: %@", jsonObject);
        
    } andFailure:^(NSString *string) {
        NSLog(@"getStudents Failure:%@", string);
        //               [self showErrorMessage:string];
    }];
}

- (void)getProfileImage:(NSDictionary *)studentData {
    
    NSNull *nullImage = [[NSNull alloc] init];
    if(studentData[@"profilePicture"] == nullImage){
        NSLog(@"There is no profile image.");
    }else{
        if(studentData[@"profilePicture"]){
            NSLog(@"profile image path: %@", studentData[@"profilePicture"][@"path"]);
            
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:studentData[@"profilePicture"][@"path"]]];
                if(data == nil)
                    return;
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.profileImage.image = [UIImage imageWithData:data];
                });
            });
        }
    }
}

- (void)resetUI {
    self.fatherNameLabel.text = @"";
    self.fatherAddressLabel.text = @"";
    self.fatherEmailLabel.text = @"";
    self.fatherNumberLabel.text = @"";
    
    self.motherNameLabel.text = @"";
    self.motherAddressLabel.text = @"";
    self.motherEmailLabel.text = @"";
    self.motherNumberLabel.text = @"";
}

- (void)configureFather:(NSDictionary *)father {
    self.fatherNameLabel.text = father[@"fullName"];
    self.fatherAddressLabel.text = @"";
    self.fatherEmailLabel.text = father[@"emailAddress"];
    self.fatherNumberLabel.text = @"";
}

- (void)configureMother:(NSDictionary *)mother {
    self.motherNameLabel.text = mother[@"fullName"];
    self.motherAddressLabel.text = @"";
    self.motherEmailLabel.text = mother[@"emailAddress"];
    self.motherNumberLabel.text = @"";
}

-(void) ClickEventOnImage:(id) sender
{
    NSLog(@"badge clicked");
    BadgeDetailView *myViewController2 = [[BadgeDetailView alloc] init];
    
    //  [myViewController2.view addSubView:mySubView];
    // add any other views to myViewController2's view
    
    [self.view addSubview:myViewController2];
    
    myViewController2.frame = CGRectMake(0, 00, myViewController2.frame.size.width, 730);
    
}
-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *) cell forRowAtIndexPath:(NSIndexPath *)indexPath // replace "postTableViewCell" with your cell
{
    
    CGRect sizeRect = [UIScreen mainScreen].applicationFrame;
    NSInteger separatorHeight = 10;
    UIView * additionalSeparator = [[UIView alloc] initWithFrame:CGRectMake(0,cell.frame.size.height-separatorHeight,sizeRect.size.width,separatorHeight)];
    
    additionalSeparator.backgroundColor = [self colorWithHexString:@"F7F8FA"];
    [cell addSubview:additionalSeparator];
    
}


- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.evaluations.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger row = indexPath.row;
   
//    if(indexPath.row == 0){
//        return 20;
//    }
//    else {
        return 220;
//    }
   
    
    
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
        static NSString *CellIdentifier = @"Cell1";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        // Configure the cell...
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
    
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:902];
    UILabel *dateLabel = (UILabel *)[cell viewWithTag:903];
    UILabel *studentNameLabel = (UILabel *)[cell viewWithTag:904];
    
    titleLabel.text = self.evaluations[indexPath.row][@"name"];
    dateLabel.text = @"11/23/2017";
    studentNameLabel.text = @"";
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"evaluationsreview" sender:self];
}


 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     
     if ([[segue identifier] isEqualToString:@"editProfile"]) {
         // Get reference to the destination view controller
         EditStudentViewController *vc = [segue destinationViewController];
         vc.studentData = self.studentData;
     }
     if ([[segue identifier] isEqualToString:@"evaluationsreview"]) {
         // Get reference to the destination view controller
         
     }
 }


- (IBAction)openProfile:(id)sender {
    _tblNotifications.hidden = YES;
    _scroller.scrollEnabled = YES;
    [self.btnProfile setBackgroundColor:RGB(255, 255, 255)];
    [self.btnNotifications setBackgroundColor:RGB(96, 96, 171)];
    [_btnProfile setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_btnNotifications setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (IBAction)openNotifications:(id)sender {
    _tblNotifications.hidden = NO;
    _scroller.scrollEnabled = NO;
    [self.btnNotifications setBackgroundColor:RGB(255, 255, 255)];
    [self.btnProfile setBackgroundColor:RGB(96, 96, 171)];
    
    [_btnNotifications setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_btnProfile setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}
@end
