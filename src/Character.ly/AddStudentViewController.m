//
//  AddStudentViewController.m
//  Character.ly
//
//  Created by Macbook Pro on 02/10/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "AddStudentViewController.h"
#import "NavViewController.h"
#import "MessageWindow.h"
#import "DGActivityIndicatorView.h"
#import "DropDownCell.h"

@interface AddStudentViewController (){
    NSMutableArray *_pickerData;
    UIView *dimView;
    DGActivityIndicatorView *activityIndicatorView;
    NSString *classID;
}

@property (assign, nonatomic) BOOL isEducator;
@property (strong, nonatomic) NSMutableArray *classrooms;

@end

@implementation AddStudentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    
    NavViewController *myViewController2 = [[NavViewController alloc] initForAutoLayout];
    [self.view addSubview:myViewController2];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:[UIApplication sharedApplication].statusBarFrame.size.height];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
    [myViewController2 autoSetDimension:ALDimensionHeight toSize:200];
    myViewController2.parent = self;
    
    NSLog(@"self.data: %@", self.data);
    
    NSString *classroomName = concat(@"Classrooms/", self.data[@"name"]);
    self.classroomNameLabel.text = concat(classroomName, @"/Add Student");
    
    NSDictionary *userInfo = [[UserCache sharedInstance] getUserInfo];
    NSLog(@"user data: %@", userInfo);
    
    if(![userInfo[@"imagePath"] isKindOfClass:[NSNull class]]){
        [self.userImageView sd_setImageWithURL:[NSURL URLWithString:userInfo[@"imagePath"]]];
    }
    
    if(![userInfo[@"educator"] isKindOfClass:[NSNull class]]){
        self.userTypeLabel.text = @"Teacher";
    }else{
        self.userTypeLabel.text = @"Student";
    }
    NSString *name = concat(userInfo[@"name"], @" ");
    self.userNameLabel.text = concat(name, userInfo[@"surname"]);
    
    _addStudentButton.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _addStudentButton.layer.borderWidth = 1.0f;
    
    _inviteButton.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _inviteButton.layer.borderWidth = 1.0f;
    
    _pickerData = [[NSMutableArray alloc]init];
    
    _txtClassName.delegate = self;
    
    self.isEducator = ![[[UserCache sharedInstance] getUserInfo][@"educator"] isKindOfClass:[NSNull class]];
    
    if (self.isEducator) {
        self.classrooms = [[UserCache sharedInstance] getUserInfo][@"educator"][@"classrooms"];
    } else {
        self.classrooms = [[UserCache sharedInstance] getUserInfo][@"student"][@"classrooms"];
    }
    
    for(int i = 0; i < self.classrooms.count;i++){
        [_pickerData addObject:[self.classrooms[i] valueForKey:@"name"]];
    }
    
    _txtClassName.text =[_pickerData objectAtIndex:0];
    
    _btnAdd.layer.borderColor = [UIColor grayColor].CGColor;
    _btnAdd.layer.borderWidth = 0.5f;
    _btnCancel.layer.borderColor = [UIColor grayColor].CGColor;
    _btnCancel.layer.borderWidth = 0.5f;
    
    _txtClassName.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtClassName.layer.borderWidth = 1.0f;
    
    _txtGrade.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtGrade.layer.borderWidth = 1.0f;
    
    _txtSchool.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtSchool.layer.borderWidth = 1.0f;
    
    _txtSearch.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtSearch.layer.borderWidth = 1.0f;
    
    _txtSort.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtSort.layer.borderWidth = 1.0f;
    
    _txtCode.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtCode.layer.borderWidth = 1.0f;
    
    
    self.selectClassTable.dataSource = self;
    self.selectClassTable.delegate = self;
    self.selectClassTable.hidden = YES;
    [self.selectClassButton.layer setBorderWidth:0.5];
    [self.selectClassButton.layer setBorderColor:[[UIColor grayColor] CGColor]];
    
    
    UIView *padView = [[UIView alloc] initWithFrame:CGRectMake(10, 110, 10, 0)];
    _txtClassName.leftView = padView;
    _txtClassName.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *padView1 = [[UIView alloc] initWithFrame:CGRectMake(10, 110, 10, 0)];
    _txtGrade.leftView = padView1;
    _txtGrade.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *padView2 = [[UIView alloc] initWithFrame:CGRectMake(10, 110, 10, 0)];
    _txtSchool.leftView = padView2;
    _txtSchool.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *padView3 = [[UIView alloc] initWithFrame:CGRectMake(10, 110, 10, 0)];
    _txtCode.leftView = padView3;
    _txtCode.leftViewMode = UITextFieldViewModeAlways;
    
}

- (void)showLoader {
    if (!dimView) {
        dimView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
        dimView.backgroundColor = [UIColor blackColor];
        dimView.alpha = 0.7f;
        dimView.userInteractionEnabled = NO;
    }
    [self.view addSubview:dimView];
    CGRect newFrame = dimView.frame;
    
    newFrame.size.width = 1200;
    newFrame.size.height = 1500;
    [dimView setFrame:newFrame];
    [dimView setCenter:CGPointMake(187.0f, 333.0f)];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeLineScale tintColor:[UIColor whiteColor] size:40.0f];
    activityIndicatorView.center = self.view.center;
    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
}

- (void)hideLoader {
    [dimView removeFromSuperview];
    [activityIndicatorView stopAnimating];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_txtGrade resignFirstResponder];
    [_txtSchool resignFirstResponder];
    [_txtCode resignFirstResponder];
    
    return YES;
}
-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [_txtGrade endEditing:YES];
    [_txtSchool endEditing:YES];
    [_txtCode endEditing:YES];
    [_txtGrade resignFirstResponder];
    [_txtSchool resignFirstResponder];
    [_txtCode resignFirstResponder];
    
    
    return false;
}


-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (tableView == _selectClassTable){
        return _pickerData.count;
    }else{
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == _selectClassTable){
        return 40;
    }else{
        return 0;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == _selectClassTable){
        
        DropDownCell *cell = [tableView dequeueReusableCellWithIdentifier:@"inviteClassCell"];
        cell.dropDownButton.tag = indexPath.row;
        [cell.dropDownButton setTitle:_pickerData[indexPath.row] forState:normal];
        [cell.dropDownText setFont:[UIFont systemFontOfSize:17]];
        
        return cell;
    }else{
        return nil;
    }
    
}

- (void)inviteButtonTap {
    [self performSegueWithIdentifier:@"invite" sender:self];
}

- (void)showErrorMessage:(NSString *)string {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Character.ly"
                                                    message:string
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (IBAction)actionAdd:(id)sender {
    NSString *branchId = [[UserCache sharedInstance] getUserInfo][@"branchId"];
    NSString *type = @"3";
    if (!classID && !IsEmpty(self.classrooms)) {
        classID = self.classrooms[0][@"id"];
    }
    NSString *typeId = classID;
    NSString *name = _txtGrade.text;
    NSString *surName = _txtSchool.text;
    NSString *email = _txtCode.text;
    
    if([name isEqualToString:@""]){
        [self showErrorMessage:@"Please enter name"];
        return;
    } else if([surName isEqualToString:@""]){
        [self showErrorMessage:@"Please enter surname"];
        return;
    } else if([email isEqualToString:@""] || ![email containsString:@"@"]){
        [self showErrorMessage:@"Please enter valid email"];
        return;
    }
    
    
    NSDictionary *dataToSend = @{
                                 @"BranchId" : branchId,
                                 @"Type" : type,
                                 @"TypeId" : typeId,
                                 @"EmailAddress" : email,
                                 @"Name" : name,
                                 @"Surname" : surName};
    
    if (IsEmpty(typeId)) {
        [self showErrorMessage:@"No classrooms to add student too"];
    } else {
        [self showLoader];
        [NetworkHelper placePostRequest:dataToSend relativeUrl:@"services/app/Invite/Create" withSuccess:^(NSDictionary *jsonObject) {
            [self hideLoader];
            [self showErrorMessage:@"Successfully Added Student"];
            [self.navigationController popViewControllerAnimated:YES];
            
        } andFailure:^(NSString *string) {
            [self hideLoader];
            NSLog(@"loginFailure:");
            [self showErrorMessage:string];
        }];
    }
}

- (IBAction)selectClass:(id)sender {
    
    if(self.selectClassTable.hidden == YES){
        
        [UIView transitionWithView: self.selectClassTable
                          duration: 0.35f
                           options: UIViewAnimationOptionTransitionCurlDown
                        animations: ^(void)
         {
             self.selectClassTable.hidden = NO;
         }
                        completion: nil];
        
    }else{
        
        [UIView transitionWithView: self.selectClassTable
                          duration: 0.35f
                           options: UIViewAnimationOptionTransitionCurlUp
                        animations: ^(void)
         {
             self.selectClassTable.hidden = YES;
         }
                        completion: nil];
    }
}

- (IBAction)selectClassCell:(UIButton *)sender {
    
    self.txtClassName.text = _pickerData[sender.tag];
    classID = self.classrooms[sender.tag][@"id"];
    
    [UIView transitionWithView: self.selectClassTable
                      duration: 0.35f
                       options: UIViewAnimationOptionTransitionCurlUp
                    animations: ^(void)
     {
         self.selectClassTable.hidden = YES;
     }
                    completion: nil];
    
    sender.backgroundColor = [UIColor whiteColor];
    [sender setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
}

- (IBAction)changeBackgroundColor:(UIButton *)sender {
    
    sender.backgroundColor = [UIColor colorWithRed:95.0f/255.0f green:96.0f/255.0f blue:171.0f/255.0f alpha:1.0f];
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
}


- (IBAction)txtCodeDismiss:(id)sender {
    [_txtGrade resignFirstResponder];
}

- (IBAction)txtSchoolDismiss:(id)sender {
    [_txtSchool resignFirstResponder];
}

- (IBAction)txtCodesDismiss:(id)sender {
    [_txtCode resignFirstResponder];
}

@end


