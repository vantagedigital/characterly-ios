//
//  NetworkHelper.m
//  Character.ly
//
//  Created by Jesse A on 10/11/17.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "NetworkHelper.h"

@implementation NetworkHelper

+ (void)placePostRequest:(NSDictionary *)data
            relativeUrl:(NSString *)relativeUrl
            withSuccess:(CompletionJsonBlock)successCallback
             andFailure:(CompletionStringBlock)failureCallback {
    
    [NetworkHelper placePostRequestWithURL:concat(URL_BASE, relativeUrl)
                                 withData:data
                              withHandler:^(NSURLResponse *response, NSData *rawData, NSError *error) {
                                  NSString *string = [[NSString alloc] initWithData:rawData
                                                                           encoding:NSUTF8StringEncoding];
                                  NSLog(@"Data is %@", data);
                                  NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
                                  NSInteger code = [httpResponse statusCode];
                                  NSLog(@"%ld", (long)code);
                                  
                                  if (!(code >= 200 && code < 300)) {
                                      NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:[string dataUsingEncoding:NSUTF8StringEncoding]
                                                                                                 options:0 error:NULL];
                                      NSMutableArray *getdata=[[NSMutableArray alloc]init];
                                      getdata=[jsonObject objectForKey:@"error"];
                                      
                                      
                                      NSString *string = [getdata valueForKey:@"details"];
                                      NSLog(@"output %@ ", string);
                                      if (!string || [string isKindOfClass:[NSNull class]]) {
                                          string = concat(@"Error with code ", @(code).stringValue);
                                      }
                                      if (failureCallback) {
                                          failureCallback(string);
                                      }
                                  } else {
                                      NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:[string dataUsingEncoding:NSUTF8StringEncoding] options:0 error:NULL];
                                      
                                      if (successCallback) {
                                          successCallback(jsonObject);
                                      }
                                  }
                              }];
}

+ (void)placePutRequest:(NSDictionary *)data
             relativeUrl:(NSString *)relativeUrl
             withSuccess:(CompletionJsonBlock)successCallback
              andFailure:(CompletionStringBlock)failureCallback {
    
    [NetworkHelper placePutRequestWithURL:concat(URL_BASE, relativeUrl)
                                  withData:data
                               withHandler:^(NSURLResponse *response, NSData *rawData, NSError *error) {
                                   NSString *string = [[NSString alloc] initWithData:rawData
                                                                            encoding:NSUTF8StringEncoding];
                                   NSLog(@"Data is %@", data);
                                   NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
                                   NSInteger code = [httpResponse statusCode];
                                   NSLog(@"%ld", (long)code);
                                   
                                   if (!(code >= 200 && code < 300)) {
                                       NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:[string dataUsingEncoding:NSUTF8StringEncoding]
                                                                                                  options:0 error:NULL];
                                       NSMutableArray *getdata=[[NSMutableArray alloc]init];
                                       getdata=[jsonObject objectForKey:@"error"];
                                       
                                       
                                       NSString *string = [getdata valueForKey:@"details"];
                                       NSLog(@"output %@ ", string);
                                       if (!string || [string isKindOfClass:[NSNull class]]) {
                                           string = concat(@"Error with code ", @(code).stringValue);
                                       }
                                       if (failureCallback) {
                                           failureCallback(string);
                                       }
                                   } else {
                                       NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:[string dataUsingEncoding:NSUTF8StringEncoding] options:0 error:NULL];
                                       
                                       if (successCallback) {
                                           successCallback(jsonObject);
                                       }
                                   }
                               }];
}

+ (void)placeDeleteRequest:(NSDictionary *)data
            relativeUrl:(NSString *)relativeUrl
            withSuccess:(CompletionJsonBlock)successCallback
             andFailure:(CompletionStringBlock)failureCallback {
    
    [NetworkHelper placeDeleteRequestWithURL:concat(URL_BASE, relativeUrl)
                                 withData:data
                              withHandler:^(NSURLResponse *response, NSData *rawData, NSError *error) {
                                  NSString *string = [[NSString alloc] initWithData:rawData
                                                                           encoding:NSUTF8StringEncoding];
                                  NSLog(@"Data is %@", data);
                                  NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
                                  NSInteger code = [httpResponse statusCode];
                                  NSLog(@"%ld", (long)code);
                                  
                                  if (!(code >= 200 && code < 300)) {
                                      NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:[string dataUsingEncoding:NSUTF8StringEncoding]
                                                                                                 options:0 error:NULL];
                                      NSMutableArray *getdata=[[NSMutableArray alloc]init];
                                      getdata=[jsonObject objectForKey:@"error"];
                                      
                                      
                                      NSString *string = [getdata valueForKey:@"details"];
                                      NSLog(@"output %@ ", string);
                                      if (!string || [string isKindOfClass:[NSNull class]]) {
                                          string = concat(@"Error with code ", @(code).stringValue);
                                      }
                                      if (failureCallback) {
                                          failureCallback(string);
                                      }
                                  } else {
                                      NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:[string dataUsingEncoding:NSUTF8StringEncoding] options:0 error:NULL];
                                      
                                      if (successCallback) {
                                          successCallback(jsonObject);
                                      }
                                  }
                              }];
}

+ (void)placePostRequestWithURL:(NSString *)action withData:(NSDictionary *)dataToSend withHandler:(void (^)(NSURLResponse *response, NSData *data, NSError *error))ourBlock {
    NSString *urlString = [NSString stringWithFormat:@"%@", action];
    NSLog(@"%@", urlString);
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSError *error;
    
    if (!dataToSend) {
        dataToSend = [NSMutableDictionary dictionary];
    }
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataToSend options:0 error:&error];
    
    NSString *jsonString;
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        NSData *requestData = [NSData dataWithBytes:[jsonString UTF8String] length:[jsonString lengthOfBytesUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        if ([[UserCache sharedInstance] isLoggedIn]) {
            [request setValue:[[UserCache sharedInstance] getAccessToken] forHTTPHeaderField:@"Authorization"];
        }
        [request setHTTPBody: requestData];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:ourBlock];
    }
}

+ (void)placePutRequestWithURL:(NSString *)action withData:(NSDictionary *)dataToSend withHandler:(void (^)(NSURLResponse *response, NSData *data, NSError *error))ourBlock {
    NSString *urlString = [NSString stringWithFormat:@"%@", action];
    NSLog(@"%@", urlString);
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSError *error;
    
    if (!dataToSend) {
        dataToSend = [NSMutableDictionary dictionary];
    }
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataToSend options:0 error:&error];
    
    NSString *jsonString;
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        NSData *requestData = [NSData dataWithBytes:[jsonString UTF8String] length:[jsonString lengthOfBytesUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPMethod:@"PUT"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        if ([[UserCache sharedInstance] isLoggedIn]) {
            [request setValue:[[UserCache sharedInstance] getAccessToken] forHTTPHeaderField:@"Authorization"];
        }
        [request setHTTPBody: requestData];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:ourBlock];
    }
}

+ (void)placeDeleteRequestWithURL:(NSString *)action withData:(NSDictionary *)dataToSend withHandler:(void (^)(NSURLResponse *response, NSData *data, NSError *error))ourBlock {
    NSString *urlString = [NSString stringWithFormat:@"%@", action];
    NSLog(@"%@", urlString);
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSError *error;
    if (!dataToSend) {
        dataToSend = [NSMutableDictionary dictionary];
    }
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataToSend options:0 error:&error];
    
    NSString *jsonString;
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        NSData *requestData = [NSData dataWithBytes:[jsonString UTF8String] length:[jsonString lengthOfBytesUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPMethod:@"DELETE"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        if ([[UserCache sharedInstance] isLoggedIn]) {
            [request setValue:[[UserCache sharedInstance] getAccessToken] forHTTPHeaderField:@"Authorization"];
        }
        [request setHTTPBody: requestData];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:ourBlock];
    }
}

+ (void)placeGetRequest:(NSDictionary *)data
                    relativeUrl:(NSString *)relativeUrl
      withSuccess:(CompletionJsonBlock)successCallback
       andFailure:(CompletionStringBlock)failureCallback {
    
    [NetworkHelper placeGetRequestWithURL:concat(URL_BASE, relativeUrl)
                                 withData:data
                              withHandler:^(NSURLResponse *response, NSData *rawData, NSError *error) {
                                  NSString *string = [[NSString alloc] initWithData:rawData
                                                                           encoding:NSUTF8StringEncoding];
                                  NSLog(@"Data is %@", data);
                                  NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
                                  NSInteger code = [httpResponse statusCode];
                                  NSLog(@"%ld", (long)code);
                                  
                                  if (!(code >= 200 && code < 300)) {
                                      NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:[string dataUsingEncoding:NSUTF8StringEncoding]
                                                                                                 options:0 error:NULL];
                                      NSMutableArray *getdata=[[NSMutableArray alloc]init];
                                      getdata=[jsonObject objectForKey:@"error"];
                                      
                                      NSLog(@"output %@ ",[getdata valueForKey:@"details"] );
                                      if (failureCallback) {
                                          failureCallback([getdata valueForKey:@"details"]);
                                      }
                                  } else {
                                      NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:[string dataUsingEncoding:NSUTF8StringEncoding] options:0 error:NULL];
                                      
                                      if (successCallback) {
                                          successCallback(jsonObject);
                                      }
                                  }
                              }];
}

+ (void)placeGetRequestWithURL:(NSString *)action withData:(NSDictionary *)dataToSend withHandler:(void (^)(NSURLResponse *response, NSData *data, NSError *error))ourBlock {
    NSString *urlString = [NSString stringWithFormat:@"%@", action];
    NSLog(@"%@", urlString);
    //TODO: append url parameters from dataToSend
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    if ([[UserCache sharedInstance] isLoggedIn]) {
        [request setValue:[[UserCache sharedInstance] getAccessToken] forHTTPHeaderField:@"Authorization"];
    }
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:ourBlock];
}

+ (void)placeUploadImageRequest:(UIImage *)image mimeType:(NSString *)mimeType category:(NSString *)category entityId:(NSString *)entityId withSuccess:(CompletionJsonBlock)successCallback
                     andFailure:(CompletionStringBlock)failureCallback {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:concat(URL_BASE, @"services/app/FileAttachment/Upload")]];
    
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; category=%@; entityId=%@\r\n\r\n", category, entityId] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@\r\n", @"Some Caption"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add image data
    if (imageData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@; filename=imageName.jpg\r\n", @"file"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimeType] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%d", [body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
        if (httpResponse.statusCode == 200) {
            NSLog(@"response success %@", response);
            if (successCallback) {
                successCallback(nil);
            }
            //success
        } else {
            NSLog(@"response failure %@", response);
            if (failureCallback) {
                failureCallback(response.description);
            }
        }
    }];
}

+ (void)placeUploadFileRequest:(NSString *)path mimeType:(NSString *)mimeType category:(NSString *)category entityId:(NSString *)entityId withSuccess:(CompletionJsonBlock)successCallback
                    andFailure:(CompletionStringBlock)failureCallback {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:concat(URL_BASE, @"services/app/FileAttachment/Upload")]];
    
    // TODO: update method to upload other types of content
    NSData *imageData = UIImageJPEGRepresentation([UIImage imageWithContentsOfFile:path], 1.0);
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"unique-consistent-string";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; category=%@; entityId=%@\r\n\r\n", category, entityId] dataUsingEncoding:NSUTF8StringEncoding]];
//    [body appendData:[[NSString stringWithFormat:@"%@\r\n", @"Some Caption"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add image data
    if (imageData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@; filename=imageName.jpg\r\n", @"file"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%d", [body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if (data.length > 0) {
            NSLog(@"response success %@", response);
            if (successCallback) {
                successCallback(nil);
            }
            //success
        } else {
            NSLog(@"response failure %@", response);
            if (failureCallback) {
                failureCallback(response.description);
            }
        }
    }];
}


@end
