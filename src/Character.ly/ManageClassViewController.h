//
//  ManageClassViewController.h
//  Character.ly
//
//  Created by Adeel Nasir on 08/09/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ManageClassViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UIView *imageButton;
@property (strong, nonatomic) IBOutlet UIView *colorButton;
@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *btnAddClass;
@property (strong, nonatomic) IBOutlet UIView *headerView;

@end
