//
//  EditStudentViewController.m
//  Character.ly
//
//  Created by Adeel Nasir on 14/08/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "EditStudentViewController.h"
#import "NavViewController.h"
#import "DGActivityIndicatorView.h"

@interface EditStudentViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    UIView *dimView;
    DGActivityIndicatorView *activityIndicatorView;
}
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;

@end

@implementation EditStudentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    
    NavViewController *myViewController2 = [[NavViewController alloc] initForAutoLayout];
    [self.view addSubview:myViewController2];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:[UIApplication sharedApplication].statusBarFrame.size.height];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
    [myViewController2 autoSetDimension:ALDimensionHeight toSize:200];
    myViewController2.parent = self;
    
    NSLog(@"%@", self.studentData);
    [self updateProfilePic];
    
    self.txtEmail.userInteractionEnabled = NO;
    self.txtPhone.userInteractionEnabled = NO;
    self.txtStudent.userInteractionEnabled = NO;
    self.txtStudent.text = ((NSNumber *)self.studentData[@"userId"]).stringValue;
    self.txtEmail.text = self.studentData[@"emailAddress"];
    self.txtName.text = self.studentData[@"name"];
    self.txtLName.text = self.studentData[@"surname"];
    
    [_scroller setScrollEnabled:YES];
    [_scroller setContentSize:CGSizeMake(375, 1700)];
    
    UIImage *image = [UIImage imageNamed:@"characterly.png"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:image];
    
    _choosePic.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _choosePic.layer.borderWidth = 1.0f;
    
    _txtStudent.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtStudent.layer.borderWidth = 1.0f;
    
    _txtGrade.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtGrade.layer.borderWidth = 1.0f;
    _txtName.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtName.layer.borderWidth = 1.0f;
    
    _txtMName.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtMName.layer.borderWidth = 1.0f;
    _txtLName.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtLName.layer.borderWidth = 1.0f;
    
    _txtPhone.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtPhone.layer.borderWidth = 1.0f;
    
    _txtEmail.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtEmail.layer.borderWidth = 1.0f;
     
}
-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

- (void)updateProfilePic {
    if(![self.studentData[@"profilePicture"] isKindOfClass:[NSNull class]]){
        [self.profileImageView sd_setImageWithURL:[NSURL URLWithString:self.studentData[@"profilePicture"][@"path"]]];
    }
}

- (IBAction)saveButtonTap:(id)sender {
    if (trim(self.txtName.text).length < 2) {
        [self showErrorMessage:@"Name must be minimum 2 characters"];
        return;
    } else if (trim(self.txtLName.text).length < 2) {
        [self showErrorMessage:@"Last Name must be minimum 2 characters"];
        return;
    } else if (trim(self.txtEmail.text).length < 2 || ![self.txtEmail.text containsString:@"@"]) {
        [self showErrorMessage:@"Invalid Email"];
        return;
    }
    
    NSDictionary *dataToSend = @{
                                 @"branchId" : self.studentData[@"branchId"],
                                 @"userId" : self.txtStudent.text,
                                 @"emailAddress" : self.studentData[@"emailAddress"],
                                 @"name" : self.txtName.text,
                                 @"surname" : self.txtLName.text,
                                 @"id" : self.studentData[@"id"]
                                 };
    
    [self showLoader];
        [NetworkHelper placePutRequest:dataToSend relativeUrl:@"services/app/Student/Update" withSuccess:^(NSDictionary *jsonObject) {
            [self hideLoader];
            [self showErrorMessage: @"Successfully updated Profile"];
            NSLog(@"received student data: %@", jsonObject[@"result"]);
            NSDictionary *profileData = jsonObject[@"result"];
            //update UserCache info
            NSMutableDictionary *userInfo = [[UserCache sharedInstance] getUserInfo].mutableCopy;
            [userInfo setObject:profileData[@"name"] forKey:@"name"];
            [userInfo setObject:profileData[@"surname"] forKey:@"surname"];
            [userInfo setObject:profileData[@"userId"] forKey:@"id"];
            [[UserCache sharedInstance] setCurrentUser:userInfo];
            
            [self.navigationController popViewControllerAnimated:YES];
            
        } andFailure:^(NSString *string) {
            [self hideLoader];
            if (!string) {
                string = @"error updating profile";
            }
            [self showErrorMessage:string];
        }];
}
- (IBAction)cancelButtonTap:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showLoader {
    if (!dimView) {
        dimView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
        dimView.backgroundColor = [UIColor blackColor];
        dimView.alpha = 0.7f;
        dimView.userInteractionEnabled = NO;
    }
    [self.view addSubview:dimView];
    CGRect newFrame = dimView.frame;
    
    newFrame.size.width = 1200;
    newFrame.size.height = 1500;
    [dimView setFrame:newFrame];
    [dimView setCenter:CGPointMake(187.0f, 333.0f)];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeLineScale tintColor:[UIColor whiteColor] size:40.0f];
    activityIndicatorView.center = self.view.center;
    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
}

- (void)hideLoader {
    [dimView removeFromSuperview];
    [activityIndicatorView stopAnimating];
}

- (void)showErrorMessage:(NSString *)string {
    if (!string) {
        string = @"";
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Character.ly"
                                                    message:string
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (IBAction)selectImage:(UIButton *)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Choose image" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        //Cancel button tapped.
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:nil];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:nil];
        
    }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

#pragma mark - ImagePickerController Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    UIImage *pickedImage = info[UIImagePickerControllerEditedImage];
    
    [self showLoader];
    [NetworkHelper placeUploadImageRequest:pickedImage mimeType:@"image/jpeg" category:@"1" entityId:self.studentData[@"id"] withSuccess:^(NSDictionary *string) {
         // TODO: get URL from success
        self.profileImageView.image = pickedImage;
         [self hideLoader];
    } andFailure:^(NSString *string) {
        [self hideLoader];
        [self showErrorMessage:string];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
