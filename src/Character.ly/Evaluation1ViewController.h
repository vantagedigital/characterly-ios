//
//  Evaluation1ViewController.h
//  Character.ly
//
//  Created by Adeel Nasir on 17/08/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuestionOptionTableViewCell.h"
#import "QuestionOptionImageTableViewCell.h"
#import "QuestionTextTableViewCell.h"

@interface Evaluation1ViewController : UIViewController<UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate> {
    
}

@property (strong, nonatomic) NSArray *questions;
@property (strong, nonatomic) NSDictionary *question;
@property (strong, nonatomic) NSDictionary *evaluation;

@end
