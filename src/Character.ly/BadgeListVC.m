//
//  BadgeListVC.m
//  Character.ly
//
//  Created by Jesse A on 13/2/18.
//  Copyright © 2018 Adeel Nasir. All rights reserved.
//

#import "BadgeListVC.h"

@interface BadgeListVC ()

@end

@implementation BadgeListVC

- (id)initWithFrame:(CGRect)aRect
{
    if ((self = [super initWithFrame:aRect])) {
        NSLog(@"Frame %@", NSStringFromCGRect(self.frame));
        [[NSBundle mainBundle] loadNibNamed:@"BadgeDetail" owner:self options:nil];
        
        self.bounds = self.viewA.bounds;
        [self addSubview:self.viewA];
        
        box = (UIButton *)[_viewA viewWithTag:100];
        close = (UIButton *)[_viewA viewWithTag:101];
        box.layer.borderWidth = 0.5;
        box.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
        [close addTarget:self action:@selector(myEventHandler) forControlEvents: UIControlEventTouchUpInside];
    }
    return self;
}
- (id)initWithCoder:(NSCoder*)coder
{
    if ((self = [super initWithCoder:coder])) {
        
    }
    return self;
}
-(void) myEventHandler{
    NSLog(@"close");
    [self removeFromSuperview];
    
}
-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

@end
