//
//  AccountInformationView.m
//  Character.ly
//
//  Created by Adeel Nasir on 23/09/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "AccountInformationView.h"

@implementation AccountInformationView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithFrame:(CGRect)aRect
{
    if ((self = [super initWithFrame:aRect])) {
        NSLog(@"Frame %@", NSStringFromCGRect(self.frame));
        [[NSBundle mainBundle] loadNibNamed:@"AccountInformationView" owner:self options:nil];
        self.bounds = self.viewA.bounds;
        [self addSubview:self.viewA];
        
     close = (UIButton *)[_viewA viewWithTag:100];
        [close addTarget:self action:@selector(myEventHandler) forControlEvents: UIControlEventTouchUpInside];
    }
    return self;
}
- (id)initWithCoder:(NSCoder*)coder
{
    if ((self = [super initWithCoder:coder])) {
        
    }
    return self;
}
- (IBAction)btn_Close:(id)sender {
    self.hidden = YES;
}
-(void) myEventHandler{
    NSLog(@"close");
    [self removeFromSuperview];
    
}
@end
