//
//  AccountInformationView.h
//  Character.ly
//
//  Created by Adeel Nasir on 23/09/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface AccountInformationView : UIView
{
    UIButton *close;
}
- (IBAction)btn_Close:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *viewA;

@end
