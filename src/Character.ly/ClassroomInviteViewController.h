//
//  ClassroomInviteViewController.h
//  Character.ly
//
//  Created by Macbook Pro on 11/09/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClassroomInviteViewController : UIViewController<UITextFieldDelegate, UITableViewDelegate,UITableViewDataSource>{
    
}

@property (strong, nonatomic) IBOutlet UIButton *btnAddClass;
@property (strong, nonatomic) IBOutlet UITextField *txtClass;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
- (IBAction)actionInvite:(id)sender;
- (IBAction)txtEmailDismiss:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *selectClassButton;
@property (weak, nonatomic) IBOutlet UITableView *selectClassTable;

@property (strong, nonatomic) NSDictionary *data;

@end
