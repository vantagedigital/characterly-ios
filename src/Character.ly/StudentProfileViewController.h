//
//  StudentProfileViewController.h
//  Character.ly
//
//  Created by Adeel Nasir on 14/08/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface StudentProfileViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate> {

  
}
@property (strong, nonatomic) IBOutlet UIScrollView *scroller;
@property (strong, nonatomic) IBOutlet UITableView *tblNotifications;
@property (weak, nonatomic) IBOutlet UIView *profileView;
- (IBAction)openProfile:(id)sender;
- (IBAction)openNotifications:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnProfile;
@property (strong, nonatomic) IBOutlet UIButton *btnNotifications;
@property (strong, nonatomic) IBOutlet UIButton *btnEdit;
@property (strong, nonatomic) IBOutlet UIView *fatherView;
@property (strong, nonatomic) IBOutlet UIView *motherView;
@property (strong, nonatomic) IBOutlet UIImageView *badge1;
@property (strong, nonatomic) IBOutlet UIImageView *badge2;
@property (strong, nonatomic) IBOutlet UIImageView *badge3;
@property (strong, nonatomic) IBOutlet UIImageView *badge4;


@end
