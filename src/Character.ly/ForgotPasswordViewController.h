//
//  ForgotPasswordViewController.h
//  Character.ly
//
//  Created by Adeel Nasir on 08/08/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblForgot;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblPassword;
@property (weak, nonatomic) IBOutlet UILabel *lblRetype;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;

@property (weak, nonatomic) IBOutlet UITextField *txtRetype;
@property (weak, nonatomic) IBOutlet UIButton *btnChange;
- (IBAction)actionBack:(id)sender;
- (IBAction)changePassword:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnLogin;
@property (strong, nonatomic) IBOutlet UIButton *btnCreate;
- (IBAction)emailDismiss:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightFromTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingFromSide;

@end
