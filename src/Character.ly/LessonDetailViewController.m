//
//  LessonDetailViewController.m
//  Character.ly
//
//  Created by Adeel Nasir on 14/08/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "LessonDetailViewController.h"
#import "NavViewController.h"
#import "Evaluation1ViewController.h"
#import "BaseView.h"
#import <AVFoundation/AVFoundation.h>
#import "DGActivityIndicatorView.h"
#import <TTGTagCollectionView/TTGTextTagCollectionView.h>
#import "SVPullToRefresh.h"

@interface LessonDetailViewController ()<TTGTextTagCollectionViewDelegate, UITableViewDelegate,UITableViewDataSource> {
    UIView *dimView;
    DGActivityIndicatorView *activityIndicatorView;
}
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UIButton *takeEvaluationButton;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageview;
@property (weak, nonatomic) IBOutlet UIImageView *videoPlayButton;
@property (weak, nonatomic) IBOutlet UIImageView *audioIcon;
@property (weak, nonatomic) IBOutlet UIView *flashcardButton;
@property (weak, nonatomic) IBOutlet UILabel *flashcardPageText;
@property (weak, nonatomic) IBOutlet UILabel *quoteText;
@property (weak, nonatomic) IBOutlet BaseView *videoView;
@property (weak, nonatomic) IBOutlet TTGTextTagCollectionView *tagView;
@property (weak, nonatomic) IBOutlet UITableView *classroomTable;
@property (weak, nonatomic) IBOutlet UIView *classroomContainer;

@property (strong, nonatomic) AVPlayer *player;
@property (strong, nonatomic) AVPlayerItem *playerItem;
@property (assign, nonatomic) NSInteger flashcardPage;
@property (assign, nonatomic) BOOL isEducator;
@property (strong, nonatomic) NSArray *classroomData;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@property (weak, nonatomic) IBOutlet UIButton *markCompletedButton;
@property (weak, nonatomic) IBOutlet UILabel *markCompletedPercent;
@property (weak, nonatomic) IBOutlet UILabel *markCompletedText;
@property (weak, nonatomic) IBOutlet UIView *markCompletedivider;
@property (weak, nonatomic) IBOutlet UILabel *markCompletedSubtitle;
@property (weak, nonatomic) IBOutlet UIImageView *fbButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tagViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentLabelHeight;


@end

@implementation LessonDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.flashcardPage = 0;
    self.navigationController.navigationBar.hidden = YES;
    NavViewController *myViewController2 = [[NavViewController alloc] initForAutoLayout];
    [self.view addSubview:myViewController2];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:[UIApplication sharedApplication].statusBarFrame.size.height];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
    [myViewController2 autoSetDimension:ALDimensionHeight toSize:200];
    myViewController2.parent = self;
    // Do any additional setup after loading the view.
    
    NSLog(@"Access token: %@", [[UserCache sharedInstance] getAccessToken]);
    
    UIImage *image = [UIImage imageNamed:@"characterly.png"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:image];
   
    _btnCloseMenu.hidden = YES;
    
    self.classroomContainer.hidden = YES;
    self.closeButton.enabled = true;
    self.classroomTable.delegate = self;
    self.classroomTable.dataSource = self;
    
    self.tagView.delegate = self;
    self.tagView.showsVerticalScrollIndicator = NO;

    TTGTextTagConfig *config = self.tagView.defaultConfig;
    
    config.tagTextFont = [UIFont systemFontOfSize:20.0f];
    
    config.tagExtraSpace = CGSizeMake(12, 12);
    
    config.tagTextColor = [UIColor whiteColor];
    config.tagSelectedTextColor = [UIColor whiteColor];
    
    config.tagBackgroundColor = [UIColor colorWithRed:0.10 green:0.53 blue:0.85 alpha:1.00];
    config.tagSelectedBackgroundColor = [UIColor colorWithRed:0.21 green:0.29 blue:0.36 alpha:1.00];
    
    config.tagCornerRadius = 8.0f;
    config.tagSelectedCornerRadius = 4.0f;
    
    config.tagBorderWidth = 0;
    
    config.tagBorderColor = [UIColor whiteColor];
    config.tagSelectedBorderColor = [UIColor whiteColor];
    
    config.tagShadowColor = [UIColor blackColor];
    config.tagShadowOffset = CGSizeMake(0, 1);
    config.tagShadowOpacity = 0.3f;
    config.tagShadowRadius = 2;
    
    self.isEducator = ![[[UserCache sharedInstance] getUserInfo][@"educator"] isKindOfClass:[NSNull class]];
    
    @try {
        [self configureLayoutForContentType:self.data];
    } @catch (NSException *exception) {}
    
    self.titleLabel.text = self.data[@"contentSchedule"][@"content"][@"title"];
    self.content.text = self.data[@"contentSchedule"][@"content"][@"contentText"];
    NSUInteger contentTextCharacterCount = [self.data[@"contentSchedule"][@"content"][@"contentText"] length];
    NSLog(@"%li", contentTextCharacterCount);
    self.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.content.lineBreakMode = NSLineBreakByWordWrapping;
    self.content.numberOfLines = 0;
    self.contentLabelHeight.constant = contentTextCharacterCount * 0.5;
    
    @try {
        NSArray<NSDictionary *> *attachments = self.data[@"contentSchedule"][@"content"][@"fileAttachments"] == nil ? self.data[@"contentSchedule"][@"content"][@"attachments"] : self.data[@"contentSchedule"][@"content"][@"fileAttachments"];
        NSString *imageUrl = attachments[attachments.count - 1][@"path"];
        
        [self.imageview sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
    } @catch (NSException *exception) {}
    
    NSString *str = self.data[@"contentSchedule"][@"startDate"];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd\'T\'HH:mm:ss"];
    
    NSDate *dte = [dateFormat dateFromString:str];
    
    [dateFormat setDateFormat:@"MMM dd, yyyy @ hh:mm a"];
    self.dateLabel.text = [NSString stringWithFormat:@"%@", [dateFormat stringFromDate:dte]];
    
    NSMutableArray *tags = [NSMutableArray array];
    @try {
        for (NSDictionary *contentTag in self.data[@"contentSchedule"][@"content"][@"contentTags"]) {
            [tags addObject:contentTag[@"name"]];
        }
    } @catch (NSException *exception) {
    }
    
    [self.tagView addTags: tags];
    self.tagViewHeight.constant = tags.count * 23;
    [self.tagView reload];
    
    self.mainViewHeight.constant = 750 + self.contentLabelHeight.constant + self.tagViewHeight.constant;
    if(self.isEducator){
        self.classroomData = [[UserCache sharedInstance] getUserInfo][@"educator"][@"classrooms"];
        self.mainViewHeight.constant = 750 + self.contentLabelHeight.constant + self.tagViewHeight.constant - 200;
    }
}

- (void)configureLayoutForContentType:(NSDictionary *)data {
//    self.imageview.hidden = YES;
//    self.quoteText.hidden = YES;
//    self.videoPlayButton.hidden = YES;
//    self.audioIcon.hidden = YES;
//    self.flashcardButton.hidden = YES;
    
    NSInteger contentType = ((NSNumber *)data[@"contentSchedule"][@"content"][@"contentType"]).integerValue;
    
    [self configureMarkAsCompletedButton:data];
    
    if (contentType == 0) {
        // Article type, just leave default description showing
    } else if (contentType == 1) {//Audio
        self.imageview.hidden = NO;
        self.audioIcon.hidden = NO;
        self.imageview.backgroundColor = [UIColor colorWithRed:0.847 green:0.847 blue:0.847 alpha:1.00];
        
        UITapGestureRecognizer* gs = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(audioIconTap)];
        
        [self.audioIcon setUserInteractionEnabled:YES];
        [self.audioIcon addGestureRecognizer:gs];
        
    } else if (contentType == 2) {//Image
        self.imageview.hidden = NO;
        NSString *imageUrl = @"";
        @try {
            NSArray<NSDictionary *> *attachments = self.data[@"contentSchedule"][@"content"][@"fileAttachments"] == nil ? self.data[@"contentSchedule"][@"content"][@"attachments"] : self.data[@"contentSchedule"][@"content"][@"fileAttachments"];
            imageUrl = attachments[attachments.count - 1][@"path"];
        } @catch (NSException *exception) {}
        [self.imageview sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
    } else if (contentType == 3) {//Video
        self.videoView.hidden = NO;
        self.videoPlayButton.hidden = NO;
        
        UITapGestureRecognizer* gs = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(videoPlayButtonTap)];
        
        [self.videoPlayButton setUserInteractionEnabled:YES];
        [self.videoPlayButton addGestureRecognizer:gs];
    } else if (contentType == 4) {//Quote
        self.quoteText.hidden = NO;
        @try {
            self.quoteText.text = [NSString stringWithFormat:@"\"%@\"", self.data[@"contentSchedule"][@"content"][@"description"]];
        } @catch (NSException *exception) {}
    } else if (contentType == 5) {//Flash Cards
        self.flashcardButton.hidden = NO;
        self.imageview.hidden = NO;
        self.quoteText.hidden = NO;
        self.imageview.backgroundColor = [UIColor colorWithRed:0.847 green:0.847 blue:0.847 alpha:1.00];
        
    }
    
}

- (void)updateFlashCardUI {
    @try {
        self.flashcardPageText.text = [NSString stringWithFormat:@"%@/%@", @(self.flashcardPage + 1), @(((NSArray *)self.data[@"contentSchedule"][@"content"][@"contentPages"]).count)];
        NSDictionary *contentPage = ((NSArray *)self.data[@"contentSchedule"][@"content"][@"contentPages"])[self.flashcardPage];
        self.quoteText.text = contentPage[@"contentText"];
    } @catch (NSException *exception) {}
}

- (void)audioIconTap {
    NSString *audioUrl = @"";
    @try {
        NSArray<NSDictionary *> *attachments = self.data[@"contentSchedule"][@"content"][@"fileAttachments"] == nil ? self.data[@"contentSchedule"][@"content"][@"attachments"] : self.data[@"contentSchedule"][@"content"][@"fileAttachments"];
        audioUrl = attachments[attachments.count - 1][@"path"];
    } @catch (NSException *exception) {}
    
    NSURL *url = [NSURL URLWithString:audioUrl];
    self.playerItem = [AVPlayerItem playerItemWithURL:url];
    self.player = [AVPlayer playerWithPlayerItem:self.playerItem];
    self.player = [AVPlayer playerWithURL:url];
    [self.player play];
}

- (void)videoPlayButtonTap {
    if (!self.player) {
        NSString *videoUrl = @"";
        @try {
            NSArray<NSDictionary *> *attachments = self.data[@"contentSchedule"][@"content"][@"fileAttachments"] == nil ? self.data[@"contentSchedule"][@"content"][@"attachments"] : self.data[@"contentSchedule"][@"content"][@"fileAttachments"];
            videoUrl = attachments[attachments.count - 1][@"path"];
        } @catch (NSException *exception) {}
        
        NSURL *url = [NSURL URLWithString:videoUrl];
        
        self.player = [[AVPlayer alloc] initWithURL:url];
        
        AVPlayerLayer *videoLayer = [AVPlayerLayer playerLayerWithPlayer:self.player];
        
        videoLayer.frame = self.videoView.bounds;
        videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        [self.videoView.layer addSublayer:videoLayer];
        self.videoView.playerLayer = videoLayer;
        
        self.player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    }
    
    [self.player play];
}

- (IBAction)fbButtonTap:(id)sender {
    [self showErrorMessage:@"Function not implemented"];
}

- (IBAction)btnPrevTap:(id)sender {
    if (!self.data[@"contentSchedule"][@"content"][@"contentPages"]) {
        return;
    }
    if (self.flashcardPage <= 0) {
        return;
    }
    self.flashcardPage--;
    [self updateFlashCardUI];
    
}
- (IBAction)btnNextTap:(id)sender {
    if (!self.data[@"contentSchedule"][@"content"][@"contentPages"]) {
        return;
    }
    if (self.flashcardPage >= ((NSArray *)self.data[@"contentSchedule"][@"content"][@"contentPages"]).count - 1) {
        return;
    }
    self.flashcardPage++;
    [self updateFlashCardUI];
    
}

- (void)configureMarkAsCompletedButton:(NSDictionary *)data {
    self.markCompletedButton.hidden = YES;
    self.markCompletedPercent.hidden = YES;
    self.markCompletedText.hidden = YES;
    self.takeEvaluationButton.hidden = YES;
    self.markCompletedivider.hidden = YES;
    self.markCompletedSubtitle.hidden = YES;
    self.fbButton.hidden = YES;
    
    if (self.isEducator) {
        
        self.takeEvaluationButton.hidden = NO;
        [self.takeEvaluationButton setTitle:@"Assign to classroom" forState:UIControlStateNormal];
        
        return;
    }
    
    NSInteger status = ((NSNumber *) data[@"status"]).integerValue;
    NSInteger percentage = ((NSNumber *) data[@"progressPercentage"]).integerValue;
    if (status == 2) {//completed
        self.markCompletedButton.hidden = NO;
        self.markCompletedText.hidden = NO;
        self.markCompletedButton.imageView.image = [UIImage imageNamed:@"btn-completed-uncheck-green"];
        
        self.markCompletedivider.hidden = NO;
        self.markCompletedSubtitle.hidden = NO;
        self.fbButton.hidden = NO;
        
        self.markCompletedSubtitle.text = @"Congratulations you have completed this task and earned 5pt. ";
    } else if (status == 1) {//InProgress
        self.markCompletedButton.hidden = NO;
        self.markCompletedText.hidden = NO;
        self.markCompletedPercent.hidden = NO;
        self.markCompletedButton.imageView.image = [UIImage imageNamed:@"btn-completed-nocheck"];
        self.markCompletedPercent.text = concat(@(percentage).stringValue, @"%");
        
        if (percentage >= 50) {
            self.markCompletedText.text = [NSString stringWithFormat:@"Congratulations you have completed %@%%, now take the evaluation to complete this lesson.", @(percentage)];
            self.takeEvaluationButton.hidden = NO;
        } else {
            self.markCompletedText.text = @"";
        }
    } else {
        self.markCompletedButton.hidden = NO;
        self.markCompletedText.hidden = NO;
        self.markCompletedButton.imageView.image = [UIImage imageNamed:@"btn-completed-uncheck"];
        self.markCompletedText.text = @"Mark completed";
        
        [self.markCompletedButton addTarget:self action:@selector(markCompletedTap) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)markCompletedTap {
    NSString *userId = self.data[@"userId"];
    NSLog(@"%@", userId);
    NSLog(@"%@", self.data[@"contentScheduleId"]);
    NSLog(@"%@", self.data[@"id"]);
    NSDictionary *dataToSend = @{
                                 @"progressPercentage" : @"50",
                                 @"userId" : userId,
                                 @"contentScheduleId" : self.data[@"contentScheduleId"],
                                 @"status" : self.data[@"status"],
                                 @"id" : self.data[@"id"],
                                 };
    
    [self showLoader];
    [NetworkHelper placePutRequest:dataToSend relativeUrl:@"services/app/UserContent/Update" withSuccess:^(NSDictionary *jsonObject) {
        [self hideLoader];
        
        [self showErrorMessage:@"Successfully Marked as completed"];
        
        NSMutableDictionary *dataMutable = self.data.mutableCopy;
        @try{
            [dataMutable setObject:jsonObject[@"result"][@"progressPercentage"] forKey:@"progressPercentage"];
            [dataMutable setObject:jsonObject[@"result"][@"status"] forKey:@"status"];
        } @catch(NSException *e) {}
        
        self.data = dataMutable;
        NSLog(@"received data: %@", jsonObject);
        
        [self configureMarkAsCompletedButton:self.data];
    } andFailure:^(NSString *string) {
        [self hideLoader];
        [self showErrorMessage:string];
    }];
}
- (IBAction)closeContainer:(id)sender {
    self.classroomContainer.hidden = YES;
    [self removeAnimate];
}
- (IBAction)removeContainer:(UIButton *)sender {
    self.classroomContainer.hidden = YES;
    [self removeAnimate];
}

- (IBAction)takeEvaluationTap:(id)sender {

    if(self.isEducator){
        self.classroomContainer.hidden = NO;
        self.closeButton.hidden = NO;
        [self showAnimate];
        
    }else{
//        [self performSegueWithIdentifier:@"eval1" sender:self];
    }
}

- (void) showAnimate {
    self.classroomContainer.transform = CGAffineTransformMakeScale(1.0, 1.0);
    self.classroomContainer.alpha = 0.0;
    [UIView animateWithDuration:0.25 animations:^{
        self.classroomContainer.alpha = 1.0;
        self.classroomContainer.transform = CGAffineTransformMakeScale(1.0, 1.0);
    }];
}

- (void) removeAnimate {
    [UIView animateWithDuration:0.25 animations:^{
        self.classroomContainer.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.classroomContainer.alpha = 0.0;
    } completion: ^(BOOL finished) {
        if(finished){
            self.classroomContainer.hidden = YES;
        }
    }];
}

- (void)showLoader {
    if (!dimView) {
        dimView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
        dimView.backgroundColor = [UIColor blackColor];
        dimView.alpha = 0.7f;
        dimView.userInteractionEnabled = NO;
    }
    [self.view addSubview:dimView];
    CGRect newFrame = dimView.frame;
    
    newFrame.size.width = 1200;
    newFrame.size.height = 1500;
    [dimView setFrame:newFrame];
    [dimView setCenter:CGPointMake(187.0f, 333.0f)];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeLineScale tintColor:[UIColor whiteColor] size:40.0f];
    activityIndicatorView.center = self.view.center;
    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
}

- (void)showErrorMessage:(NSString *)string {
    if (!string) {
        string = @"";
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Character.ly"
                                                    message:string
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)hideLoader {
    [dimView removeFromSuperview];
    [activityIndicatorView stopAnimating];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView == _classroomTable){
        return self.classroomData.count;
    }else{
        return 0;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView == _classroomTable) {
        //assign lesson to selected classroom.
        NSString *contentScheduleParam = [@"contentScheduleId=" stringByAppendingString: self.data[@"contentScheduleId"]];
        NSString *classroomParam = [@"classroomId=" stringByAppendingString: self.classroomData[indexPath.row][@"id"]];
        NSString *url = [NSString stringWithFormat:@"services/app/ContentSchedule/AssignToClassroom?%@&%@", contentScheduleParam, classroomParam];
        NSLog(@"selected URL: %@", url);
        [self showLoader];
        [NetworkHelper placePostRequest:nil relativeUrl:url withSuccess:^(NSDictionary *jsonObject) {
            [self hideLoader];
            self.classroomContainer.hidden = YES;
            [self removeAnimate];
            [self showErrorMessage:@"Successfully Assigned"];
        } andFailure:^(NSString *string) {
            [self hideLoader];
            NSLog(@"Failure: %@", string);
            self.classroomContainer.hidden = YES;
            [self removeAnimate];
            [self showErrorMessage:string];
        }];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView == _classroomTable){
        return 250;
    }else{
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView == _classroomTable){
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"classroom"];
        
        UIImageView *imageView = (UIImageView *)[cell viewWithTag:10];
        UILabel *classCode = (UILabel *)[cell viewWithTag:11];
        UILabel *classTitle = (UILabel *)[cell viewWithTag:12];
        UILabel *gradeName = (UILabel *)[cell viewWithTag:13];
        UILabel *studentsCount = (UILabel *)[cell viewWithTag:14];
        
        classCode.text = self.classroomData[indexPath.row][@"classCode"];
        NSLog(@"classroom data: %@ : %@ : %@", self.classroomData[indexPath.row][@"classCode"], self.classroomData[indexPath.row][@"name"], self.classroomData[indexPath.row][@"grade"][@"name"]);
        classTitle.text = self.classroomData[indexPath.row][@"name"];
        NSNull *null = [[NSNull alloc] init];
        if(self.classroomData[indexPath.row][@"grade"] == null){
            
        }else{
            gradeName.text = self.classroomData[indexPath.row][@"grade"][@"name"];
        }
        studentsCount.text = @"Students:";
        
        // Configure the cell...
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"classroom"];
        }
        
        return cell;
    }else{
        return nil;
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"eval1"]) {
        // Get reference to the destination view controller
        Evaluation1ViewController *vc = [segue destinationViewController];
//        vc.contentResult = self.data;
    }
}


@end
