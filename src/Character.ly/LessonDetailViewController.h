//
//  LessonDetailViewController.h
//  Character.ly
//
//  Created by Adeel Nasir on 14/08/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LessonDetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIButton *btnCloseMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;

@property (nonatomic, strong) NSMutableDictionary *data;

@end
