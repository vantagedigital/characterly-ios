//
//  TempViewController.m
//  Character.ly
//
//  Created by Adeel Nasir on 15/09/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "TempViewController.h"
#import "NavViewController.h"
#import "DGActivityIndicatorView.h"

@interface TempViewController (){
    DGActivityIndicatorView *activityIndicatorView;
}

@end

@implementation TempViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     self.navigationController.navigationBar.hidden = YES;
    
    NavViewController *myViewController2 = [[NavViewController alloc] initForAutoLayout];
    [self.view addSubview:myViewController2];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:[UIApplication sharedApplication].statusBarFrame.size.height];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
    [myViewController2 autoSetDimension:ALDimensionHeight toSize:200];
    myViewController2.parent = self;
    
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) viewDidAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
}
- (void)sendHttpPost {
    UIView *dimView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
    dimView.backgroundColor = [UIColor blackColor];
    dimView.alpha = 0.5f;
    dimView.userInteractionEnabled = NO;
    [self.view addSubview:dimView];
    CGRect newFrame = dimView.frame;
    
    newFrame.size.width = 1200;
    newFrame.size.height = 1500;
    [dimView setFrame:newFrame];
    [dimView setCenter:CGPointMake(160.0f, 250.0f)];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeLineScale tintColor:[UIColor whiteColor] size:40.0f];
    activityIndicatorView.center = self.view.center;
    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    
    NSString *valueToSave;
    
    //NSLog(@"%s - %d", __PRETTY_FUNCTION__, __LINE__);
    
    NSString *postMsg = [NSString stringWithFormat:@"userNameOrEmailAddress=test@test.com&password=test&rememberClient=true"];
    NSString *urlStr = @"http://13.92.193.213:80/api/TokenAuth/Authenticate";
    
    /*
     * dataUsingEncoding:allowLossyConversion: - Returns nil if flag is NO and the receiver can’t be converted without losing some information.
     */
    NSData *postData = [postMsg dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%d",[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:urlStr]];
    
    [request setHTTPMethod:@"POST"];
    
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    /*
     * Sets value for default header field
     */
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    /*
     * Add customer header field and value
     */
   // [request addValue:@"apiuser" forHTTPHeaderField:@"X-USERNAME"];
    
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *error;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    [dimView removeFromSuperview];
    [activityIndicatorView stopAnimating];
    NSString *responseStr = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSLog(@"%s - %d # responseStr = %@", __PRETTY_FUNCTION__, __LINE__, responseStr);
    
    NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    NSLog(@"Json is %@", json);
    
    NSString *string = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    NSString* tokenValue1 = [string
                             stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    if (string.length >= 12 && [string rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location != NSNotFound) {
        // this matches the criteria
        NSLog(@"Tkoen is %@",tokenValue1);
        [[NSUserDefaults standardUserDefaults] setObject:tokenValue1 forKey:@"isLogin"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        // tokenValue = tokenValue1;
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        ViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
//        
//        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
//        [navController setViewControllers: @[rootViewController] animated: YES];
        
        // [self.revealViewController setFrontViewController:navController];
        //[self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        //  [self getProfileData];
        
        
        
        
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:string
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"isLogin"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    
    
    
    
    
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
