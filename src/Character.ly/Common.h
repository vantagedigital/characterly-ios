//
//

#ifndef Common_h
#define Common_h

//#define URL_BASE @"http://13.92.193.213:80/api/"
#define URL_BASE @"http://betaapi.character.ly/api/"


typedef void (^CompletionBlock) ();
typedef void (^CompletionStringBlock) (NSString *string);
typedef void (^CompletionJsonBlock) (NSDictionary *string);

static inline int randInt(int min, int max) {
    // random() is normally exclusive of the top value,
    // so add 1 to make it inclusive
    int randomNum = arc4random_uniform(((max - min) + 1)) + min;
    return randomNum;
}

static inline BOOL IsEmpty(id thing) {
    return thing == nil
    || ([thing isEqual:[NSNull null]]) //JS addition for coredata
    || ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}

static inline NSString *concat(NSString *s1, NSString *s2) {
    return [NSString stringWithFormat:@"%@%@", s1, s2];
}

static inline NSString *trim (NSString *string) {
    return [string stringByTrimmingCharactersInSet:
            [NSCharacterSet whitespaceCharacterSet]];
}

#endif /* Common_h */
