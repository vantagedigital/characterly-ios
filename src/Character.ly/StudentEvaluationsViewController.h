//
//  StudentEvaluationsViewController.h
//  Character.ly
//
//  Created by Adeel Nasir on 10/09/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StudentEvaluationsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate> {
    
    
}
@property (strong, nonatomic) IBOutlet UIScrollView *scroller;
@property (strong, nonatomic) IBOutlet UITableView *tblNotifications;
- (IBAction)openProfile:(id)sender;
- (IBAction)openNotifications:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnProfile;
@property (strong, nonatomic) IBOutlet UIButton *btnNotifications;
@property (strong, nonatomic) IBOutlet UIButton *btnEdit;
@property (strong, nonatomic) IBOutlet UIView *fatherView;
@property (strong, nonatomic) IBOutlet UIView *motherView;
@property (strong, nonatomic) IBOutlet UIImageView *badge1;
@property (strong, nonatomic) IBOutlet UIImageView *badge2;
@property (strong, nonatomic) IBOutlet UIImageView *badge3;
@property (strong, nonatomic) IBOutlet UIImageView *badge4;
@property (strong, nonatomic) IBOutlet UITextField *txtEvaluation;

@property (strong, nonatomic) NSDictionary *classroomData;
@property (strong, nonatomic) NSDictionary *studentData;

@end
