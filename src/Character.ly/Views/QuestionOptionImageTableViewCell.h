//
//  QuestionOptionImageTableViewCell.h
//  Character.ly
//
//  Created by dev on 3/27/18.
//  Copyright © 2018 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BEMCheckBox.h"

@interface QuestionOptionImageTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *optionImageView;
@property (weak, nonatomic) IBOutlet UILabel *optionDescription;
@property (weak, nonatomic) IBOutlet BEMCheckBox *optionCheck;

@end
