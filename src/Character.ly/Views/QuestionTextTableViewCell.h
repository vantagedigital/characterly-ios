//
//  QuestionTextTableViewCell.h
//  Character.ly
//
//  Created by dev on 3/27/18.
//  Copyright © 2018 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionTextTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *answerTextField;

@end
