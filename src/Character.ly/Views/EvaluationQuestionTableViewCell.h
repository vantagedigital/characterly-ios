//
//  EvaluationQuestionTableViewCell.h
//  Character.ly
//
//  Created by dev on 3/14/18.
//  Copyright © 2018 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuestionOptionTableViewCell.h"
#import "QuestionOptionImageTableViewCell.h"
#import "QuestionTextTableViewCell.h"

@interface EvaluationQuestionTableViewCell : UITableViewCell<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *questionDescription;
@property (weak, nonatomic) IBOutlet UITableView *optionTable;
@property (strong, nonatomic) NSDictionary *questionContent;
@property (assign, nonatomic) NSInteger questionTypeNumber;


@end
