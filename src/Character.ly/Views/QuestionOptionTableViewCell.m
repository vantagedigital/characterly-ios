//
//  QuestionOptionTableViewCell.m
//  Character.ly
//
//  Created by dev on 3/14/18.
//  Copyright © 2018 Adeel Nasir. All rights reserved.
//

#import "QuestionOptionTableViewCell.h"

@implementation QuestionOptionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
    if (self.optionCheck.on) {
        self.optionCheck.on = NO;
    }else{
        self.optionCheck.on = YES;
    }
    
    self.optionCheck.boxType = BEMBoxTypeSquare;
    self.optionCheck.onAnimationType = BEMAnimationTypeBounce;
    self.optionCheck.offAnimationType = BEMAnimationTypeBounce;
    self.optionCheck.lineWidth = 2;
    self.optionCheck.tintColor = [UIColor colorWithRed:95.0f/255.0f green:96.0f/255.0f blue:171.0f/255.0f alpha:1.0f];
    self.optionCheck.onTintColor = [UIColor whiteColor];
    self.optionCheck.onFillColor = [UIColor colorWithRed:95.0f/255.0f green:96.0f/255.0f blue:171.0f/255.0f alpha:1.0f];
    self.optionCheck.onCheckColor = [UIColor whiteColor];
    self.optionDescription.adjustsFontSizeToFitWidth = YES;
}
- (IBAction)checkAction:(BEMCheckBox *)sender {
    
}

@end
