//
//  BaseView.m
//  TakeAnL
//
//  Created by Jesse A on 19/09/2017.
//  Copyright © 2017 Jesse A. All rights reserved.
//

#import "BaseView.h"

@implementation BaseView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)layoutSublayersOfLayer:(CALayer *)layer {
    [super layoutSublayersOfLayer:layer];
    self.playerLayer.frame = self.bounds;
}

@end
