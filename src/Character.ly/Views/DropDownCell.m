//
//  DropDownCell.m
//  Character.ly
//
//  Created by dev on 2/28/18.
//  Copyright © 2018 Adeel Nasir. All rights reserved.
//

#import "DropDownCell.h"

@implementation DropDownCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
