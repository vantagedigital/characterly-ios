//
//  ContentType1Cell.m
//  Character.ly
//
//  Created by Jesse A on 10/11/17.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "ContentType1Cell.h"

@implementation ContentType1Cell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
