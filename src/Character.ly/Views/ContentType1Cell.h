//
//  ContentType1Cell.h
//  Character.ly
//
//  Created by Jesse A on 10/11/17.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContentType1Cell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *subjectLabel;
@property (weak, nonatomic) IBOutlet UILabel *gradeLabel;
@property (weak, nonatomic) IBOutlet UIButton *pillar1;
@property (weak, nonatomic) IBOutlet UIButton *pillar2;
@property (weak, nonatomic) IBOutlet UIButton *pillar3;
@property (weak, nonatomic) IBOutlet UIButton *pillar4;
@property (weak, nonatomic) IBOutlet UIButton *pillar5;
@property (weak, nonatomic) IBOutlet UIButton *pillar6;
@property (weak, nonatomic) IBOutlet UIButton *pillar7;
@property (weak, nonatomic) IBOutlet UIButton *pillar8;
@property (weak, nonatomic) IBOutlet UIButton *pillar9;
@property (weak, nonatomic) IBOutlet UIImageView *imageview;

@end
