//
//  EvaluationQuestionTableViewCell.m
//  Character.ly
//
//  Created by dev on 3/14/18.
//  Copyright © 2018 Adeel Nasir. All rights reserved.
//

#import "EvaluationQuestionTableViewCell.h"

@implementation EvaluationQuestionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.optionTable.delegate = self;
    self.optionTable.dataSource = self;
    
    self.questionDescription.adjustsFontSizeToFitWidth = YES;
    self.questionTypeNumber = [self.questionContent[@"questionType"] integerValue];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
    self.questionTypeNumber = [self.questionContent[@"questionType"] integerValue];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSArray *questionOptions = self.questionContent[@"questionOptions"];
    return questionOptions.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.questionTypeNumber = [self.questionContent[@"questionType"] integerValue];
    
    if(self.questionTypeNumber == 0 || self.questionTypeNumber == 1)
    {
        QuestionTextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"optionText"];
        
        if(cell == nil) {
            NSArray *cellObjects = [[NSBundle mainBundle] loadNibNamed:@"QuestionTextTableViewCell" owner:self options:nil];
            cell = [cellObjects objectAtIndex:0];
        }
        
        return cell;
    }
    else if(self.questionTypeNumber == 2 || self.questionTypeNumber == 3)
    {
        QuestionOptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"optionCell"];
        
        if(cell == nil) {
            NSArray *cellObjects = [[NSBundle mainBundle] loadNibNamed:@"QuestionOptionTableViewCell" owner:self options:nil];
            cell = [cellObjects objectAtIndex:0];
        }
        
        NSArray *questionOptions = self.questionContent[@"questionOptions"];
        NSDictionary *options = questionOptions[indexPath.row];
        
        cell.optionDescription.text = options[@"description"];
        
        return cell;
    }
    else if(self.questionTypeNumber == 4 || self.questionTypeNumber == 5)
    {
        QuestionOptionImageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"optionImage"];
        
        if(cell == nil) {
            NSArray *cellObjects = [[NSBundle mainBundle] loadNibNamed:@"QuestionOptionImageTableViewCell" owner:self options:nil];
            cell = [cellObjects objectAtIndex:0];
        }
        
        NSArray *questionOptions = self.questionContent[@"questionOptions"];
        NSDictionary *options = questionOptions[indexPath.row];
        
        cell.optionDescription.text = options[@"description"];
        
        if(![options[@"fileAttachment"] isKindOfClass:[NSNull class]]){
            
            [cell.optionImageView sd_setImageWithURL: options[@"fileAttachment"][@"path"]];
        }
        
        return cell;
    }
    else
    {
        return nil;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

@end
