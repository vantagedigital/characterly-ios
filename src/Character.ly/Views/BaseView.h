//
//  BaseView.h
//  TakeAnL
//
//  Created by Jesse A on 19/09/2017.
//  Copyright © 2017 Jesse A. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseView : UIView

@property (nonatomic, strong) CALayer *playerLayer;

@end
