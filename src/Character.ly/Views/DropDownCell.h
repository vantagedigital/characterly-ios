//
//  DropDownCell.h
//  Character.ly
//
//  Created by dev on 2/28/18.
//  Copyright © 2018 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DropDownCell : UITableViewCell {
    
}

@property (weak, nonatomic) IBOutlet UIButton *dropDownButton;
@property (weak, nonatomic) IBOutlet UILabel *dropDownText;

@end
