//
//  MessageWindow.h
//  Character.ly
//
//  Created by Adeel Nasir on 09/10/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageWindow : UIView
@property (strong, nonatomic) IBOutlet UIView *viewA;
-(void)setMessage:(NSString*)message;
@end
