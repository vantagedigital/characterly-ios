//
//  MessageWindow.m
//  Character.ly
//
//  Created by Adeel Nasir on 09/10/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "MessageWindow.h"
@interface MessageWindow (){
    UILabel* lblNotification;
}
@end
@implementation MessageWindow
- (id)initWithFrame:(CGRect)aRect
{
    if ((self = [super initWithFrame:aRect])) {
        NSLog(@"Frame %@", NSStringFromCGRect(self.frame));
        [[NSBundle mainBundle] loadNibNamed:@"MessageWindow" owner:self options:nil];
        self.bounds = self.viewA.bounds;
        self.viewA.layer.cornerRadius = 5;
        self.viewA.layer.masksToBounds = true;
        [self addSubview:self.viewA];
        
        lblNotification = (UILabel*)[self viewWithTag:100];
    }
    return self;
}

- (id)initWithCoder:(NSCoder*)coder
{
    if ((self = [super initWithCoder:coder])) {
        
    }
    return self;
}
-(void)setMessage:(NSString*)message {
    lblNotification.text = message;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
