//
//  QuestionOptionTableViewCell.h
//  Character.ly
//
//  Created by dev on 3/14/18.
//  Copyright © 2018 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BEMCheckBox.h"

@interface QuestionOptionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet BEMCheckBox *optionCheck;
@property (weak, nonatomic) IBOutlet UILabel *optionDescription;

@end
