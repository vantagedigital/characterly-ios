//
//  ManageViewController.m
//  Character.ly
//
//  Created by Adeel Nasir on 10/09/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "ManageViewController.h"
#import "NavViewController.h"
#import "ManageClassViewController.h"
#import "DGActivityIndicatorView.h"
#import "DropDownCell.h"

@interface ManageViewController ()<UITextFieldDelegate, UITableViewDelegate,UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    UIView *dimView;
    DGActivityIndicatorView *activityIndicatorView;
}
@property (strong, nonatomic) NSMutableArray *grades, *students;
@property (assign, nonatomic) NSInteger currentGradeSelection, currentSortSelection;
@property (strong, nonatomic) NSArray *gradeModel;
@property (strong, nonatomic) NSMutableArray *selectedUserIds;
@property (strong, nonatomic) NSMutableArray <NSString *> *sortOrders;
@property (strong, nonatomic) NSString *colorCode;

@property (weak, nonatomic) IBOutlet UIButton *selectGradeButton;
@property (weak, nonatomic) IBOutlet UIButton *selectSortButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeightConstraint;

@end

@implementation ManageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.grades = [NSMutableArray array];
    self.students = [NSMutableArray array];
    self.selectedUserIds = [NSMutableArray array];
    [self.selectGradeButton.layer setBorderWidth: 0.5];
    [self.selectGradeButton.layer setBorderColor:[[UIColor grayColor] CGColor]];
    
    
    [self.selectSortButton.layer setBorderWidth: 0.5];
    [self.selectSortButton.layer setBorderColor:[[UIColor grayColor] CGColor]];
    
    self.colorCode = @"FFFFFF";
    self.ColorSelectView.hidden = YES;
    
    for (ColorButton *colorSelectButton in self.colorButtonGroup){
        colorSelectButton.clipsToBounds = YES;
        colorSelectButton.layer.cornerRadius = 0.5 * colorSelectButton.bounds.size.height;
    }
    
    self.sortOrders = @[@"Descending",
                      @"Ascending"
                      ].mutableCopy;
    
    self.txtSort.text = self.sortOrders[self.currentSortSelection];
    
    self.gradeTable.delegate = self;
    self.gradeTable.dataSource = self;
    self.gradeTable.hidden = YES;
    self.gradeTable.editing = NO;
    self.sortTable.delegate = self;
    self.sortTable.dataSource = self;
    self.sortTable.hidden = YES;
    self.sortTable.editing = NO;
    
    [self getGrades];
    [self getStudents];
    [self getClassroomStudents];
    
    self.txtSchool.delegate = self;
    self.txtClassName.delegate = self;
    self.txtSearch.delegate = self;
    
    if (self.data) {
        self.txtClassName.text = self.data[@"name"];
        self.txtCode.text = self.data[@"classCode"];
    }
    
    self.navigationController.navigationBar.hidden = YES;
    
    NavViewController *myViewController2 = [[NavViewController alloc] initForAutoLayout];
    [self.view addSubview:myViewController2];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:[UIApplication sharedApplication].statusBarFrame.size.height];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
    [myViewController2 autoSetDimension:ALDimensionHeight toSize:200];
    myViewController2.parent = self;
    
    // Do any additional setup after loading the view.
    _containerView.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _containerView.layer.borderWidth = 0.5f;
    
    _btnImage.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _btnImage.layer.borderWidth = 0.5f;
    
    _btnAdd.layer.borderColor = [UIColor grayColor].CGColor;
    _btnAdd.layer.borderWidth = 0.5f;
    _btnCancel.layer.borderColor = [UIColor grayColor].CGColor;
    _btnCancel.layer.borderWidth = 0.5f;
    
    _txtClassName.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtClassName.layer.borderWidth = 1.0f;
    
    _txtGrade.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtGrade.layer.borderWidth = 1.0f;
    
    _txtSchool.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtSchool.layer.borderWidth = 1.0f;
    
    _txtSearch.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtSearch.layer.borderWidth = 1.0f;
    
    _txtSort.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtSort.layer.borderWidth = 1.0f;
    
    _txtCode.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtCode.layer.borderWidth = 1.0f;
    
    
//    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 1200, 0);
    self.tableView.separatorColor = [UIColor clearColor];
    
    
    [_tableView setShowsHorizontalScrollIndicator:NO];
    [_tableView setShowsVerticalScrollIndicator:NO];
//    _btnAddClass.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
//    _btnAddClass.layer.borderWidth = 1.0f;
    
    [self.tableView setSeparatorColor:[UIColor clearColor]];
    
    [_tableView setShowsHorizontalScrollIndicator:NO];
    [_tableView setShowsVerticalScrollIndicator:NO];
}
- (IBAction)gradeSelectAction:(id)sender {
    
    if(self.gradeTable.hidden == YES){
        
        [UIView transitionWithView: self.gradeTable
                          duration: 0.35f
                           options: UIViewAnimationOptionTransitionCurlDown
                        animations: ^(void)
         {
             self.gradeTable.hidden = NO;
         }
                        completion: nil];
        
    }else{
        
        [UIView transitionWithView: self.gradeTable
                          duration: 0.35f
                           options: UIViewAnimationOptionTransitionCurlUp
                        animations: ^(void)
         {
             self.gradeTable.hidden = YES;
         }
                        completion: nil];
    }
}

- (IBAction)sortSelectAction:(id)sender {
    
    if(self.sortTable.hidden == YES){
        
        [UIView transitionWithView: self.sortTable
                          duration: 0.35f
                           options: UIViewAnimationOptionTransitionCurlDown
                        animations: ^(void)
         {
             self.sortTable.hidden = NO;
         }
                        completion: nil];
        
    }else{
        
        [UIView transitionWithView: self.sortTable
                          duration: 0.35f
                           options: UIViewAnimationOptionTransitionCurlUp
                        animations: ^(void)
         {
             self.sortTable.hidden = YES;
         }
                        completion: nil];
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [self.txtGrade resignFirstResponder];
    [self.txtSort resignFirstResponder];
    return true;
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(tableView == _tableView){
        return self.students.count + 2;
    }else if(tableView == _gradeTable){
        return self.grades.count;
    }else if(tableView == _sortTable){
        return self.sortOrders.count;
    }else{
        return 0;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView == _tableView){
        
        if(indexPath.row < self.self.students.count){
            return 120;
        }
        else  if(indexPath.row == self.students.count){
            return 140;
        }
        else  if(indexPath.row == self.students.count + 1){
            return 386;
        }
        else {
            return 0;
        }
    }else{
        return 40;
    }
    
}
-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *) cell forRowAtIndexPath:(NSIndexPath *)indexPath // replace "postTableViewCell" with your cell
{
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.tableHeightConstraint.constant = 1600 + self.students.count * 120;
    self.mainViewHeightConstraint.constant = 1700 + self.students.count * 120;
    
    if(tableView == _tableView){
        
        static NSString *CellIdentifier;
        UITableViewCell *cell;
        
        if (indexPath.row <self.students.count)
        {
            CellIdentifier = @"Cell";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            UILabel *firstNameLabel = (UILabel *)[cell viewWithTag:800];
            UILabel *lastNameLabel = (UILabel *)[cell viewWithTag:801];
            UIImageView *checkImage = (UIImageView *)[cell viewWithTag:802];
            
            firstNameLabel.text = self.students[indexPath.row][@"name"];
            lastNameLabel.text = self.students[indexPath.row][@"surname"];
            
            NSString *user_id = self.students[indexPath.row][@"id"];
            if ([self.selectedUserIds containsObject:user_id]) {
                checkImage.hidden = NO;
            } else {
                checkImage.hidden = YES;
            }
            
            
        }
        else if (indexPath.row == self.students.count) {
            CellIdentifier = @"Cell1";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
           
            UIButton *recipeNameButton3 = (UIButton *)[cell viewWithTag:200];
            recipeNameButton3.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
            recipeNameButton3.layer.borderWidth = 0.5f;
            
            
            UIButton *topButton = (UIButton *)[cell viewWithTag:805];
            [topButton addTarget: self
                          action: @selector(topButtonTap)
                forControlEvents: UIControlEventTouchUpInside];
            UIButton *bottomButton = (UIButton *)[cell viewWithTag:806];
            [bottomButton addTarget: self
                             action: @selector(bottomButtonTap)
                   forControlEvents: UIControlEventTouchUpInside];
            
            NSString *topTitle = @"Cancel";
            NSString *bottomTitle = @"Add Class";
            if (self.data) {
                topTitle = @"Delete";
                bottomTitle = @"Save Changes";
            }
            [topButton setTitle:topTitle forState:UIControlStateNormal];
            [bottomButton setTitle:bottomTitle forState:UIControlStateNormal];
            
            
        }
        else if (indexPath.row == self.students.count+1) {
            CellIdentifier = @"Cell2";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            UIButton *recipeNameButton3 = (UIButton *)[cell viewWithTag:201];
            recipeNameButton3.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
            recipeNameButton3.layer.borderWidth = 0.5f;
            
            UIButton *recipeNameButton4 = (UIButton *)[cell viewWithTag:202];
            recipeNameButton4.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
            recipeNameButton4.layer.borderWidth = 0.5f;
            
            UIButton *recipeNameButton5 = (UIButton *)[cell viewWithTag:203];
            recipeNameButton5.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
            recipeNameButton5.layer.borderWidth = 0.5f;
            
            UIButton *recipeNameButton6 = (UIButton *)[cell viewWithTag:204];
            recipeNameButton6.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
            recipeNameButton6.layer.borderWidth = 0.5f;
            
            UIButton *addStudentButton = (UIButton *)[cell viewWithTag:202];
            UIButton *inviteButton = (UIButton *)[cell viewWithTag:203];
            
            [addStudentButton addTarget: self
                                 action: @selector(addStudentButtonTap)
                       forControlEvents: UIControlEventTouchUpInside];
            [inviteButton addTarget: self
                             action: @selector(inviteButtonTap)
                   forControlEvents: UIControlEventTouchUpInside];
            
            UILabel *educatorName = (UILabel *)[cell viewWithTag:906];
            NSString *name = concat([[UserCache sharedInstance] getUserInfo][@"name"], @" ");
            name = concat(name, [[UserCache sharedInstance] getUserInfo][@"surname"]);
            educatorName.text = name;
        }
        
        // Configure the cell...
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        return cell;
        
    }
    
    if(tableView == _gradeTable){
        
        DropDownCell *cell = [tableView dequeueReusableCellWithIdentifier:@"gradeCell"];
        cell.dropDownButton.tag = indexPath.row;
        [cell.dropDownButton setTitle:self.grades[indexPath.row] forState:normal];
        [cell.dropDownText setFont:[UIFont systemFontOfSize:17]];
        
        return cell;
    }
    
    if(tableView == _sortTable){
        
        DropDownCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sortCell"];
        cell.dropDownButton.tag = indexPath.row;
        [cell.dropDownButton setTitle:self.sortOrders[indexPath.row] forState:normal];
        [cell.dropDownText setFont:[UIFont systemFontOfSize:17]];
        
        return cell;
    }else{
        return nil;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView == _tableView){
        
        if (indexPath.row <self.students.count) {
            
            NSString *user_id = self.students[indexPath.row][@"id"];
            if ([self.selectedUserIds containsObject:user_id]) {
                [self.selectedUserIds removeObject:user_id];
            } else {
                [self.selectedUserIds addObject:user_id];
            }
            [tableView reloadData];
            
        } else if (indexPath.row == self.students.count) {
            
        } else if (indexPath.row == self.students.count+1) {
            
        }
    }
    
    if(tableView == _gradeTable){

        DropDownCell *cell = [self.gradeTable cellForRowAtIndexPath:indexPath];
        self.currentGradeSelection = indexPath.row;
        self.txtGrade.text = cell.dropDownText.text;
        self.gradeTable.hidden = YES;
    }

    if(tableView == _sortTable){

        DropDownCell *cell = [self.gradeTable cellForRowAtIndexPath:indexPath];
        self.currentSortSelection = indexPath.row;
        self.txtSort.text = cell.dropDownText.text;
        self.sortTable.hidden = YES;
    }
}

- (void)showLoader {
    if (!dimView) {
        dimView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
        dimView.backgroundColor = [UIColor blackColor];
        dimView.alpha = 0.7f;
        dimView.userInteractionEnabled = NO;
    }
    [self.view addSubview:dimView];
    CGRect newFrame = dimView.frame;
    
    newFrame.size.width = 1200;
    newFrame.size.height = 1500;
    [dimView setFrame:newFrame];
    [dimView setCenter:CGPointMake(187.0f, 333.0f)];
    
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeLineScale tintColor:[UIColor whiteColor] size:40.0f];
    activityIndicatorView.center = self.view.center;
    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
}

- (void)hideLoader {
    [dimView removeFromSuperview];
    [activityIndicatorView stopAnimating];
}

- (void)addStudentButtonTap {
    [self performSegueWithIdentifier:@"addstudent" sender:self];
}

- (void)inviteButtonTap {
    [self performSegueWithIdentifier:@"invite" sender:self];
}

- (void)topButtonTap {
    if (self.data) {
        [self showLoader];
        NSString *url = concat(@"services/app/Classroom/Delete?id=", self.data[@"id"]);
        [NetworkHelper placeDeleteRequest:nil relativeUrl:url withSuccess:^(NSDictionary *jsonObject) {
            [self hideLoader];
            [self showErrorMessage:@"Successfully Deleted Class"];
            
            @try {
                [self.navigationController popToViewController:self.navigationController.viewControllers[self.navigationController.viewControllers.count-3] animated:YES];
            } @catch (NSException *exception) {
                [self.navigationController popViewControllerAnimated:YES];
            }
        } andFailure:^(NSString *string) {
            [self hideLoader];
            if (!string) {
                string = @"error deleting class";
            }
            [self showErrorMessage:string];
        }];
        
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)showErrorMessage:(NSString *)string {
    if (!string) {
        string = @"";
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Character.ly"
                                                    message:string
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (IBAction)actionSelectColor:(ColorButton *)sender {
    
    for (ColorButton *colorSelectButton in self.colorButtonGroup){
        colorSelectButton.layer.borderColor = [self colorWithHexString:@"ffffff"].CGColor;
    }
    
    if(sender.tag == 1){
        self.colorCode = @"FF2600";
        sender.layer.borderColor = [self colorWithHexString:@"000000"].CGColor;
    }
    if(sender.tag == 2){
        self.colorCode = @"FF9300";
        sender.layer.borderColor = [self colorWithHexString:@"000000"].CGColor;
    }
    if(sender.tag == 3){
        self.colorCode = @"FFFB00";
        sender.layer.borderColor = [self colorWithHexString:@"000000"].CGColor;
    }
    if(sender.tag == 4){
        self.colorCode = @"00F900";
        sender.layer.borderColor = [self colorWithHexString:@"000000"].CGColor;
    }
    if(sender.tag == 5){
        self.colorCode = @"00FDFF";
        sender.layer.borderColor = [self colorWithHexString:@"000000"].CGColor;
    }
    if(sender.tag == 6){
        self.colorCode = @"942192";
        sender.layer.borderColor = [self colorWithHexString:@"000000"].CGColor;
    }
    if(sender.tag == 7){
        self.colorCode = @"FF40FF";
        sender.layer.borderColor = [self colorWithHexString:@"000000"].CGColor;
    }
    if(sender.tag == 8){
        self.colorCode = @"000000";
        sender.layer.borderColor = [self colorWithHexString:@"000000"].CGColor;
    }
    if(sender.tag == 9){
        self.colorCode = @"FFFFFF";
        sender.layer.borderColor = [self colorWithHexString:@"000000"].CGColor;
    }
}


- (void)bottomButtonTap {
    NSString *branchId = [[UserCache sharedInstance] getUserInfo][@"branchId"];
    NSString *educatorId = [[UserCache sharedInstance] getUserInfo][@"educator"][@"id"];
    NSString *gradeId = self.gradeModel[self.currentGradeSelection][@"id"];
    
    NSDictionary *dataToSend = @{
                                 @"BranchId" : branchId,
                                 @"EducatorId" : educatorId,
                                 @"Name" : self.txtClassName.text,
                                 @"ColorCode" : self.colorCode,
                                 @"GradeId" : gradeId,
                                 @"Students" : self.selectedUserIds
                                 };
    
    [self showLoader];
    if (self.data) {
        dataToSend = @{
                       @"BranchId" : branchId,
                       @"EducatorId" : educatorId,
                       @"Name" : self.txtClassName.text,
                       @"ColorCode" : self.colorCode,
                       @"ClassCode" : self.data[@"classCode"],
                       @"GradeId" : gradeId,
                       @"Students" : self.selectedUserIds,
                       @"id" : self.data[@"id"]
                       };
        [NetworkHelper placePutRequest:dataToSend relativeUrl:@"services/app/Classroom/Update" withSuccess:^(NSDictionary *jsonObject) {
            [self hideLoader];
            [self showErrorMessage:@"Successfully updated Class"];
            [self.navigationController popViewControllerAnimated:YES];
            
        } andFailure:^(NSString *string) {
            [self hideLoader];
            if (!string) {
                string = @"error updating class";
            }
            [self showErrorMessage:string];
        }];
    } else {
        [NetworkHelper placePostRequest:dataToSend relativeUrl:@"services/app/Classroom/Create" withSuccess:^(NSDictionary *jsonObject) {
            [self hideLoader];
            [self showErrorMessage:@"Successfully Added Class"];
            [self.navigationController popViewControllerAnimated:YES];
            
        } andFailure:^(NSString *string) {
            [self hideLoader];
            [self showErrorMessage:string];
        }];
    }
}


- (IBAction)sortViewGs:(id)sender {
}

- (void)getGrades {
    [NetworkHelper placeGetRequest:nil relativeUrl:@"services/app/Grade/GetAll" withSuccess:^(NSDictionary *jsonObject) {
        
        self.gradeModel = jsonObject[@"result"][@"items"];
        for (int i = 0; i < self.gradeModel.count ; i++) {
            NSDictionary *grade = self.gradeModel[i];
            [self.grades addObject:grade[@"name"]];
            if (self.data && [grade[@"id"] isEqualToString:self.data[@"gradeId"]]) {
                self.currentGradeSelection = i;
                self.txtGrade.text = grade[@"name"];
            }
        }
        [self.gradeTable reloadData];
        
    } andFailure:^(NSString *string) {
        NSLog(@"getGrades Failure:%@", string);
    }];
}

- (void)getStudents {
    [NetworkHelper placeGetRequest:nil relativeUrl:@"services/app/Student/GetAll" withSuccess:^(NSDictionary *jsonObject) {
        [self.students removeAllObjects];
        [self.students addObjectsFromArray:jsonObject[@"result"]];
        [self.tableView reloadData];

    } andFailure:^(NSString *string) {
        NSLog(@"getStudents Failure:%@", string);
        [self showErrorMessage:string];
    }];
}

- (void)searchStudent:(NSString *)value {
    [self showLoader];
    NSDictionary *dataToSend = @{
                                 @"searchText" : value,
                                 @"sortOrder" : @(self.currentSortSelection),
                                 @"maxResultCount" : @"20",
                                 @"skipCount" : @"0"};
    
    
    
    [NetworkHelper placePostRequest:dataToSend relativeUrl:@"services/app/Student/Search" withSuccess:^(NSDictionary *jsonObject) {
            [self hideLoader];
            if (((NSArray *)jsonObject[@"result"]).count == 0) {
               [self showErrorMessage:[NSString stringWithFormat:@"No results found for:  %@", value]];
            }
            [self.students removeAllObjects];
            [self.students addObjectsFromArray:jsonObject[@"result"]];
            [self.tableView reloadData];
        } andFailure:^(NSString *string) {
            [self hideLoader];
            NSLog(@"loginFailure:");
            [self showErrorMessage:string];
        }];
}

- (void)getClassroomStudents {
    if (!self.data) {
        return;
    }
    NSString *url = concat(@"services/app/Student/GetByClassroomId?id=", self.data[@"id"]);
    [NetworkHelper placeGetRequest:nil relativeUrl:url withSuccess:^(NSDictionary *jsonObject) {
        NSArray *students = jsonObject[@"result"];

        for (NSDictionary *student in students) {
            [self.selectedUserIds addObject:student[@"id"]];
        }
        [self.tableView reloadData];
        
    } andFailure:^(NSString *string) {
        NSLog(@"getClassroomStudents Failure:%@", string);
        //               [self showErrorMessage:string];
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    NSLog(@"return pressed");
    if(textField == _txtSearch){
        [self searchStudent:textField.text];
    }
    return YES;
}
- (IBAction)selectImage:(UIButton *)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Choose image" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
        //Cancel button tapped.
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:nil];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:nil];
        
    }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

- (NSString *)imageToNSString:(UIImage *)image {
    NSData *imageData = UIImagePNGRepresentation(image);
    return [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.chosenImage.image = chosenImage;
    NSString *imageString = [self imageToNSString:chosenImage];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    NSDictionary *dataToSend = @{
                                 @"image" : imageString
                                 };
    [self showLoader];
    
    [NetworkHelper placePostRequest:dataToSend relativeUrl:@"services/app/FileAttachment/Upload" withSuccess:^(NSDictionary *jsonObject) {
        [self hideLoader];
        [self showErrorMessage:@"Successfully Added Class"];
        [self.navigationController popViewControllerAnimated:YES];
        
    } andFailure:^(NSString *string) {
        [self hideLoader];
        [self showErrorMessage:string];
    }];
    
}

- (IBAction)actionSortCell:(UIButton *)sender {
    
    self.txtSort.text = self.sortOrders[sender.tag];
    self.currentSortSelection = sender.tag;
    
    NSLog(@"sender tag: %li", sender.tag);
    
    [UIView transitionWithView: self.sortTable
                      duration: 0.35f
                       options: UIViewAnimationOptionTransitionCurlUp
                    animations: ^(void)
     {
         self.sortTable.hidden = YES;
     }
                    completion: nil];
    
    sender.backgroundColor = [UIColor whiteColor];
    [sender setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
}

- (IBAction)changeBackgroundColorSort:(UIButton *)sender {
    sender.backgroundColor = [UIColor colorWithRed:95.0f/255.0f green:96.0f/255.0f blue:171.0f/255.0f alpha:1.0f];
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
}

- (IBAction)actionGradeCell:(UIButton *)sender {
    
    self.txtGrade.text = self.grades[sender.tag];
    self.currentGradeSelection = sender.tag;
    
    NSLog(@"sender tag: %li", sender.tag);
    
    [UIView transitionWithView: self.gradeTable
                      duration: 0.35f
                       options: UIViewAnimationOptionTransitionCurlUp
                    animations: ^(void)
     {
         self.gradeTable.hidden = YES;
     }
                    completion: nil];
    
    sender.backgroundColor = [UIColor whiteColor];
    [sender setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
}

- (IBAction)changeBackgroundColorGrade:(UIButton *)sender {
    sender.backgroundColor = [UIColor colorWithRed:95.0f/255.0f green:96.0f/255.0f blue:171.0f/255.0f alpha:1.0f];
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
}

- (IBAction)actionImage:(id)sender {
    _ColorSelectView.hidden = YES;
    _imageSelectView.hidden = NO;
    _btnImage.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _btnImage.layer.borderWidth = 0.5f;
    _btnColor.layer.borderColor = [UIColor clearColor].CGColor;
    _btnColor.layer.borderWidth = 0.5f;
}

- (IBAction)actionColor:(id)sender {
    _ColorSelectView.hidden = NO;
    _imageSelectView.hidden = YES;
    _btnImage.layer.borderColor =  [UIColor clearColor].CGColor;
    _btnImage.layer.borderWidth = 0.5f;
    _btnColor.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _btnColor.layer.borderWidth = 0.5f;
}
@end
