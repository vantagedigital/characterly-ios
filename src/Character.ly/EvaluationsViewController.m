//
//  EvaluationsViewController.m
//  Character.ly
//
//  Created by Adeel Nasir on 22/08/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "EvaluationsViewController.h"
#import "NavViewController.h"
#import "DGActivityIndicatorView.h"
#import "EvaluationCheckViewController.h"

@interface EvaluationsViewController (){
    NSArray *_pickerData;
    NSArray *_gradesData;
    NSArray * objects;
    UIView *dimView;
    DGActivityIndicatorView *activityIndicatorView;
}

@property (strong, nonatomic) NSMutableArray *evaluations;
@property (strong, nonatomic) NSDictionary *evaluation;
@property (assign, nonatomic) BOOL isEducator;
@property (strong, nonatomic) NSMutableArray *classrooms;
@property (strong, nonatomic) NSMutableArray <NSString *> *gradeData;
@property (strong, nonatomic) NSMutableArray <NSString *> *classData;
@property (strong, nonatomic) NSMutableArray <NSString *> *sortData;
@property (strong, nonatomic) NSArray *gradeModel;
@property (assign, nonatomic) NSInteger currentSortSelection;

@end

@implementation EvaluationsViewController {
     TLTagsControl *demoTagsControl;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.headerView.hidden = YES;
    
    [self.sortButton.layer setBorderWidth:0.5];
    [self.sortButton.layer setBorderColor:[[UIColor grayColor] CGColor]];
    [self.gradeButton.layer setBorderWidth:0.5];
    [self.gradeButton.layer setBorderColor:[[UIColor grayColor] CGColor]];
    [self.classButton.layer setBorderWidth:0.5];
    [self.classButton.layer setBorderColor:[[UIColor grayColor] CGColor]];
    
    self.sortTable.delegate = self;
    self.sortTable.dataSource = self;
    self.sortTable.hidden = YES;
    self.sortTable.editing = NO;
    
    self.classTable.delegate = self;
    self.classTable.dataSource = self;
    self.classTable.hidden = YES;
    self.classTable.editing = NO;
    
    self.gradeTable.delegate = self;
    self.gradeTable.dataSource = self;
    self.gradeTable.hidden = YES;
    self.gradeTable.editing = NO;
    
    [self getSortData];
    [self getGradeData];
    [self getClassData];
    
    self.evaluations = [NSMutableArray array];
    
    [self getEvaluations];
    
    self.navigationController.navigationBar.hidden = YES;
    
    NavViewController *myViewController2 = [[NavViewController alloc] initForAutoLayout];
    [self.view addSubview:myViewController2];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:[UIApplication sharedApplication].statusBarFrame.size.height];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
    [myViewController2 autoSetDimension:ALDimensionHeight toSize:200];
    myViewController2.parent = self;
    
    [_scoller setScrollEnabled:NO];
    [_scoller setContentSize:CGSizeMake(375, 1850)];
    
    
    NSMutableArray *tags = [NSMutableArray arrayWithArray:@[@"Trustworthines", @"Respect", @"Caring"]];
    _defaultEditingTagControl.tags = [tags mutableCopy];
    _defaultEditingTagControl.tagPlaceholder = @"Placeholder";
     [_defaultEditingTagControl reloadTagSubviews];
    
   
    [self.view addSubview:demoTagsControl];
    
    [_tableView setShowsHorizontalScrollIndicator:NO];
    [_tableView setShowsVerticalScrollIndicator:NO];
    
    [self.tableView setSeparatorColor:[UIColor clearColor]];
    
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 700, 0);
  
}

- (void) getClassData {
    
    self.isEducator = ![[[UserCache sharedInstance] getUserInfo][@"educator"] isKindOfClass:[NSNull class]];
    
    if (self.isEducator) {
        self.classrooms = [[UserCache sharedInstance] getUserInfo][@"educator"][@"classrooms"];
    } else {
        self.classrooms = [[UserCache sharedInstance] getUserInfo][@"student"][@"classrooms"];
    }
    
    for(int i = 0; i < self.classrooms.count;i++) {
        [self.classData addObject:[self.classrooms[i] valueForKey:@"name"]];
    }
    NSLog(@"class count: %li", self.classData.count);
}

- (void) getSortData {
    
    self.sortData = @[@"Descending", @"Ascending"].mutableCopy;
}

- (void) getGradeData {
    
    [NetworkHelper placeGetRequest:nil relativeUrl:@"services/app/Grade/GetAll" withSuccess:^(NSDictionary *jsonObject) {
        
        self.gradeModel = jsonObject[@"result"][@"items"];
        for (int i = 0; i < self.gradeModel.count ; i++) {
            NSDictionary *grade = self.gradeModel[i];
            [self.gradeData addObject:grade[@"name"]];
        }
        [self.gradeTable reloadData];
        NSLog(@"grade count: %li", self.gradeData.count);
        
    } andFailure:^(NSString *string) {
        NSLog(@"getGrades Failure:%@", string);
    }];
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

-(void) ShowSelectedDate {
    [_txtSort resignFirstResponder];
    [_txtGrades resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - TLTagsControlDelegate
- (void)tagsControl:(TLTagsControl *)tagsControl tappedAtIndex:(NSInteger)index {
    NSLog(@"Tag \"%@\" was tapped", tagsControl.tags[index]);
}

- (void)tagsControl:(TLTagsControl *)tagsControl removedAtIndex:(NSInteger)index {
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"evaluations"]) {
        // Get reference to the destination view controller
        EvaluationCheckViewController *vc = [segue destinationViewController];
        vc.evaluation = self.evaluation;
    }
    
    if ([[segue identifier] isEqualToString:@"evaluation"]) {
        Evaluation1ViewController *vc = [segue destinationViewController];
        vc.evaluation = self.evaluation;
    }
}

- (void)getEvaluations {
    [self showLoader];
    [NetworkHelper placeGetRequest:nil relativeUrl:@"services/app/Evaluation/GetAll" withSuccess:^(NSDictionary *jsonObject) {
        [self hideLoader];
        [self.evaluations removeAllObjects];
        [self.evaluations addObjectsFromArray:jsonObject[@"result"]];
        [self.tableView reloadData];
        
        NSLog(@"evaluations count: %li", self.evaluations.count);
        NSLog(@"All evaluations: %@", self.evaluations);
        
    } andFailure:^(NSString *string) {
        NSLog(@"getStudents Failure:%@", string);
        //               [self showErrorMessage:string];
    }];
}

- (void)showLoader {
    if (!dimView) {
        dimView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
        dimView.backgroundColor = [UIColor blackColor];
        dimView.alpha = 0.7f;
        dimView.userInteractionEnabled = NO;
    }
    [self.view addSubview:dimView];
    CGRect newFrame = dimView.frame;
    
    newFrame.size.width = 1200;
    newFrame.size.height = 1500;
    [dimView setFrame:newFrame];
    [dimView setCenter:CGPointMake(187.0f, 333.0f)];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeLineScale tintColor:[UIColor whiteColor] size:40.0f];
    activityIndicatorView.center = self.view.center;
    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
}

- (void)showErrorMessage:(NSString *)string {
    if (!string) {
        string = @"";
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Character.ly"
                                                    message:string
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)hideLoader {
    [dimView removeFromSuperview];
    [activityIndicatorView stopAnimating];
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *) cell forRowAtIndexPath:(NSIndexPath *)indexPath // replace "postTableViewCell" with your cell
{
    
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(tableView == _tableView){
        return self.evaluations.count;
    }else if(tableView == _sortTable){
        return self.sortData.count;
    }else if(tableView == _classTable){
        return self.classData.count;
    }else if(tableView == _gradeTable){
        return self.gradeData.count;
    }else{
        return 0;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger row = [indexPath row];
    
    if(tableView == _tableView){
        
        if(_isEducator){
            self.evaluation = self.evaluations[row];
            [self performSegueWithIdentifier:@"evaluations" sender:self];
        }else{
            self.evaluation = self.evaluations[row];
            [self performSegueWithIdentifier:@"evaluation" sender:nil];
        }
        
    }
    
    if(tableView == _sortTable){
        
    }
    
    if(tableView == _classTable){
        
    }
    
    if(tableView == _gradeTable){
        
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView == _tableView) {
        
        static NSString *CellIdentifier = @"pink_cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        
        // Configure the cell...
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        UILabel *titleLabel = (UILabel *)[cell viewWithTag:902];
        UILabel *dateLabel = (UILabel *)[cell viewWithTag:903];
        UIView *view1 = (UIView *)[cell viewWithTag:11];
        UIView *view2 = (UIView *)[cell viewWithTag:12];
        UIView *view3 = (UIView *)[cell viewWithTag:13];
        UIView *view4 = (UIView *)[cell viewWithTag:14];
        UIView *view5 = (UIView *)[cell viewWithTag:15];
        UIView *view6 = (UIView *)[cell viewWithTag:16];
        UIView *view7 = (UIView *)[cell viewWithTag:17];
        UIView *view8 = (UIView *)[cell viewWithTag:18];
        UIView *view9 = (UIView *)[cell viewWithTag:19];
        
        view1.layer.cornerRadius = view1.frame.size.height / 2;
        view2.layer.cornerRadius = view2.frame.size.height / 2;
        view3.layer.cornerRadius = view3.frame.size.height / 2;
        view4.layer.cornerRadius = view4.frame.size.height / 2;
        view5.layer.cornerRadius = view5.frame.size.height / 2;
        view6.layer.cornerRadius = view6.frame.size.height / 2;
        view7.layer.cornerRadius = view7.frame.size.height / 2;
        view8.layer.cornerRadius = view8.frame.size.height / 2;
        view9.layer.cornerRadius = view9.frame.size.height / 2;
        view9.layer.borderColor = [self colorWithHexString:@"111111"].CGColor;
        view9.layer.borderWidth = 0.5;
        
        titleLabel.text = self.evaluations[indexPath.row][@"name"];
        dateLabel.text = @"11/23/2017";
        return cell;
        
    } else if(tableView == _sortTable) {
        
        DropDownCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sortCell"];
        cell.dropDownButton.tag = indexPath.row;
        [cell.dropDownButton setTitle:self.sortData[indexPath.row] forState:normal];
        cell.dropDownText.hidden = YES;
        return cell;
        
    } else if(tableView == _classTable) {
        
        DropDownCell *cell = [tableView dequeueReusableCellWithIdentifier:@"classCell"];
        cell.dropDownButton.tag = indexPath.row;
        [cell.dropDownButton setTitle:self.classData[indexPath.row] forState:normal];
        cell.dropDownText.hidden = YES;
        return cell;
        
    } else if(tableView == _gradeTable) {
        
        DropDownCell *cell = [tableView dequeueReusableCellWithIdentifier:@"gradeCell"];
        cell.dropDownButton.tag = indexPath.row;
        [cell.dropDownButton setTitle:self.gradeData[indexPath.row] forState:normal];
        cell.dropDownText.hidden = YES;
        return cell;
        
    } else {
        return nil;
    }
    
    
}

- (IBAction)selectSortButton:(id)sender {
    
    if(self.sortTable.hidden == YES){
        
        [UIView transitionWithView: self.sortTable
                          duration: 0.35f
                           options: UIViewAnimationOptionTransitionCurlDown
                        animations: ^(void)
         {
             self.sortTable.hidden = NO;
         }
                        completion: nil];
        
    }else{
        
        [UIView transitionWithView: self.sortTable
                          duration: 0.35f
                           options: UIViewAnimationOptionTransitionCurlUp
                        animations: ^(void)
         {
             self.sortTable.hidden = YES;
         }
                        completion: nil];
    }
}

- (IBAction)actionSortButton:(UIButton *)sender {
    
    self.txtSort.text = self.sortData[sender.tag];
    self.currentSortSelection = sender.tag;
    
    [UIView transitionWithView: self.sortTable
                      duration: 0.35f
                       options: UIViewAnimationOptionTransitionCurlUp
                    animations: ^(void)
     {
         self.sortTable.hidden = YES;
     }
                    completion: nil];
    
    sender.backgroundColor = [UIColor whiteColor];
    [sender setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
}

- (IBAction)selectGradeButton:(id)sender {
    
    if(self.gradeTable.hidden == YES){
        
        [UIView transitionWithView: self.gradeTable
                          duration: 0.35f
                           options: UIViewAnimationOptionTransitionFlipFromBottom
                        animations: ^(void)
         {
             self.gradeTable.hidden = NO;
         }
                        completion: nil];
        
    }else{
        
        [UIView transitionWithView: self.gradeTable
                          duration: 0.35f
                           options: UIViewAnimationOptionTransitionFlipFromTop
                        animations: ^(void)
         {
             self.gradeTable.hidden = YES;
         }
                        completion: nil];
    }
}

- (IBAction)actionGradeButton:(UIButton *)sender {
    
    self.txtGrades.text = self.gradeData[sender.tag];
    
    [UIView transitionWithView: self.gradeTable
                      duration: 0.35f
                       options: UIViewAnimationOptionTransitionFlipFromTop
                    animations: ^(void)
     {
         self.gradeTable.hidden = YES;
     }
                    completion: nil];
    
    sender.backgroundColor = [UIColor whiteColor];
    [sender setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
}

- (IBAction)selectClassButton:(id)sender {
    
    if(self.classTable.hidden == YES){
        
        [UIView transitionWithView: self.classTable
                          duration: 0.35f
                           options: UIViewAnimationOptionTransitionFlipFromBottom
                        animations: ^(void)
         {
             self.classTable.hidden = NO;
         }
                        completion: nil];
        
    }else{
        
        [UIView transitionWithView: self.classTable
                          duration: 0.35f
                           options: UIViewAnimationOptionTransitionFlipFromTop
                        animations: ^(void)
         {
             self.classTable.hidden = YES;
         }
                        completion: nil];
    }
}

- (IBAction)actionClassButton:(UIButton *)sender {
    
    self.txtClass.text = self.classData[sender.tag];
    
    [UIView transitionWithView: self.classTable
                      duration: 0.35f
                       options: UIViewAnimationOptionTransitionFlipFromTop
                    animations: ^(void)
     {
         self.classTable.hidden = YES;
     }
                    completion: nil];
    
    sender.backgroundColor = [UIColor whiteColor];
    [sender setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
}


@end
