//
//  Evaluation1ViewController.m
//  Character.ly
//
//  Created by Adeel Nasir on 17/08/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "Evaluation1ViewController.h"
#import "NavViewController.h"
#import "DGActivityIndicatorView.h"

@interface Evaluation1ViewController () {
    UIView *dimView;
    DGActivityIndicatorView *activityIndicatorView;
}
@property (weak, nonatomic) IBOutlet UIView *selectionView;
@property (weak, nonatomic) IBOutlet UITableView *questionTable;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
@property (weak, nonatomic) IBOutlet UIButton *prevBtn;
@property (weak, nonatomic) IBOutlet UILabel *titleText;
@property (weak, nonatomic) IBOutlet UILabel *questionOrder;
@property (weak, nonatomic) IBOutlet UILabel *questionDescription;
@property (strong, nonatomic) NSString *answerString;
@property (assign, nonatomic) NSInteger questionNumber;
@property (assign, nonatomic) NSInteger questionTypeNumber;
@property (strong, nonatomic) NSMutableArray <NSDictionary *> *answerArray;
@property (strong, nonatomic) NSMutableArray <NSDictionary *> *selectedOptions;
@property (strong, nonatomic) NSString *evaluationQuestionId;
@property (weak, nonatomic) IBOutlet UITextField *answerLabel;

@end

@implementation Evaluation1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = YES;
    NavViewController *myViewController2 = [[NavViewController alloc] init];
    
    //  [myViewController2.view addSubView:mySubView];
    // add any other views to myViewController2's view
    
    [self.view addSubview:myViewController2];
    
    myViewController2.frame = self.navigationController.navigationBar.frame;
    myViewController2.frame = CGRectMake(myViewController2.frame.origin.x, [UIApplication sharedApplication].statusBarFrame.size.height, myViewController2.frame.size.width, myViewController2.frame.size.height);
    // Do any additional setup after loading the view.
    
    self.answerArray = [NSMutableArray array];
    self.selectedOptions = [NSMutableArray array];
    
    self.prevBtn.layer.borderWidth = 0.5;
    self.prevBtn.layer.borderColor = [UIColor grayColor].CGColor;
    self.prevBtn.hidden = YES;
    
    self.questionTable.delegate = self;
    self.questionTable.dataSource = self;
   
    self.titleText.text = self.evaluation[@"name"];
    _questionNumber = 0;
    self.questionTypeNumber = 9;
    self.questions = self.evaluation[@"questions"];
    
    if(self.questions.count > 0){
        self.question = self.questions[_questionNumber];
        
        self.evaluationQuestionId = self.question[@"id"];
        self.questionTypeNumber = [self.question[@"questionType"] integerValue];
        self.questionDescription.text = self.question[@"description"];
        self.questionOrder.text = [NSString stringWithFormat:@"Question %li out of %li", _questionNumber + 1, self.questions.count];
        
        NSLog(@"question type number: %li", self.questionTypeNumber);
    }else{
        [self disableNext];
    }
    
    if(self.questions.count == 1){
        [self.nextBtn setTitle:@"Submit" forState:UIControlStateNormal];
    }
    
    NSLog(@"evaluation data: %@", self.evaluation);
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(self.questionTypeNumber > 1 && self.questionTypeNumber < 9) {
        NSArray *options = self.question[@"questionOptions"];
        return options.count;
    }else if(self.questionTypeNumber < 2){
        return 1;
    }else{
        return 0;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self enableNext];
    if(self.questionTypeNumber == 2 || self.questionTypeNumber == 3 || self.questionTypeNumber == 4 || self.questionTypeNumber == 5){
        
        //single choice
        if(self.questionTypeNumber == 2 || self.questionTypeNumber == 4){
            [self.selectedOptions removeAllObjects];
        }
        NSArray *questionOptions = self.question[@"questionOptions"];
        NSDictionary *selectedQuestionOption = @{
                                                 @"evaluationQuestionOptionId": questionOptions[indexPath.row][@"id"]
                                                 };
        NSLog(@"%@", selectedQuestionOption);
        [self.selectedOptions addObject:selectedQuestionOption];
        NSLog(@"%li", self.selectedOptions.count);
        NSLog(@"%@", [self.selectedOptions objectAtIndex:0]);
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self disableNext];
    
    if(self.questionTypeNumber == 0 || self.questionTypeNumber == 1)
    {
        QuestionTextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"optionText"];
        [cell.answerTextField addTarget:self action:@selector(updateAnswerString:) forControlEvents:UIControlEventEditingChanged];
        
        if(cell == nil) {
            NSArray *cellObjects = [[NSBundle mainBundle] loadNibNamed:@"QuestionTextTableViewCell" owner:self options:nil];
            cell = [cellObjects objectAtIndex:0];
        }
        
        return cell;
    }
    else if(self.questionTypeNumber == 2 || self.questionTypeNumber == 3)
    {
        QuestionOptionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"optionCell"];
        
        if(cell == nil) {
            NSArray *cellObjects = [[NSBundle mainBundle] loadNibNamed:@"QuestionOptionTableViewCell" owner:self options:nil];
            cell = [cellObjects objectAtIndex:0];
        }
        
        NSArray *questionOptions = self.question[@"questionOptions"];
        NSDictionary *options = questionOptions[indexPath.row];
        
        cell.optionDescription.text = options[@"description"];
        
        return cell;
        
    }
    else if(self.questionTypeNumber == 2 || self.questionTypeNumber == 3)
    {
        QuestionOptionImageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"optionImage"];
        
        if(cell == nil) {
            NSArray *cellObjects = [[NSBundle mainBundle] loadNibNamed:@"QuestionOptionImageTableViewCell" owner:self options:nil];
            cell = [cellObjects objectAtIndex:0];
        }
        
        NSArray *questionOptions = self.question[@"questionOptions"];
        NSDictionary *options = questionOptions[indexPath.row];
        
        cell.optionDescription.text = options[@"description"];
        
        if(![options[@"fileAttachment"] isKindOfClass:[NSNull class]]){
            [cell.optionImageView sd_setImageWithURL:options[@"fileAttachment"][@"path"]];
        }
        
        return cell;
    }
    else
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"textCell"];        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(self.questionTypeNumber == 0){
        NSLog(@"Open question: answer text");
        return 50;
    }else if(self.questionTypeNumber == 1){
        NSLog(@"True / False");
        return 100;
    }else if(self.questionTypeNumber == 2){
        NSLog(@"Single choice");
        return 50;
    }else if(self.questionTypeNumber == 3){
        NSLog(@"Multiple choice");
        return 50;
    }else if(self.questionTypeNumber == 4){
        NSLog(@"Single image choice");
        return 150;
    }else if(self.questionTypeNumber == 5){
        NSLog(@"Multile image choice");
        return 150;
    }else{
        return 0;
    }
}

- (IBAction)prevButtonTap:(id)sender {
    
    [self.nextBtn setTitle:@"Next Question" forState:UIControlStateNormal];
    [self enableNext];
    [self.answerArray removeObjectAtIndex: _questionNumber - 1];
    _questionNumber -= 1;
    self.question = self.questions[_questionNumber];
    self.questionTypeNumber = [self.question[@"questionType"] integerValue];
    self.questionDescription.text = self.question[@"description"];
    self.questionOrder.text = [NSString stringWithFormat:@"Question %li out of %li", _questionNumber + 1, self.questions.count];
    [self.questionTable reloadData];
    if(_questionNumber == 0){
        self.prevBtn.hidden = YES;
    }
}

- (IBAction)nextButtonTap:(id)sender {
    [self disableNext];
    NSDictionary *answerJson;
    
    //create answer json data.
    if(self.questionTypeNumber == 0){
        NSLog(@"Open question: answer text");
        answerJson = @{
                                     @"evaluationQuestionId": self.evaluationQuestionId,
                                     @"answerText": self.answerString
                                     };
    }else if(self.questionTypeNumber == 1){
        NSLog(@"True / False");
        answerJson = @{
                       @"evaluationQuestionId": self.evaluationQuestionId,
                       @"answerText": self.answerString
                       };
    }else if(self.questionTypeNumber == 2){
        NSLog(@"Single choice");
        answerJson = @{
                       @"evaluationQuestionId": self.evaluationQuestionId,
                       @"selectedOptions": self.selectedOptions
                       };
        
    }else if(self.questionTypeNumber == 3){
        NSLog(@"Multiple choice");
        answerJson = @{
                       @"evaluationQuestionId": self.evaluationQuestionId,
                       @"selectedOptions": self.selectedOptions
                       };
        
    }else if(self.questionTypeNumber == 4){
        NSLog(@"Single image choice");
        answerJson = @{
                       @"evaluationQuestionId": self.evaluationQuestionId,
                       @"answerText": self.answerString
                       };
    }else if(self.questionTypeNumber == 5){
        NSLog(@"Multile image choice");
        answerJson = @{
                       @"evaluationQuestionId": self.evaluationQuestionId,
                       @"selectedOptions": self.selectedOptions
                       };
    }else{
        
    }
    
    [self.answerArray addObject:answerJson];
    
    if(self.questions.count - 1 > _questionNumber)
    {
        [self.nextBtn setTitle:@"Next Question" forState:UIControlStateNormal];
        _questionNumber += 1;
        self.prevBtn.hidden = NO;
        self.question = self.questions[_questionNumber];
        self.questionTypeNumber = [self.question[@"questionType"] integerValue];
        self.questionDescription.text = self.question[@"description"];
        self.evaluationQuestionId = self.question[@"id"];
        self.questionOrder.text = [NSString stringWithFormat:@"Question %li out of %li", _questionNumber + 1, self.questions.count];
        [self.questionTable reloadData];
        NSLog(@"%li", _questionNumber);
        if(self.questions.count - 1 == _questionNumber){
            [self.nextBtn setTitle:@"Submit" forState:UIControlStateNormal];
        }
    }else if(self.questions.count - 1 == _questionNumber){
        //submit with answer Array.
        NSDictionary *dataToSend = @{
                                     @"id": self.evaluation[@"id"],
                                     @"status": self.evaluation[@"status"],
                                     @"answers": self.answerArray,
                                     @"userId": [[UserCache sharedInstance] getUserId],
                                     @"evaluationId": self.evaluation[@"contentId"]
                                     };
        [self showLoader];
        [NetworkHelper placePostRequest:dataToSend relativeUrl:@"services/app/UserEvaluation/Submit" withSuccess:^(NSDictionary *jsonObject) {
            [self hideLoader];
            [self showErrorMessage:@"Successfully Submitted Evaluation"];
            [self.navigationController popViewControllerAnimated:YES];
            
        } andFailure:^(NSString *string) {
            [self hideLoader];
            NSLog(@"loginFailure:");
            [self showErrorMessage:string];
        }];
//        [self disableNext];
    }
    else
    {
        [self.nextBtn setTitle:@"Submit" forState:UIControlStateNormal];
//        [self disableNext];
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
}

- (void)disableNext {
    self.nextBtn.userInteractionEnabled = NO;
    self.nextBtn.backgroundColor = [UIColor colorWithRed:0.000 green:0.737 blue:0.941 alpha:0.24];
}

- (void)enableNext {
    self.nextBtn.userInteractionEnabled = YES;
    self.nextBtn.backgroundColor = [UIColor colorWithRed:0.000 green:0.737 blue:0.941 alpha:1.00];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showLoader {
    if (!dimView) {
        dimView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
        dimView.backgroundColor = [UIColor blackColor];
        dimView.alpha = 0.7f;
        dimView.userInteractionEnabled = NO;
    }
    [self.view addSubview:dimView];
    CGRect newFrame = dimView.frame;
    
    newFrame.size.width = 1200;
    newFrame.size.height = 1500;
    [dimView setFrame:newFrame];
    [dimView setCenter:CGPointMake(187.0f, 333.0f)];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeLineScale tintColor:[UIColor whiteColor] size:40.0f];
    activityIndicatorView.center = self.view.center;
    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
}

- (void)hideLoader {
    [dimView removeFromSuperview];
    [activityIndicatorView stopAnimating];
}

- (void)showErrorMessage:(NSString *)string {
    if (!string) {
        string = @"";
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Character.ly"
                                                    message:string
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)updateAnswerString: (UITextField *)sender {
    [self enableNext];
    self.answerString = sender.text;
}

//textfield delegate method.
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    [self enableNext];
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
//    [self updateAnswerString: newString];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
}

@end
