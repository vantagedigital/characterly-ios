//
//  EvaluationCheckViewController.h
//  Character.ly
//
//  Created by dev on 2/22/18.
//  Copyright © 2018 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BEMCheckBox.h"

@interface EvaluationCheckViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>


@property (strong, nonatomic) NSDictionary *evaluation;
@property (strong, nonatomic) NSArray *questions;
@property (weak, nonatomic) IBOutlet UITableView *questionTable;
@property (weak, nonatomic) IBOutlet UITableView *classroomTable;
@property (weak, nonatomic) IBOutlet UILabel *evaluationName;
@property (weak, nonatomic) IBOutlet UILabel *evaluationTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *questionTableHeight;
@property (weak, nonatomic) IBOutlet UIButton *assignButton;
@property (weak, nonatomic) IBOutlet UIButton *postButton;
@property (weak, nonatomic) IBOutlet UIView *classroomContainer;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end
