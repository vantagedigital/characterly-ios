//
//  ViewController.m
//  Character.ly
//
//  Created by Adeel Nasir on 08/08/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "ViewController.h"
#import "NavViewController.h"
#import "WebAPIs.h"
#import "MessageWindow.h"
#import "DGActivityIndicatorView.h"

@interface ViewController (){
    UIView *dimView;
    DGActivityIndicatorView *activityIndicatorView;
}
@end

@implementation ViewController

#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    
//    self.txtUsername.text = @"student@gmail.com";
//    self.txtPassword.text = @"111111";
    
    if ([[UserCache sharedInstance] isLoggedIn]) {
        [self performSegueWithIdentifier:@"hs" sender:self];
    }
    
    checked1 = NO;
    // Do any additional setup after loading the view, typically from a nib.
    [self.lblCreateaccount setUserInteractionEnabled:YES];
    _lblCreateaccount.userInteractionEnabled = YES;
    
    
    
    [self.lblForgot setUserInteractionEnabled:YES];
    _lblForgot.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tapGesture = \
    [[UITapGestureRecognizer alloc]
     initWithTarget:self action:@selector(didTapLabelWithGesture:)];
    [_lblCreateaccount addGestureRecognizer:tapGesture];
    
    UITapGestureRecognizer *tapGesture2 = \
    [[UITapGestureRecognizer alloc]
     initWithTarget:self action:@selector(didTapLabelWithGesture2:)];
    [_lblForgot addGestureRecognizer:tapGesture2];
    
    [self.view setBackgroundColor: RGB(247, 248, 250)];
    [self.btnLogin setBackgroundColor:RGB(0, 188, 240)];
    [self.txtUsername setBackgroundColor:RGB(251, 251, 251)];
    [self.txtPassword setBackgroundColor:RGB(251, 251, 251)];
    [self.lblForgot setTextColor:RGB(107, 124, 147)];
    [self.lblCreateaccount setTextColor:RGB(107, 124, 147)];
    [self.lblUser setTextColor:RGB(107, 124, 147)];
    [self.lblPassword setTextColor:RGB(107, 124, 147)];
    
    
    _btnForgot.layer.borderColor = [UIColor whiteColor].CGColor;
    _btnForgot.layer.borderWidth = 1.0f;
    
    _btnCreate.layer.borderColor = [UIColor whiteColor].CGColor;
    _btnCreate.layer.borderWidth = 1.0f;
    
    
    
    
    UIView *padView = [[UIView alloc] initWithFrame:CGRectMake(10, 110, 10, 0)];
    _txtUsername.leftView = padView;
    _txtUsername.leftViewMode = UITextFieldViewModeAlways;
    
    
    UIView *padView1 = [[UIView alloc] initWithFrame:CGRectMake(10, 110, 10, 0)];
    _txtPassword.leftView = padView1;
    _txtPassword.leftViewMode = UITextFieldViewModeAlways;
    
    
    _txtUsername.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtUsername.layer.borderWidth = 1.0f;
    
    _txtPassword.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtPassword.layer.borderWidth = 1.0f;
    
    self.checkRemember.on = NO;
    self.checkRemember.boxType = BEMBoxTypeSquare;
    self.checkRemember.onAnimationType = BEMAnimationTypeBounce;
    self.checkRemember.offAnimationType = BEMAnimationTypeBounce;
    self.checkRemember.lineWidth = 2;
    self.checkRemember.tintColor = [self colorWithHexString:@"00bcf0"];
    self.checkRemember.onTintColor = [UIColor whiteColor];
    self.checkRemember.onFillColor = [self colorWithHexString:@"00bcf0"];
    self.checkRemember.onCheckColor = [UIColor whiteColor];
    
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [_txtUsername resignFirstResponder];
    [_txtPassword resignFirstResponder];
    return false;
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

- (void)didTapLabelWithGesture:(UITapGestureRecognizer *)tapGesture {
    [self performSegueWithIdentifier:@"ca" sender:self];
}

- (void)didTapLabelWithGesture2:(UITapGestureRecognizer *)tapGesture2 {
    [self performSegueWithIdentifier:@"fp" sender:self];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"ca"])
    {
//        // Get reference to the destination view controller
//        YourViewController *vc = [segue destinationViewController];
//        
//        // Pass any objects to the view controller here, like...
//        [vc setMyObjectHere:object];
    }
    if ([[segue identifier] isEqualToString:@"fp"])
    {

    }

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
}


- (IBAction)Login:(id)sender {
    
    dimView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
    dimView.backgroundColor = [UIColor blackColor];
    dimView.alpha = 0.7f;
    dimView.userInteractionEnabled = NO;
    [self.view addSubview:dimView];
    CGRect newFrame = dimView.frame;
    
    newFrame.size.width = 1200;
    newFrame.size.height = 1500;
    [dimView setFrame:newFrame];
    [dimView setCenter:CGPointMake(187.0f, 333.0f)];
    
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeLineScale tintColor:[UIColor whiteColor] size:40.0f];
    activityIndicatorView.center = self.view.center;
    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    
    // TODO: update remember user field
    if(!checked1){
        [self login:_txtUsername.text :_txtPassword.text :@"false"];
    }else{
        [self login:_txtUsername.text :_txtPassword.text :@"true"];
    }
}

- (void)showErrorMessage:(NSString *)messageText {
    
    [dimView removeFromSuperview];
    [activityIndicatorView stopAnimating];
    
    MessageWindow *message = [[MessageWindow alloc] init];
    
    //  [myViewController2.view addSubView:mySubView];
    // add any other views to myViewController2's view
    [message removeFromSuperview];
    
    [self.view addSubview:message];
    
    [message setMessage:messageText];
    
    [message setAlpha:1.0f];
    
    //fade in
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionTransitionFlipFromTop
                     animations:^{
                         
                         [message setAlpha:1.0f];
                         message.frame = CGRectMake(_leadingFromSide.constant + _mainView.frame.origin.x, _heightFromTop.constant + _mainView.frame.origin.y + _scrollView.frame.origin.y, _txtUsername.frame.size.width, _txtUsername.frame.size.height);
                         
                     } completion:^(BOOL finished) {
                         
                         //fade out
                         [UIView animateWithDuration:0.3
                                               delay:2.0
                                             options:UIViewAnimationOptionTransitionCurlUp
                                          animations:^{
                                              
                                              [message setAlpha:0.0f];
                                              //                                 message.frame = CGRectMake(30.0, 140.0, 350, 0);
                                              //
                                              
                                              
                                          } completion:^(BOOL finished) {
                                              [message setMessage:@""];
                                          }];
                         
                     }];
}

-(void) returnTextColor {
    
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds *NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        _txtUsername.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
        _txtUsername.backgroundColor = [self colorWithHexString:@"f7f8fa"];
        _txtUsername.textColor = [self colorWithHexString:@"6b7c93"];
        _txtPassword.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
        _txtPassword.backgroundColor = [self colorWithHexString:@"f7f8fa"];
        _txtPassword.textColor = [self colorWithHexString:@"6b7c93"];
    });
}

-(void) displayLoginError:(UITextField *)errorTextField {
    errorTextField.layer.borderColor = [self colorWithHexString:@"e82a16"].CGColor;
    errorTextField.backgroundColor = [self colorWithHexString:@"ffc0ba"];
    errorTextField.textColor = [self colorWithHexString:@"e82a16"];
}

-(void)login:(NSString*)username :(NSString*)password :(NSString*)remember {
    if([username isEqualToString:@""]){
        
        [self showErrorMessage:@"Please fill username field"];
        [self displayLoginError:_txtUsername];
        [self returnTextColor];
        
    } else if([password isEqualToString:@""]){
        
        [self showErrorMessage:@"Please fill password field"];
        [self displayLoginError:_txtPassword];
        [self returnTextColor];
        
    } else {
        NSDictionary *dataToSend = @{
                                     @"userNameOrEmailAddress" : username,
                                     @"password" : password,
                                     @"rememberClient" : remember};
        
        [NetworkHelper placePostRequest:dataToSend relativeUrl:@"TokenAuth/Authenticate" withSuccess:^(NSDictionary *jsonObject) {
            // cache data
            [[UserCache sharedInstance] setUserId:jsonObject[@"result"][@"userId"]];
            [[UserCache sharedInstance] setAccessToken:jsonObject[@"result"][@"accessToken"]];
            [[UserCache sharedInstance] setEncryptedAccessToken:jsonObject[@"result"][@"encryptedAccessToken"]];
            
            [self getUserInfo];
        } andFailure:^(NSString *string) {
            NSLog(@"loginFailure:");
            [self showErrorMessage:string];
            [self displayLoginError:_txtUsername];
            [self displayLoginError:_txtPassword];
            [self returnTextColor];
        }];
    }
}

- (void)getUserInfo {
    [NetworkHelper placeGetRequest:nil relativeUrl:@"services/app/Session/GetCurrentLoginInformations" withSuccess:^(NSDictionary *jsonObject) {
        [[UserCache sharedInstance] setCurrentUser:jsonObject[@"result"][@"user"]];
        [self getContentInfo];
    } andFailure:^(NSString *string) {
        NSLog(@"getUserInfo Failure:%@", string);
        //               [self showErrorMessage:string];
    }];
}

- (void)getContentInfo {
    NSString *contentURL = @"services/app/UserContent/GetMyContent?SkipCount=0&MaxResultCount=100";
//    NSString *contentURL = @"services/app/UserContent/GetMyContent";
    [NetworkHelper placeGetRequest:nil relativeUrl: contentURL withSuccess:^(NSDictionary *jsonObject) {
        [[UserCache sharedInstance] setContentInfo:jsonObject];
        NSLog(@"ContentInfo: %@", jsonObject);
        [self goToHome];
    } andFailure:^(NSString *string) {
        NSLog(@"getContentInfo Failure:%@", string);
        [self showErrorMessage:string];
    }];
}

- (IBAction)actionBack:(id)sender {
      [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)checkRemember:(BEMCheckBox *)sender {
    
    if(!checked1){
        checked1 = YES;
    }else{
        checked1 = NO;
    }
}

- (IBAction)usernameDismiss:(id)sender {
    [_txtUsername endEditing:YES];
    [_txtUsername resignFirstResponder];
    [_txtPassword resignFirstResponder];
}

- (IBAction)passwordDissmiss:(id)sender {
    [_txtPassword endEditing:YES];
}

- (void)goToHome {
    [dimView removeFromSuperview];
    [activityIndicatorView stopAnimating];
    
    [self performSegueWithIdentifier:@"hs" sender:self];
}
@end
