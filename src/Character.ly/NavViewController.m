//
//  NavViewController.m
//  Character.ly
//
//  Created by Macbook Pro on 14/09/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "NavViewController.h"
#import "UIView+AutoLayout.h"

@interface NavViewController () {
    
    UIView *parentView;
    UIButton *backButton;
    
    
    UIButton *menu;
    UIButton *closeMenu;
    UIButton *search;
    
}
@property (weak, nonatomic) IBOutlet UILabel *notificationsLabel;
@property (weak, nonatomic) IBOutlet UILabel *notificationCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *profileLabel;
@property (weak, nonatomic) IBOutlet UILabel *signOutLabel;
@property (weak, nonatomic) IBOutlet UILabel *classroomsLabel;
@property (weak, nonatomic) IBOutlet UILabel *lessonsLabel;
@property (weak, nonatomic) IBOutlet UILabel *evaluationsLabel;
@property (weak, nonatomic) IBOutlet UIButton *profileIconButton;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UIView *rightSelectView;
@property (weak, nonatomic) IBOutlet UIView *leftSelectView;
@property (weak, nonatomic) IBOutlet UIView *menuView;
@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UITextField *searchText;

@property (strong, nonatomic)  NSString *currentImagePath;
@property (assign, nonatomic) BOOL isRightSelectView;
@property (assign, nonatomic) BOOL isLeftSelectView;
@property (assign, nonatomic) BOOL isSearchView;

@end

@implementation NavViewController


- (id)initWithFrame:(CGRect)aRect
{
    if ((self = [super initWithFrame:aRect])) {
        NSLog(@"Frame %@", NSStringFromCGRect(self.frame));
        [[NSBundle mainBundle] loadNibNamed:@"NavigationB" owner:self options:nil];
        self.bounds = self.viewA.bounds;
        [self addSubview:self.viewA];
        
        self.viewA.translatesAutoresizingMaskIntoConstraints = NO;
        [self.viewA autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:0];
        [self.viewA autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
        [self.viewA autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
        [self.viewA autoPinEdgeToSuperviewEdge:ALEdgeBottom withInset:0];
        
        self.isRightSelectView = NO;
        self.isLeftSelectView = NO;
        
        if(self.isLeftSelectView == NO){
            UIImage *hamImage = [UIImage imageNamed: @"ico-ham-nav.png"];
            [_menuButton setImage:hamImage forState:UIControlStateNormal];
        }else{
            UIImage *hamImage = [UIImage imageNamed: @"icon-close-white.png"];
            [_menuButton setImage:hamImage forState:UIControlStateNormal];
        }
        
        self.isSearchView = NO;
        
        backButton = (UIButton *)[self viewWithTag:100];
        UIButton *accountBtn = (UIButton *)[self viewWithTag:200];
    
        self.rightSelectView.hidden = YES;
        self.leftSelectView.hidden = YES;
        self.searchView.hidden = YES;
        self.notificationCountLabel.hidden = YES;
        
        parentView = (UIView *)[self viewWithTag:300];
        
        
        [backButton addTarget:self action:@selector(myEventHandler) forControlEvents: UIControlEventTouchUpInside];

        [accountBtn addTarget:self action:@selector(openAccount) forControlEvents: UIControlEventTouchUpInside];
        
        UITapGestureRecognizer* signOutGs = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(signOutTap)];
        
        [self.signOutLabel setUserInteractionEnabled:YES];
        [self.signOutLabel addGestureRecognizer:signOutGs];
        
        UITapGestureRecognizer* accountGs = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(accountTap)];
        
        [self.profileLabel setUserInteractionEnabled:YES];
        [self.profileLabel addGestureRecognizer:accountGs];
        
        UITapGestureRecognizer* notificationsGs = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(notificationsTap)];
        
        [self.notificationsLabel setUserInteractionEnabled:YES];
        [self.notificationsLabel addGestureRecognizer:notificationsGs];
        
        UITapGestureRecognizer *classroomGs = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(classroomsTap)];
        
        [self.classroomsLabel setUserInteractionEnabled:YES];
        [self.classroomsLabel addGestureRecognizer:classroomGs];
        
        UITapGestureRecognizer *evaluationsGs = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(evaluationsTap)];
        
        [self.evaluationsLabel setUserInteractionEnabled:YES];
        [self.evaluationsLabel addGestureRecognizer:evaluationsGs];
        
        UITapGestureRecognizer *lessonsGs = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lessonsTap)];
        
        [self.lessonsLabel setUserInteractionEnabled:YES];
        [self.lessonsLabel addGestureRecognizer:lessonsGs];
        
        [self updatePic];
    }
    
    [self performSelector:@selector(openAccount) withObject:nil afterDelay:0.2];
    [self performSelector:@selector(openAccount) withObject:nil afterDelay:0.3];
    
    return self;
}

- (void)updatePic {
    @try {
        if (IsEmpty([[UserCache sharedInstance] getUserInfo][@"imagePath"])) {
            [NetworkHelper placeGetRequest:nil relativeUrl:concat(@"services/app/FileAttachment/GetByEntityId?id=", [[UserCache sharedInstance] getUserInfo][@"id"]) withSuccess:^(NSDictionary *model) {
                @try {
                    NSArray *arr = model[@"result"];
                    NSString *path = arr[arr.count - 1][@"path"];
                    
                    NSMutableDictionary *dic = [[UserCache sharedInstance] getUserInfo].mutableCopy;
                    [dic setObject:path forKey:@"imagePath"];
                    [[UserCache sharedInstance] setCurrentUser:dic];
                    
                    self.currentImagePath = [[UserCache sharedInstance] getUserInfo][@"imagePath"];
                    [self setProfilePic];
                    [self checkCurrentImage];
                } @catch(NSException *e) {
                    NSLog(@"error saving profile image");
                }
                
            } andFailure:^(NSString *string) {
                NSLog(@"getUserInfo Failure:%@", string);
                //               [self showErrorMessage:string];
            }];
        } else {
            self.currentImagePath = [[UserCache sharedInstance] getUserInfo][@"imagePath"];
            [self setProfilePic];
            [self checkCurrentImage];
        }
    } @catch (NSException *e) {
        [self checkCurrentImage];
    }
}
- (IBAction)click:(id)sender {
    
}

- (void)setProfilePic {
    
    [self getUserProfileImage:self.currentImagePath imageView:self.profileIconButton];
    
    self.profileIconButton.layer.masksToBounds = true;
    self.profileIconButton.layer.cornerRadius = self.profileIconButton.frame.size.width / 2;
}

- (void)getUserProfileImage:(NSString *)imagePath imageView:(UIButton *)profileButton {
    
    if(imagePath){
        
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:imagePath]];
            if(data == nil)
                return;
            dispatch_async(dispatch_get_main_queue(), ^{
                [profileButton setImage:[UIImage imageWithData:data] forState:normal];
            });
        });
    }
}

- (void)checkCurrentImage {
    @try {
        if (IsEmpty([[UserCache sharedInstance] getUserInfo][@"imagePath"]) && ![self.currentImagePath isEqualToString:[[UserCache sharedInstance] getUserInfo][@"imagePath"]]) {
            self.currentImagePath = [[UserCache sharedInstance] getUserInfo][@"imagePath"];
            [self setProfilePic];
        }
    } @catch (NSException *e) {
        
    }
    [self performSelector:@selector(checkCurrentImage) withObject:nil afterDelay:1.0f];
}

- (void)lessonsTap {
    @try {
        UIViewController *controller = [self.parent.storyboard instantiateViewControllerWithIdentifier:@"Home"];
        [self.parent.navigationController pushViewController: controller animated: YES];
    } @catch (NSException *exception) {
        NSLog(@"exception with classroomsTap caught");
    }
}

- (void)classroomsTap {
    @try {
        UIViewController *controller = [self.parent.storyboard instantiateViewControllerWithIdentifier:@"Classroom"];
        [self.parent.navigationController pushViewController: controller animated: YES];
    } @catch (NSException *exception) {
        NSLog(@"exception with classroomsTap caught");
    }
}

- (void)evaluationsTap {
    @try {
        UIViewController *controller = [self.parent.storyboard instantiateViewControllerWithIdentifier:@"Evaluation"];
        [self.parent.navigationController pushViewController: controller animated: YES];
    } @catch (NSException *exception) {
        NSLog(@"exception with evaluationsTap caught");
    }
}

- (void)accountTap {
    [UserCache sharedInstance].isNotifications = NO;
    [self goToMyAccount];
}

- (void)goToMyAccount {
    if ([[[UserCache sharedInstance] getUserInfo][@"educator"] isKindOfClass:[NSNull class]]) {
        UIViewController *controller = [self.parent.storyboard instantiateViewControllerWithIdentifier:@"StudentProfile"];
        [self.parent.navigationController pushViewController: controller animated: YES];
    }else{
        UIViewController *controller = [self.parent.storyboard instantiateViewControllerWithIdentifier:@"EducatorProfile"];
        [self.parent.navigationController pushViewController: controller animated: YES];
    }
}

- (void)notificationsTap {
    [UserCache sharedInstance].isNotifications = YES;
    [self goToMyAccount];
}


- (void)signOutTap {
    
    [[UserCache sharedInstance] clear];
    UINavigationController *navController = (UINavigationController *)[[[UIApplication sharedApplication] keyWindow] rootViewController];
    [navController popToRootViewControllerAnimated:YES];
}

- (id)initWithCoder:(NSCoder*)coder
{
    if ((self = [super initWithCoder:coder])) {
       
    }
    return self;
}

-(void) myEventHandler {
    NSLog(@"back");
 //  [[UIApplication sharedApplication].n popViewControllerAnimated:YES];
    UINavigationController *navController = (UINavigationController *)[[[UIApplication sharedApplication] keyWindow] rootViewController];
    [navController popViewControllerAnimated:YES];
}

- (IBAction)openMenu:(id)sender {
    NSLog(@"open menu adeel");
    self.isRightSelectView = NO;
    self.isSearchView = NO;
    self.searchView.hidden = YES;
    self.rightSelectView.hidden = YES;
    
    if(!self.isLeftSelectView){
        
        [parentView setFrame:CGRectMake(parentView.frame.origin.x, parentView.frame.origin.y, parentView.frame.size.width, 200)];
        self.isLeftSelectView = YES;
        self.leftSelectView.hidden = NO;
        

    }
    else if(self.isLeftSelectView){
        
        self.leftSelectView.hidden = YES;

        [parentView setFrame:CGRectMake(parentView.frame.origin.x, parentView.frame.origin.y, parentView.frame.size.width, 100)];
        self.isLeftSelectView = NO;
    }
    
    if(self.isLeftSelectView == NO){
        UIImage *hamImage = [UIImage imageNamed: @"ico-ham-nav.png"];
        [_menuButton setImage:hamImage forState:UIControlStateNormal];
    }else{
        UIImage *hamImage = [UIImage imageNamed: @"icon-close-white.png"];
        [_menuButton setImage:hamImage forState:UIControlStateNormal];
    }
}

- (IBAction)openSearch:(id)sender {
    NSLog(@"Search");
    self.isRightSelectView = NO;
    self.isLeftSelectView = NO;
    self.rightSelectView.hidden = YES;
    self.leftSelectView.hidden = YES;
    
    if(!self.isSearchView){
        
        [parentView setFrame:CGRectMake(parentView.frame.origin.x, parentView.frame.origin.y, parentView.frame.size.width, 150)];
        self.isSearchView = YES;
        self.searchView.hidden = NO;
    }
    else if(self.isSearchView){
        
        self.searchView.hidden = YES;
        [parentView setFrame:CGRectMake(parentView.frame.origin.x, parentView.frame.origin.y, parentView.frame.size.width, 100)];
        self.isSearchView = NO;
    }
}



-(void) openAccount {
    
    self.isSearchView = NO;
    self.isLeftSelectView = NO;
    self.leftSelectView.hidden = YES;
    self.searchView.hidden = YES;
    
    if(!self.isRightSelectView){
        
        self.rightSelectView.hidden = NO;
        
        [parentView setFrame:CGRectMake(parentView.frame.origin.x, parentView.frame.origin.y, parentView.frame.size.width, 200)];
        self.isRightSelectView= YES;
    }
    else if(self.isRightSelectView){
        
        self.rightSelectView.hidden = YES;
        [parentView setFrame:CGRectMake(parentView.frame.origin.x, parentView.frame.origin.y, parentView.frame.size.width, 100)];
        self.isRightSelectView = NO;
    }
}

- (void)hideBackButton {
    backButton.hidden = YES;
}


@end

