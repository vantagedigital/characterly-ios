//
//  EvaluationCheckViewController.m
//  Character.ly
//
//  Created by dev on 2/22/18.
//  Copyright © 2018 Adeel Nasir. All rights reserved.
//

#import "EvaluationCheckViewController.h"
#import "NavViewController.h"
#import "DGActivityIndicatorView.h"
#import "EvaluationQuestionTableViewCell.h"

@interface EvaluationCheckViewController ()<UITextFieldDelegate>{
    UIView *dimView;
    DGActivityIndicatorView *activityIndicatorView;
}

@property (assign, nonatomic) BOOL isEducator;
@property (strong, nonatomic) NSArray *classroomData;

@end

@implementation EvaluationCheckViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.hidden = YES;
    
    NavViewController *myViewController2 = [[NavViewController alloc] initForAutoLayout];
    [self.view addSubview:myViewController2];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:[UIApplication sharedApplication].statusBarFrame.size.height];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
    [myViewController2 autoSetDimension:ALDimensionHeight toSize:200];
    myViewController2.parent = self;
    
    self.postButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.postButton.layer.borderWidth = 0.5;
    
    NSLog(@"Evaluation: %@", self.evaluation);
    
    self.evaluationTitle.text = self.evaluation[@"name"];
    self.evaluationTitle.adjustsFontSizeToFitWidth = YES;
    NSString *evaluationRootName = @"Evaluations / ";
    NSString *evaluationNameString = [evaluationRootName stringByAppendingString:self.evaluation[@"name"]];
    self.evaluationName.text = evaluationNameString;
    self.evaluationName.adjustsFontSizeToFitWidth = YES;
    
    self.questionTable.delegate = self;
    self.questionTable.dataSource = self;
    
    self.questions = self.evaluation[@"questions"];
   
    self.classroomContainer.hidden = YES;
    self.classroomTable.delegate = self;
    self.classroomTable.dataSource = self;
    
    self.classroomData = [[UserCache sharedInstance] getUserInfo][@"educator"][@"classrooms"];
    
    NSLog(@"classroom data: %@", self.classroomData);
}

- (void)showLoader {
    if (!dimView) {
        dimView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
        dimView.backgroundColor = [UIColor blackColor];
        dimView.alpha = 0.7f;
        dimView.userInteractionEnabled = NO;
    }
    [self.view addSubview:dimView];
    CGRect newFrame = dimView.frame;
    
    newFrame.size.width = 1200;
    newFrame.size.height = 1500;
    [dimView setFrame:newFrame];
    [dimView setCenter:CGPointMake(187.0f, 333.0f)];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeLineScale tintColor:[UIColor whiteColor] size:40.0f];
    activityIndicatorView.center = self.view.center;
    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
}

- (void)hideLoader {
    [dimView removeFromSuperview];
    [activityIndicatorView stopAnimating];
}


- (IBAction)actionAssign:(id)sender {
    
    self.classroomContainer.hidden = NO;
    [self showAnimate];
}

- (IBAction)actionPost:(id)sender {
    
    
}

- (IBAction)closeContainer:(id)sender {
    
    self.classroomContainer.hidden = YES;
    [self removeAnimate];
}

- (void) showAnimate {
    self.classroomContainer.transform = CGAffineTransformMakeScale(1.0, 1.0);
    self.classroomContainer.alpha = 0.0;
    [UIView animateWithDuration:0.25 animations:^{
        self.classroomContainer.alpha = 1.0;
        self.classroomContainer.transform = CGAffineTransformMakeScale(1.0, 1.0);
    }];
}

- (void) removeAnimate {
    [UIView animateWithDuration:0.25 animations:^{
        self.classroomContainer.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.classroomContainer.alpha = 0.0;
    } completion: ^(BOOL finished) {
        if(finished){
            self.classroomContainer.hidden = YES;
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(tableView == _questionTable){
        return self.questions.count;
    }else if(tableView == _classroomTable){
        return self.classroomData.count;
    }else{
        return 0;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView == _classroomTable) {
        //assign evaluation to selected classroom.
        NSLog(@" token: %@", [[UserCache sharedInstance] getAccessToken]);
        
        NSString *evaluationParam = [@"evaluationId=" stringByAppendingString:self.evaluation[@"id"]];
        NSString *classroomParam = [@"classroomId=" stringByAppendingString:self.classroomData[indexPath.row][@"id"]];
        NSString *url = [NSString stringWithFormat:@"services/app/Evaluation/AssignToClassroom?%@&%@", evaluationParam, classroomParam];
        NSLog(@"%@", url);
        [self showLoader];
        [NetworkHelper placePostRequest:nil relativeUrl:url withSuccess:^(NSDictionary *jsonObject) {
            [self hideLoader];
            self.classroomContainer.hidden = YES;
            [self removeAnimate];
            [self showErrorMessage:@"Successfully Assigned"];
        } andFailure:^(NSString *string) {
            [self hideLoader];
            NSLog(@"Failure: %@", string);
            self.classroomContainer.hidden = YES;
            [self removeAnimate];
            [self showErrorMessage:string];
        }];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView == _questionTable)
    {
        EvaluationQuestionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"questionCell"];
        
        if(cell == nil){
            NSArray *cellObjects = [[NSBundle mainBundle] loadNibNamed:@"EvaluationQuestionTableViewCell" owner:self options:nil];
            cell = [cellObjects objectAtIndex:0];
        }
        
        NSDictionary *options = self.questions[indexPath.row];
        NSString *questionNumber = [NSString stringWithFormat:@"%ld", (long)(indexPath.row + 1)];
        NSString *questionContent = [NSString stringWithFormat:@" - %@", options[@"description"]];
        NSString *questionString = [questionNumber stringByAppendingString:questionContent];
        cell.questionDescription.text = questionString;
        cell.questionContent = options;
        
        return cell;
    }else if(tableView == _classroomTable){
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"classroom"];
        
        UIImageView *imageView = (UIImageView *)[cell viewWithTag:10];
        UILabel *classCode = (UILabel *)[cell viewWithTag:11];
        UILabel *classTitle = (UILabel *)[cell viewWithTag:12];
        UILabel *gradeName = (UILabel *)[cell viewWithTag:13];
        UILabel *studentsCount = (UILabel *)[cell viewWithTag:14];
        
        classCode.text = self.classroomData[indexPath.row][@"classCode"];
        NSLog(@"classroom data: %@ : %@ : %@", self.classroomData[indexPath.row][@"classCode"], self.classroomData[indexPath.row][@"name"], self.classroomData[indexPath.row][@"grade"][@"name"]);
        classTitle.text = self.classroomData[indexPath.row][@"name"];
        NSNull *null = [[NSNull alloc] init];
        if(self.classroomData[indexPath.row][@"grade"] == null){
            
        }else{
            gradeName.text = self.classroomData[indexPath.row][@"grade"][@"name"];
        }
        studentsCount.text = @"Students:";
        
        // Configure the cell...
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"classroom"];
        }
        
        return cell;
    }else{
        return nil;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView == _questionTable)
    {
        NSDictionary *options = self.questions[indexPath.row];
        NSArray *questionOptions = options[@"questionOptions"];
        self.questionTableHeight.constant += 100 + questionOptions.count * 70;
        
        return 100 + questionOptions.count * 70;
        
    }else if(tableView == _classroomTable){
        return 200;
    }else{
        return 0;
    }
        
}

- (void)showErrorMessage:(NSString *)string {
    if (!string) {
        string = @"";
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Character.ly"
                                                    message:string
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
