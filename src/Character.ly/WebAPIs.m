//
//  WebAPIs.m
//  Character.ly
//
//  Created by Adeel Nasir on 08/10/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "WebAPIs.h"
@interface WebAPIs() {
    NSMutableArray* loginData;
}
@end
@implementation WebAPIs

-(NSMutableArray*)Login:(NSString*)username :(NSString*)password :(NSString*)remember{
    loginData = NULL;
    loginData = [[NSMutableArray alloc]init];
    NSDictionary *dataToSend = [NSDictionary dictionaryWithObjectsAndKeys:
                                username, @"userNameOrEmailAddress",
                                password, @"password",
                                remember, @"rememberClient",
                                nil];
    [self login:dataToSend
       calledBy:self
    withSuccess:@selector(loginDidEnd:)
     andFailure:@selector(loginFailure:)];
    return loginData;
    
}
-(void)placePostRequestWithURL:(NSString *)action withData:(NSDictionary *)dataToSend withHandler:(void (^)(NSURLResponse *response, NSData *data, NSError *error))ourBlock {
    NSString *urlString = [NSString stringWithFormat:@"%@", action];
    NSLog(@"%@", urlString);
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSError *error;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataToSend options:0 error:&error];
    
    NSString *jsonString;
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        NSData *requestData = [NSData dataWithBytes:[jsonString UTF8String] length:[jsonString lengthOfBytesUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: requestData];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:ourBlock];
    }
}

- (void) login:(NSDictionary *)data
      calledBy:(id)calledBy
   withSuccess:(SEL)successCallback
    andFailure:(SEL)failureCallback{
    [self placePostRequestWithURL:@"http://13.92.193.213:80/api/TokenAuth/Authenticate"
                         withData:data
                      withHandler:^(NSURLResponse *response, NSData *rawData, NSError *error) {
                          NSString *string = [[NSString alloc] initWithData:rawData
                                                                   encoding:NSUTF8StringEncoding];
                          
                          NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
                          NSInteger code = [httpResponse statusCode];
                          NSLog(@"%ld", (long)code);
                          
                          if (!(code >= 200 && code < 300)) {
                              NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:[string dataUsingEncoding:NSUTF8StringEncoding]
                                                                                           options:0 error:NULL];
                              NSMutableArray *getdata=[[NSMutableArray alloc]init];
                              getdata=[jsonObject objectForKey:@"error"];
                              
                              NSLog(@"output %@ ",[getdata valueForKey:@"details"] );
                              [loginData addObject:[getdata valueForKey:@"details"]];
                              [calledBy performSelector:failureCallback withObject:string];
                          } else {
                              NSLog(@"OK");
                              
                              NSDictionary *result = [NSDictionary dictionaryWithObjectsAndKeys:
                                                      string, @"id",
                                                      nil];
                             // NSLog(@"%@", string);
                              NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:[string dataUsingEncoding:NSUTF8StringEncoding]
                                                                                    options:0 error:NULL];
                              
                              
                              NSMutableArray *getdata=[[NSMutableArray alloc]init];
                              getdata=[jsonObject objectForKey:@"result"];
                              
                              NSLog(@"output %@ ",[getdata valueForKey:@"accessToken"] );
                              [loginData addObject:[getdata valueForKey:@"encryptedAccessToken"]];
                              [loginData addObject:[getdata valueForKey:@"encryptedAccessToken"]];
                             
                              [calledBy performSelector:successCallback withObject:result];
                          }
                      }];
}
- (void)loginDidEnd:(id)result{
    NSLog(@"loginDidEnd:");
    // Do your actions
}

- (void)loginFailure:(id)result{
    NSLog(@"loginFailure:");
    // Do your actions
}
@end
