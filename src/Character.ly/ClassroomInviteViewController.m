//
//  ClassroomInviteViewController.m
//  Character.ly
//
//  Created by Macbook Pro on 11/09/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "ClassroomInviteViewController.h"
#import "NavViewController.h"
#import "MessageWindow.h"
#import "DGActivityIndicatorView.h"
#import "DropDownCell.h"
#import "TNRadioButtonGroup.h"

@interface ClassroomInviteViewController (){
    
    NSMutableArray *_pickerData;
    UIView *dimView;
    UIActivityIndicatorView *spinner;
    DGActivityIndicatorView *activityIndicatorView;
    NSString *classID, *classCode, *typeString;
}
@property (assign, nonatomic) BOOL isEducator;
@property (strong, nonatomic) NSMutableArray *classrooms;
@property (nonatomic, strong) TNRadioButtonGroup *selectButtonGroup;

@property (weak, nonatomic) IBOutlet UIButton *classCodeButton;
@property (weak, nonatomic) IBOutlet UILabel *educatorNameLabel;
@property (weak, nonatomic) IBOutlet UIView *selectGroupView;

@end

@implementation ClassroomInviteViewController
- (IBAction)addClassTap:(id)sender {
    [self performSegueWithIdentifier:@"addclass" sender:self];
}
- (IBAction)addStudentTap:(id)sender {
    [self performSegueWithIdentifier:@"addstudent" sender:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *name = concat([[UserCache sharedInstance] getUserInfo][@"name"], @" ");
    name = concat(name, [[UserCache sharedInstance] getUserInfo][@"surname"]);
    self.educatorNameLabel.text = name;
    
    [self.classCodeButton setTitle:self.data[@"classCode"] forState:UIControlStateNormal];
    
    self.selectClassTable.dataSource = self;
    self.selectClassTable.delegate = self;
    self.selectClassTable.hidden = YES;
    
    [self.selectClassButton.layer setBorderWidth: 0.5];
    [self.selectClassButton.layer setBorderColor:[[UIColor grayColor] CGColor]];
    
    self.navigationController.navigationBar.hidden = YES;
    
    NavViewController *myViewController2 = [[NavViewController alloc] initForAutoLayout];
    [self.view addSubview:myViewController2];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:[UIApplication sharedApplication].statusBarFrame.size.height];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
    [myViewController2 autoSetDimension:ALDimensionHeight toSize:200];
    myViewController2.parent = self;
    
    _btnAddClass.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _btnAddClass.layer.borderWidth = 1.0f;
    
    _pickerData = [[NSMutableArray alloc] init];
    _txtClass.delegate = self;
    
    self.isEducator = ![[[UserCache sharedInstance] getUserInfo][@"educator"] isKindOfClass:[NSNull class]];
    
    if (self.isEducator) {
        self.classrooms = [[UserCache sharedInstance] getUserInfo][@"educator"][@"classrooms"];
    } else {
        self.classrooms = [[UserCache sharedInstance] getUserInfo][@"student"][@"classrooms"];
    }
    
    for(int i = 0; i < self.classrooms.count;i++){
        [_pickerData addObject:[self.classrooms[i] valueForKey:@"name"]];
    }
    
    _txtClass.text =[_pickerData objectAtIndex:0];
    
    UIView *padView = [[UIView alloc] initWithFrame:CGRectMake(10, 110, 10, 0)];
    _txtClass.leftView = padView;
    _txtClass.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *padView1 = [[UIView alloc] initWithFrame:CGRectMake(10, 110, 10, 0)];
    _txtEmail.leftView = padView1;
    _txtEmail.leftViewMode = UITextFieldViewModeAlways;
    
    [self setTypeGroup];
    
    typeString = @"";
}

- (void)setTypeGroup {
    
    TNCircularRadioButtonData *parentData = [TNCircularRadioButtonData new];
    parentData.labelText = @"Parent";
    parentData.identifier = @"parent";
    parentData.selected = NO;
    parentData.labelFont = [UIFont boldSystemFontOfSize:18.0];
    parentData.borderRadius = 20;
    parentData.circleRadius = 10;
    parentData.circleLineThickness = 3;
    parentData.circleColor = [UIColor colorWithRed:95.0f/255.0f green:96.0f/255.0f blue:171.0f/255.0f alpha:1.0f];
    parentData.borderColor = [UIColor colorWithRed:95.0f/255.0f green:96.0f/255.0f blue:171.0f/255.0f alpha:1.0f];
    
    TNCircularRadioButtonData *studentData = [TNCircularRadioButtonData new];
    studentData.labelText = @"Student";
    studentData.labelFont = [UIFont boldSystemFontOfSize:18.0];
    studentData.identifier = @"student";
    studentData.selected = NO;
    studentData.borderRadius = 20;
    studentData.circleRadius = 10;
    studentData.circleLineThickness = 3;
    studentData.circleColor = [UIColor colorWithRed:95.0f/255.0f green:96.0f/255.0f blue:171.0f/255.0f alpha:1.0f];
    studentData.borderColor = [UIColor colorWithRed:95.0f/255.0f green:96.0f/255.0f blue:171.0f/255.0f alpha:1.0f];
    
    TNCircularRadioButtonData *otherData = [TNCircularRadioButtonData new];
    otherData.labelText = @"Other";
    otherData.identifier = @"other";
    otherData.selected = NO;
    otherData.labelFont = [UIFont boldSystemFontOfSize:18.0];
    otherData.borderRadius = 20;
    otherData.circleRadius = 10;
    otherData.circleLineThickness = 3;
    otherData.circleColor = [UIColor colorWithRed:95.0f/255.0f green:96.0f/255.0f blue:171.0f/255.0f alpha:1.0f];
    otherData.borderColor = [UIColor colorWithRed:95.0f/255.0f green:96.0f/255.0f blue:171.0f/255.0f alpha:1.0f];
    
    self.selectButtonGroup = [[TNRadioButtonGroup alloc] initWithRadioButtonData:@[parentData, studentData, otherData] layout:TNRadioButtonGroupLayoutVertical];
    self.selectButtonGroup.identifier = @"selectButtonGroup";
    [self.selectButtonGroup create];
    self.selectButtonGroup.position = CGPointMake(0, 0);
    
    [self.selectGroupView addSubview:self.selectButtonGroup];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectGroupUpdated:) name:SELECTED_RADIO_BUTTON_CHANGED object:self.selectButtonGroup];
    
    [self.selectButtonGroup update];
}

- (void)selectGroupUpdated:(NSNotification *)notification {
    typeString = self.selectButtonGroup.selectedRadioButton.data.identifier;
}

- (IBAction)actionSelectClassButton:(id)sender {
    
    if(self.selectClassTable.hidden == YES){
        
        [UIView transitionWithView: self.selectClassTable
                          duration: 0.35f
                           options: UIViewAnimationOptionTransitionCurlDown
                        animations: ^(void)
         {
             self.selectClassTable.hidden = NO;
         }
                        completion: nil];
        
    }else{
        
        [UIView transitionWithView: self.selectClassTable
                          duration: 0.35f
                           options: UIViewAnimationOptionTransitionCurlUp
                        animations: ^(void)
         {
             self.selectClassTable.hidden = YES;
         }
                        completion: nil];
    }
}

- (IBAction)selectClassCell:(UIButton *)sender {
    
    self.txtClass.text = _pickerData[sender.tag];
    classID = self.classrooms[sender.tag][@"id"];
    classCode = self.classrooms[sender.tag][@"classCode"];
    
    [UIView transitionWithView: self.selectClassTable
                      duration: 0.35f
                       options: UIViewAnimationOptionTransitionCurlUp
                    animations: ^(void)
     {
         self.selectClassTable.hidden = YES;
     }
                    completion: nil];
    
    sender.backgroundColor = [UIColor whiteColor];
    [sender setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
}

- (IBAction)changeBackgroundColor:(UIButton *)sender {
    
    sender.backgroundColor = [UIColor colorWithRed:95.0f/255.0f green:96.0f/255.0f blue:171.0f/255.0f alpha:1.0f];
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(tableView == _selectClassTable){
        return _pickerData.count;
    }else{
        return 0;
    }
}

- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *) cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView == _selectClassTable){
        
    }else{
        
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView == _selectClassTable){
        return 40;
    }else{
        return 0;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView == _selectClassTable){
        DropDownCell *cell = [tableView dequeueReusableCellWithIdentifier: @"classCell"];
        cell.dropDownButton.tag = indexPath.row;
        [cell.dropDownButton setTitle:_pickerData[indexPath.row] forState:normal];
        [cell.dropDownText setFont:[UIFont systemFontOfSize: 17]];
        return cell;
    }else{
        return nil;
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [_txtEmail endEditing:YES];
    [_txtEmail resignFirstResponder];
    return false;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_txtEmail resignFirstResponder];
    return YES;
}
-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}
- (void)showLoader {
    if (!dimView) {
        dimView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
        dimView.backgroundColor = [UIColor blackColor];
        dimView.alpha = 0.7f;
        dimView.userInteractionEnabled = NO;
    }
    [self.view addSubview:dimView];
    CGRect newFrame = dimView.frame;
    
    newFrame.size.width = 1200;
    newFrame.size.height = 1500;
    [dimView setFrame:newFrame];
    [dimView setCenter:CGPointMake(187.0f, 333.0f)];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeLineScale tintColor:[UIColor whiteColor] size:40.0f];
    activityIndicatorView.center = self.view.center;
    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
}

- (void)hideLoader {
    [dimView removeFromSuperview];
    [activityIndicatorView stopAnimating];
}

- (IBAction)actionInvite:(id)sender {
    
    NSString *branchId = [[UserCache sharedInstance] getUserInfo][@"branchId"];
    NSString *type = @"";
    if ([typeString isEqualToString:@"parent"]) {
        type = @"4";
    } else if ([typeString isEqualToString:@"student"]) {
        type = @"3";
    } else if ([typeString isEqualToString:@"other"]) {
        type = @"0";
    }
    
    NSString *userId = [[UserCache sharedInstance] getUserInfo][@"id"];
    
    if (!classID && !IsEmpty(self.classrooms)) {
        classID = self.classrooms[0][@"id"];
        classCode = self.classrooms[0][@"classCode"];
    }
    
    NSString *typeId = classID;
    NSString *email = _txtEmail.text;;
    
    if([email isEqualToString:@""] || ![email containsString:@"@"]){
        [self showErrorMessage:@"Please enter valid email"];
        return;
    } else if([type isEqualToString:@""]){
        [self showErrorMessage:@"Please enter correct type"];
        return;
    }
    
    if([self.txtFirstName.text isEqualToString:@""]){
        [self showErrorMessage:@"Please enter first Name"];
        return;
    }
    if([self.txtLastName.text isEqualToString:@""]){
        [self showErrorMessage:@"Please enter last Name"];
        return;
    }
    
    NSDictionary *dataToSend = @{
                                 @"BranchId" : branchId,
                                 @"Type" : type,
                                 @"TypeId" : typeId,
                                 @"UserId" : userId,
                                 @"Name" : self.txtFirstName.text,
                                 @"Surname" : self.txtLastName.text,
                                 @"EmailAddress" : email,
                                 @"Code" : classCode
                                 };
    
    if (IsEmpty(typeId)) {
        [self showErrorMessage:@"No classrooms to add student too"];
    } else {
        [self showLoader];
        [NetworkHelper placePostRequest:dataToSend relativeUrl:@"services/app/Invite/Create" withSuccess:^(NSDictionary *jsonObject) {
            [self hideLoader];
            [self showErrorMessage:@"Successfully Invited Student"];
            [self.navigationController popViewControllerAnimated:YES];
            
        } andFailure:^(NSString *string) {
            [self hideLoader];
            NSLog(@"loginFailure:");
            [self showErrorMessage:string];
        }];
    }
    
}

- (void)showErrorMessage:(NSString *)string {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Character.ly"
                                                    message:string
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (IBAction)txtEmailDismiss:(id)sender {
    [_txtEmail resignFirstResponder];
}
@end
