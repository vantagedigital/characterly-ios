//
//  NetworkHelper.h
//  Character.ly
//
//  Created by Jesse A on 10/11/17.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkHelper : NSObject

+ (void)placePostRequest:(NSDictionary *)data
             relativeUrl:(NSString *)relativeUrl
             withSuccess:(CompletionJsonBlock)successCallback
              andFailure:(CompletionStringBlock)failureCallback;

+ (void)placePutRequest:(NSDictionary *)data
            relativeUrl:(NSString *)relativeUrl
            withSuccess:(CompletionJsonBlock)successCallback
             andFailure:(CompletionStringBlock)failureCallback;

+ (void)placeGetRequest:(NSDictionary *)data
            relativeUrl:(NSString *)relativeUrl
            withSuccess:(CompletionJsonBlock)successCallback
             andFailure:(CompletionStringBlock)failureCallback;

+ (void)placeDeleteRequest:(NSDictionary *)data
               relativeUrl:(NSString *)relativeUrl
               withSuccess:(CompletionJsonBlock)successCallback
                andFailure:(CompletionStringBlock)failureCallback;

+ (void)placeUploadImageRequest:(UIImage *)image mimeType:(NSString *)mimeType category:(NSString *)category entityId:(NSString *)entityId withSuccess:(CompletionJsonBlock)successCallback
                    andFailure:(CompletionStringBlock)failureCallback;

+ (void)placeUploadFileRequest:(NSString *)path mimeType:(NSString *)mimeType category:(NSString *)category entityId:(NSString *)entityId withSuccess:(CompletionJsonBlock)successCallback
                    andFailure:(CompletionStringBlock)failureCallback;

@end
