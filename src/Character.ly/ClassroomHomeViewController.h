//
//  ClassroomHomeViewController.h
//  Character.ly
//
//  Created by Adeel Nasir on 08/09/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClassroomHomeViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIButton *btnStudent;
- (IBAction)actionStudent:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnEvalutions;
- (IBAction)actionEcaluations:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UITextField *sortField;
@property (strong, nonatomic) IBOutlet UIScrollView *scroller;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIButton *selectSortButton;
@property (weak, nonatomic) IBOutlet UITableView *sortTable;

@property (strong, nonatomic) NSDictionary *data;

@end
