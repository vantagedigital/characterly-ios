//
//  Evaluation4ViewController.m
//  Character.ly
//
//  Created by Adeel Nasir on 16/08/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "Evaluation4ViewController.h"
#import "NavViewController.h"

@interface Evaluation4ViewController ()

@end

@implementation Evaluation4ViewController
@synthesize check1,check2,check3;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    NavViewController *myViewController2 = [[NavViewController alloc] init];
    
    //  [myViewController2.view addSubView:mySubView];
    // add any other views to myViewController2's view
    
    [self.view addSubview:myViewController2];
    
    myViewController2.frame = self.navigationController.navigationBar.frame;
    myViewController2.frame = CGRectMake(myViewController2.frame.origin.x, [UIApplication sharedApplication].statusBarFrame.size.height, myViewController2.frame.size.width, myViewController2.frame.size.height);
    // Do any additional setup after loading the view.
    [_scroller setScrollEnabled:YES];
    [_scroller setContentSize:CGSizeMake(375, 1400)];
    checked1 = YES;
    checked2 = NO;
    checked3 = NO;
    
    for(BEMCheckBox *checkBox in self.checkBoxGroup){
        
        checkBox.on = NO;
        checkBox.boxType = BEMBoxTypeSquare;
        checkBox.onAnimationType = BEMAnimationTypeBounce;
        checkBox.offAnimationType = BEMAnimationTypeBounce;
        checkBox.lineWidth = 2;
        checkBox.tintColor = [UIColor colorWithRed:95.0f/255.0f green:96.0f/255.0f blue:171.0f/255.0f alpha:1.0f];
        checkBox.onTintColor = [UIColor whiteColor];
        checkBox.onFillColor = [UIColor colorWithRed:95.0f/255.0f green:96.0f/255.0f blue:171.0f/255.0f alpha:1.0f];
        checkBox.onCheckColor = [UIColor whiteColor];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)checkAction:(BEMCheckBox *)sender {
    
    if(sender.tag == 1){
        
    }
    
    if(sender.tag == 2){
        
    }
    
    if(sender.tag == 3){
        
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
