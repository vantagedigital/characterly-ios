//
//  EditStudentViewController.h
//  Character.ly
//
//  Created by Adeel Nasir on 14/08/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditStudentViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIScrollView *scroller;
@property (strong, nonatomic) IBOutlet UITextField *txtStudent;

@property (strong, nonatomic) IBOutlet UITextField *txtGrade;
@property (strong, nonatomic) IBOutlet UITextField *txtName;
@property (strong, nonatomic) IBOutlet UITextField *txtMName;
@property (strong, nonatomic) IBOutlet UITextField *txtLName;
@property (strong, nonatomic) IBOutlet UITextField *txtPhone;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UIButton *choosePic;
@property (strong, nonatomic) NSDictionary *studentData;
@end
