//
//  ClassTimelineViewController.m
//  Character.ly
//
//  Created by Adeel Nasir on 07/09/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "ClassTimelineViewController.h"
#import "NavViewController.h"

@interface ClassTimelineViewController (){
    CGFloat customTableCellHeight;
    int checkThird;
    int checkHeight;
}

@end

@implementation ClassTimelineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    checkThird = 1;
    checkHeight = 1;
    self.navigationController.navigationBar.hidden = YES;
    
    NavViewController *myViewController2 = [[NavViewController alloc] initForAutoLayout];
    [self.view addSubview:myViewController2];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:[UIApplication sharedApplication].statusBarFrame.size.height];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
    [myViewController2 autoSetDimension:ALDimensionHeight toSize:200];
    myViewController2.parent = self;
    
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.notesButton addGestureRecognizer:singleFingerTap];
    
//    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 1300, 0);
    self.tableView.separatorColor = [UIColor clearColor];
    _messageTextView.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _messageTextView.layer.borderWidth = 0.5f;
    
    _messageTextView.backgroundColor = [self colorWithHexString:@"fcfcfc"];
    
    _notesButton.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _notesButton.layer.borderWidth = 0.5f;
    
    _photosButton.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _photosButton.layer.borderWidth = 0.5f;
    
     
    
    _audioButton.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _audioButton.layer.borderWidth = 0.5f;
    
    
    _videoButton.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _videoButton.layer.borderWidth = 0.5f;
    
    _addPhoto.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _addPhoto.layer.borderWidth = 0.5f;
    
    _addFile.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _addFile.layer.borderWidth = 0.5f;
    
     [self.tableView setSeparatorColor:[UIColor clearColor]];
    
    _tableView.layer.borderColor = [self colorWithHexString:@"ffffff"].CGColor;
    _tableView.layer.borderWidth = 0.5f;
    
    [_tableView setShowsHorizontalScrollIndicator:NO];
    [_tableView setShowsVerticalScrollIndicator:NO];
    
    _tableView.allowsSelection = NO;

    self.classroomNameLabel.text = self.data[@"name"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    
    //Do stuff here...
    NSLog(@"Notes");
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger row = indexPath.row;
    for (int i = 0; i < indexPath.section; i++) {
        row += [tableView numberOfRowsInSection:i];
    }
    BOOL isOdd = row % 2;
    if(indexPath.row == 0){
        return 50;
    }
    else if(isOdd==0){
       // [tableView setRowHeight:230.0];
        return 240;
    }
    else{
        return 460;
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;
    UITableViewCell *cell;
    
    if(indexPath.row == 0){
        CellIdentifier = @"Cell2";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        UILabel *recipeNameButton3 = (UILabel *)[cell viewWithTag:200];
        recipeNameButton3.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
        recipeNameButton3.layer.borderWidth = 0.5f;
    }
    
    else if (indexPath.row % 2 == 0)
    {
        NSLog(@"%i ",checkThird);
        CellIdentifier = @"Cell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        NSLog(@"%f",customTableCellHeight);
   
    }
    else
    {
        CellIdentifier = @"Cell1";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        NSLog(@"%f",customTableCellHeight);
        
    }
   

    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }
//    UIView *likeViewButton = (UIView *)[cell viewWithTag:700];
//    UIView *commentViewButton = (UIView *)[cell viewWithTag:701];
//
//    likeViewButton.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
//    likeViewButton.layer.borderWidth = 0.5f;
//
//    UIView *likeViewButton2 = (UIView *)[cell viewWithTag:703];
//    UIView *commentViewButton2 = (UIView *)[cell viewWithTag:704];
//
//    likeViewButton2.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
//    likeViewButton2.layer.borderWidth = 0.5f;
//
//    commentViewButton.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
//    commentViewButton.layer.borderWidth = 0.5f;
//
//    commentViewButton2.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
//    commentViewButton2.layer.borderWidth = 0.5f;
   
    
   
    if([cell.reuseIdentifier isEqualToString:@"Cell"]){
      
    }
    else {
       
    }
    
   
   // cell.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
   // cell.layer.borderWidth = 1.0f;
    
    return cell;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
