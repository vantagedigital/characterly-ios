//
//  EducatorProfileViewController.h
//  Character.ly
//
//  Created by Macbook Pro on 10/08/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EducatorProfileViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>{

}

@property (strong, nonatomic) IBOutlet UIScrollView *scroller;
@property (strong, nonatomic) IBOutlet UITextField *txtMiddleName;
@property (strong, nonatomic) IBOutlet UITableView *tblNotifications;
- (IBAction)openProfile:(id)sender;
- (IBAction)openNotifications:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnProfile;
@property (strong, nonatomic) IBOutlet UIButton *btnNotifications;
@property (strong, nonatomic) IBOutlet UITextField *txtUsername;
@property (strong, nonatomic) IBOutlet UITextField *txtName;
@property (strong, nonatomic) IBOutlet UITextField *txtLastNAme;
@property (strong, nonatomic) IBOutlet UIView *schoolView;


@end
