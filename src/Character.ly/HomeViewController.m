//
//  HomeViewController.m
//  Character.ly
//
//  Created by Adeel Nasir on 10/08/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "HomeViewController.h"
#import "NavViewController.h"
#import "ContentType1Cell.h"
#import "LessonDetailViewController.h"
#import "ManageClassViewController.h"

@interface HomeViewController (){
    
    //calendar parameters
    NSMutableDictionary *_eventsByDate;
    
    NSDate *_todayDate;
    NSDate *_minDate;
    NSDate *_maxDate;
    
    NSDate *_dateSelected;
}

@property (nonatomic, strong) NSArray *contentData;
@property (nonatomic, strong) NSMutableDictionary *selectedData;
@property (weak, nonatomic) IBOutlet UILabel *noContentLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITableView *subTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lessonTableHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mainViewHeightConstraint;

@end

@implementation HomeViewController
#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]


- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    NavViewController *myViewController2 = [[NavViewController alloc] initForAutoLayout];
    
    //  [myViewController2.view addSubView:mySubView];
    // add any other views to myViewController2's view
    
    [self.view addSubview:myViewController2];
    
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:[UIApplication sharedApplication].statusBarFrame.size.height];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
    [myViewController2 autoSetDimension:ALDimensionHeight toSize:200];
    myViewController2.parent = self;
    
    [myViewController2 hideBackButton];
    
    UIImage *image = [UIImage imageNamed:@"characterly.png"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:image];
    [self.topView setBackgroundColor:RGB(240, 85, 61)];
    _btnCloseMenu.hidden = YES;
    
    _menuLessons.hidden = YES;
    _menuClassrooms.hidden = YES;
    _menuEvalucations.hidden = YES;
    
    NSDictionary *userInfo = [[UserCache sharedInstance] getUserInfo];
    NSLog(@"user info: %@", userInfo);
    self.contentData = [[UserCache sharedInstance] getContentInfo][@"result"];
    NSLog(@"%li", self.contentData.count);
    
    if (self.contentData.count == 0) {
        self.noContentLabel.hidden = NO;
        self.tableView.hidden = YES;
    }
    
    self.lessonTableHeightConstraint.constant = 50 + self.contentData.count * 400;
    if(self.contentData.count < 2){
        self.mainViewHeightConstraint.constant = self.view.frame.size.height;
    }else{
        self.mainViewHeightConstraint.constant = self.lessonTableHeightConstraint.constant + 50;
    }
    
    //set calendar
    _calendarManager = [JTCalendarManager new];
    _calendarManager.delegate = self;
    
    // Generate random events sort by date using a dateformatter for the demonstration
//    [self createRandomEvents];
    
    // Create a min and max date for limit the calendar, optional
    [self createMinAndMaxDate];
    
    [_calendarManager setMenuView:_calendarMenuView];
    [_calendarManager setContentView:_calendarContentView];
    [_calendarManager setDate:_todayDate];
    
    _calendarManager.settings.weekModeEnabled = NO;
    self.calendarContentViewHeight.constant = 85;
    [_calendarButton setTitle:@"Expand" forState:UIControlStateNormal];
    
    _calendarManager.settings.weekModeEnabled = !_calendarManager.settings.weekModeEnabled;
    [_calendarManager reload];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 400;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.contentData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContentType1Cell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ContentType1Cell class])];
    if (!cell) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([ContentType1Cell class]) owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    NSDictionary *data = [NSMutableDictionary dictionary];
    @try {
        data = self.contentData[indexPath.row];
    } @catch (NSException *exception) {
    }

    cell.title.text = data[@"contentSchedule"][@"content"][@"title"];
    @try {
        cell.subjectLabel.text = ((NSArray *)data[@"contentSchedule"][@"content"][@"subjects"])[0][@"name"];
    } @catch (NSException *exception) {}
    @try {
        cell.gradeLabel.text = ((NSArray *)data[@"contentSchedule"][@"content"][@"grades"])[0][@"name"];
    } @catch (NSException *exception) {}
    
    @try {
        NSArray<NSDictionary *> *attachments = data[@"contentSchedule"][@"content"][@"fileAttachments"] == nil ? data[@"contentSchedule"][@"content"][@"attachments"] : data[@"contentSchedule"][@"content"][@"fileAttachments"];
        NSString *imageUrl = attachments[attachments.count - 1][@"path"];
        [cell.imageview sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
    } @catch (NSException *exception) {}
    

    cell.pillar1.hidden = YES;
    cell.pillar1.layer.masksToBounds = true;
    cell.pillar1.layer.cornerRadius = cell.pillar1.frame.size.width / 2;
    cell.pillar2.hidden = YES;
    cell.pillar2.layer.masksToBounds = true;
    cell.pillar2.layer.cornerRadius = cell.pillar2.frame.size.width / 2;
    cell.pillar3.hidden = YES;
    cell.pillar3.layer.masksToBounds = true;
    cell.pillar3.layer.cornerRadius = cell.pillar3.frame.size.width / 2;
    cell.pillar4.hidden = YES;
    cell.pillar4.layer.masksToBounds = true;
    cell.pillar4.layer.cornerRadius = cell.pillar4.frame.size.width / 2;
    cell.pillar5.hidden = YES;
    cell.pillar5.layer.masksToBounds = true;
    cell.pillar5.layer.cornerRadius = cell.pillar5.frame.size.width / 2;
    cell.pillar6.hidden = YES;
    cell.pillar6.layer.masksToBounds = true;
    cell.pillar6.layer.cornerRadius = cell.pillar6.frame.size.width / 2;
    cell.pillar7.hidden = YES;
    cell.pillar7.layer.masksToBounds = true;
    cell.pillar7.layer.cornerRadius = cell.pillar7.frame.size.width / 2;
    cell.pillar8.hidden = YES;
    cell.pillar8.layer.masksToBounds = true;
    cell.pillar8.layer.cornerRadius = cell.pillar8.frame.size.width / 2;
    cell.pillar9.hidden = YES;
    cell.pillar9.layer.masksToBounds = true;
    cell.pillar9.layer.cornerRadius = cell.pillar9.frame.size.width / 2;
    
    NSArray *pillars = ((NSArray *)data[@"contentSchedule"][@"content"][@"pillars"]);
    for (int i = 0; i < pillars.count; i++) {
        NSDictionary *pillar = pillars[i];
        
        UIButton *pillarView = [self getPillarForRow:cell i:i];
        pillarView.hidden = NO;
        pillarView.backgroundColor = [self colorFromHexString:pillar[@"colorCode"]];
    }
    return cell;
}

// Assumes input like "#00FF00" (#RRGGBB).
- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

- (UIButton *)getPillarForRow:(ContentType1Cell *)cell i:(NSInteger)i {
    if (i == 0) {
        return cell.pillar1;
    } else if (i == 1) {
        return cell.pillar2;
    } else if (i == 2) {
        return cell.pillar3;
    } else if (i == 3) {
        return cell.pillar4;
    } else if(i == 4){
        return cell.pillar5;
    }else if(i == 5){
        return cell.pillar6;
    }else if(i == 6){
        return cell.pillar7;
    }else if(i == 7){
        return cell.pillar8;
    }else{
        return cell.pillar9;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = [indexPath row];
    
    
    if(tableView == _tableView){
        
    }
    if(tableView == _subTableView){
        
        NSLog(@"This is content data: %@", self.contentData[row]);
        self.selectedData = self.contentData[row];
        [self performSegueWithIdentifier:@"lessonDetail" sender:self];
    }
   
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"ld"])
    {
        
    }
    if ([[segue identifier] isEqualToString:@"content"]){
        
    }
    if([[segue identifier] isEqualToString:@"classrooms"]){
        
    }
    if([[segue identifier] isEqualToString:@"lessonDetail"]){
        LessonDetailViewController *vc = [segue destinationViewController];
        vc.data = self.selectedData;
    }
}

- (IBAction)openMenu:(id)sender {
    NSLog(@"Menu open");
    _menuView.hidden = NO;
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationCurveEaseInOut
                     animations:^ {
                        // NSLog(@"yourview : %@",_menuView);
                         
                         _menuView.frame = CGRectMake(self.menuView.frame.origin.x, self.menuView.frame.origin.y, self.menuView.frame.size.width, 155);
                       //  cgre
                        // NSLog(@"yourview : %@",_menuView);
                         
                     }completion:^(BOOL finished) {
                         _btnCloseMenu.hidden = NO;
                         _btnMenu.hidden = YES;
                         _menuLessons.hidden = NO;
                         _menuClassrooms.hidden = NO;
                         _menuEvalucations.hidden = NO;
                     }];
  
}
- (IBAction)closeMenu:(id)sender {
    _menuLessons.hidden = YES;
    _menuClassrooms.hidden = YES;
    _menuEvalucations.hidden = YES;
    [UIView animateWithDuration:1
                          delay:0
                        options:UIViewAnimationCurveEaseInOut
                     animations:^ {
                         // NSLog(@"yourview : %@",_menuView);
                         
                         _menuView.frame = CGRectMake(self.menuView.frame.origin.x, self.menuView.frame.origin.y, self.menuView.frame.size.width, 0);
                         //  cgre
                         // NSLog(@"yourview : %@",_menuView);
                         
                     }completion:^(BOOL finished) {
                         _btnCloseMenu.hidden = YES;
                         _btnMenu.hidden = NO;
                         
                         
                         
                     }];
}

- (IBAction)didGoTodayTouch
{
    [_calendarManager setDate:_todayDate];
}

- (IBAction)didChangeModeTouch
{
    _calendarManager.settings.weekModeEnabled = !_calendarManager.settings.weekModeEnabled;
    [_calendarManager reload];
    
    [_calendarButton setTitle:@"Close" forState:UIControlStateNormal];
    
    CGFloat newHeight = 300;
    if(_calendarManager.settings.weekModeEnabled){
        newHeight = 85.;
        [_calendarButton setTitle:@"Expand" forState:UIControlStateNormal];
    }
    
    self.calendarContentViewHeight.constant = newHeight;
    [self.view layoutIfNeeded];
}

#pragma mark - CalendarManager delegate

// Exemple of implementation of prepareDayView method
// Used to customize the appearance of dayView
- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    // Today
    if([_calendarManager.dateHelper date:[NSDate date] isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor colorWithRed:95.0f/255.0f green:96.0f/255.0f blue:171.0f/255.0f alpha:1.0f];
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Selected date
    else if(_dateSelected && [_calendarManager.dateHelper date:_dateSelected isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor redColor];
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Other month
    else if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]){
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = [UIColor redColor];
        dayView.textLabel.textColor = [UIColor lightGrayColor];
    }
    // Another day of the current month
    else{
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = [UIColor redColor];
        dayView.textLabel.textColor = [UIColor blackColor];
    }
    
    if([self haveEventForDay:dayView.date]){
        dayView.dotView.hidden = NO;
    }
    else{
        dayView.dotView.hidden = YES;
    }
}

- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
    _dateSelected = dayView.date;
    
    // Animation for the circleView
    dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
    [UIView transitionWithView:dayView
                      duration:.3
                       options:0
                    animations:^{
                        dayView.circleView.transform = CGAffineTransformIdentity;
                        [_calendarManager reload];
                    } completion:nil];
    
    
    // Don't change page in week mode because block the selection of days in first and last weeks of the month
    if(_calendarManager.settings.weekModeEnabled){
        return;
    }
    
    // Load the previous or next page if touch a day from another month
    
    if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]){
        if([_calendarContentView.date compare:dayView.date] == NSOrderedAscending){
            [_calendarContentView loadNextPageWithAnimation];
        }
        else{
            [_calendarContentView loadPreviousPageWithAnimation];
        }
    }
}

#pragma mark - CalendarManager delegate - Page mangement

// Used to limit the date for the calendar, optional
- (BOOL)calendar:(JTCalendarManager *)calendar canDisplayPageWithDate:(NSDate *)date
{
    return [_calendarManager.dateHelper date:date isEqualOrAfter:_minDate andEqualOrBefore:_maxDate];
}

- (void)calendarDidLoadNextPage:(JTCalendarManager *)calendar
{
    //    NSLog(@"Next page loaded");
}

- (void)calendarDidLoadPreviousPage:(JTCalendarManager *)calendar
{
    //    NSLog(@"Previous page loaded");
}

#pragma mark - Fake data

- (void)createMinAndMaxDate
{
    _todayDate = [NSDate date];
    
    // Min date will be 2 month before today
    _minDate = [_calendarManager.dateHelper addToDate:_todayDate months:-2];
    
    // Max date will be 2 month after today
    _maxDate = [_calendarManager.dateHelper addToDate:_todayDate months:2];
}

// Used only to have a key for _eventsByDate
- (NSDateFormatter *)dateFormatter
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"dd-MM-yyyy";
    }
    
    return dateFormatter;
}

- (BOOL)haveEventForDay:(NSDate *)date
{
    NSString *key = [[self dateFormatter] stringFromDate:date];
    
    if(_eventsByDate[key] && [_eventsByDate[key] count] > 0){
        return YES;
    }
    
    return NO;
    
}

- (void)createRandomEvents
{
    _eventsByDate = [NSMutableDictionary new];
    
    for(int i = 0; i < 30; ++i){
        // Generate 30 random dates between now and 60 days later
        NSDate *randomDate = [NSDate dateWithTimeInterval:(rand() % (3600 * 24 * 60)) sinceDate:[NSDate date]];
        
        // Use the date as key for eventsByDate
        NSString *key = [[self dateFormatter] stringFromDate:randomDate];
        
        if(!_eventsByDate[key]){
            _eventsByDate[key] = [NSMutableArray new];
        }
        
        [_eventsByDate[key] addObject:randomDate];
    }
}



@end
