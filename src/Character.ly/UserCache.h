//
//  UserCache.h
//


@interface UserCache : NSObject

extern NSString *const kPrefsName;

extern NSString *const KEY_ACCESS_TOKEN;
extern NSString *const KEY_ENCRYPTED_ACCESS_TOKEN;
extern NSString *const KEY_USER_INFO;
extern NSString *const KEY_CONTENT_INFO;
extern NSString *const KEY_USER_ID;

@property (assign, nonatomic) BOOL isNotifications;

+ (UserCache *) sharedInstance;

- (NSMutableDictionary *)generalPrefs;
- (void)saveGeneralPrefs:(NSMutableDictionary *)dic;

- (BOOL)isLoggedIn;

- (NSString *)getAccessToken;

- (void)setAccessToken:(NSString *)value;

- (NSString *)getEncryptedAccessToken;

- (void)setEncryptedAccessToken:(NSString *)value;

- (NSString *)getUserId;

- (void)setUserId:(NSString *)value;

- (NSDictionary *)getUserInfo;

- (void)setCurrentUser:(NSDictionary *)value;

- (NSDictionary *)getContentInfo;

- (void)setContentInfo:(NSDictionary *)value;

- (void)clear;

@end
