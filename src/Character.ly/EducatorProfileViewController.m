//
//  EducatorProfileViewController.m
//  Character.ly
//
//  Created by Macbook Pro on 10/08/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import "EducatorProfileViewController.h"
#import "NavViewController.h"
#import "DGActivityIndicatorView.h"


@interface EducatorProfileViewController (){
    UIView *dimView;
    DGActivityIndicatorView *activityIndicatorView;
}
@property (weak, nonatomic) IBOutlet UILabel *educatorName;
@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *firstname;
@property (weak, nonatomic) IBOutlet UITextField *lastName;
@property (weak, nonatomic) IBOutlet UIView *profileView;
@property (weak, nonatomic) IBOutlet UILabel *schoolName;
@property (weak, nonatomic) IBOutlet UITextView *schoolInfo;
@property (weak, nonatomic) IBOutlet UIImageView *educatorImageView;
@property (weak, nonatomic) IBOutlet UILabel *notificationCountLabel;
@property (strong, nonatomic) NSDictionary *educatorInfo;
@property (strong, nonatomic) NSMutableArray <NSDictionary *> *notificationList;

@end

@implementation EducatorProfileViewController
#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view
    self.navigationController.navigationBar.hidden = YES;
    
    NavViewController *myViewController2 = [[NavViewController alloc] initForAutoLayout];
    [self.view addSubview:myViewController2];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeTop withInset:[UIApplication sharedApplication].statusBarFrame.size.height];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeLeft withInset:0];
    [myViewController2 autoPinEdgeToSuperviewEdge:ALEdgeRight withInset:0];
    [myViewController2 autoSetDimension:ALDimensionHeight toSize:200];
    myViewController2.parent = self;
    
    self.notificationList = [NSMutableArray array];
    
    self.educatorInfo = [[UserCache sharedInstance] getUserInfo];
    NSString *firstName = self.educatorInfo[@"name"];
    firstName = concat(firstName, @" ");
    NSString *lastName = self.educatorInfo[@"surname"];
    NSString *username = self.educatorInfo[@"userName"];
    NSString *notificationCount = self.educatorInfo[@"notificationsCount"];
    NSLog(@"%@", notificationCount);
    self.educatorName.text = concat(firstName, lastName);
    self.username.text = username;
    self.firstname.text = firstName;
    self.lastName.text = lastName;
    self.educatorImageView.layer.cornerRadius = self.educatorImageView.bounds.size.width / 2;
    if(![self.educatorInfo[@"educator"][@"school"] isKindOfClass:[NSNull class]]){
        
    }
    if(![self.educatorInfo[@"imagePath"] isKindOfClass:[NSNull class]]){
        [self.educatorImageView sd_setImageWithURL:[NSURL URLWithString: self.educatorInfo[@"imagePath"]]];
    }
    NSLog(@"%@", self.educatorInfo[@"educator"][@"id"]);
    
    
    [_scroller setScrollEnabled:YES];
    [_scroller setContentSize:CGSizeMake(375, 1730)];
    [self.tblNotifications setSeparatorColor:[UIColor whiteColor]];
    
    [_tblNotifications setShowsHorizontalScrollIndicator:NO];
    [_tblNotifications setShowsVerticalScrollIndicator:NO];
    
    UIImage *image = [UIImage imageNamed:@"characterly.png"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:image];
    _tblNotifications.hidden = YES;
     
    [self.tblNotifications setSeparatorColor:[UIColor clearColor]];
    
    _txtUsername.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtUsername.layer.borderWidth = 1.0f;
    
    _txtName.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtName.layer.borderWidth = 1.0f;
    
    _txtMiddleName.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtMiddleName.layer.borderWidth = 1.0f;
    
    _txtLastNAme.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _txtLastNAme.layer.borderWidth = 1.0f;
    
    _schoolView.layer.borderColor = [self colorWithHexString:@"e6e6e6"].CGColor;
    _schoolView.layer.borderWidth = 1.0f;
    
    if ([UserCache sharedInstance].isNotifications) {
        [self openNotifications:nil];
    }else{
        [self openProfile:nil];
    }
    [self.btnProfile addTarget:self action:@selector(openProfile) forControlEvents:UIControlEventTouchUpInside];
    
    [self getNotifications];
}

- (void)getNotifications {
    
    [self.notificationList removeAllObjects];
    
    NSString *rootURL = @"services/app/SystemNotification/GetByUserId?id=";
    NSString *url = concat(rootURL, self.educatorInfo[@"id"]);
    [self showLoader];

    [NetworkHelper placeGetRequest:nil relativeUrl: url withSuccess:^(NSDictionary *jsonObject) {
        [self hideLoader];
        [self showErrorMessage:@"Successfully Notifications have been received."];
        NSLog(@"Notifications data: %@", jsonObject);
        self.notificationList = jsonObject[@"result"];
        self.notificationCountLabel.text = [NSString stringWithFormat:@"%ld", (long)self.notificationList.count];
        [self.tblNotifications reloadData];
    } andFailure:^(NSString *string) {
        [self hideLoader];
        [self showErrorMessage:string];
    }];

}

- (IBAction)saveProfileAction:(id)sender {
    NSDictionary *dataToSend = @{
                                 @"branchId": self.educatorInfo[@"branchId"],
                                 @"emailAddress": self.educatorInfo[@"emailAddress"],
                                 @"userName": self.educatorInfo[@"userName"],
                                 @"name": self.firstname.text,
                                 @"surname": self.lastName.text,
                                 @"id": self.educatorInfo[@"educator"][@"id"]
                                 };
    [self showLoader];
    [NetworkHelper placePutRequest:dataToSend relativeUrl:@"services/app/Educator/Update" withSuccess:^(NSDictionary *jsonObject) {
        [self hideLoader];
        [self showErrorMessage:@"Successfully Educator profile saved."];
        NSLog(@"received profile data: %@", jsonObject[@"result"][@"classrooms"][0][@"educator"]);
        NSDictionary *profileData = jsonObject[@"result"][@"classrooms"][0][@"educator"];
        //update UserCache info
        NSMutableDictionary *userInfo = [[UserCache sharedInstance] getUserInfo].mutableCopy;
        [userInfo setObject:profileData[@"name"] forKey:@"name"];
        [userInfo setObject:profileData[@"surname"] forKey:@"surname"];
        [[UserCache sharedInstance] setCurrentUser:userInfo];
        
    } andFailure:^(NSString *string) {
        [self hideLoader];
        [self showErrorMessage:string];
    }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    return YES;
}

- (void)showLoader {
    if (!dimView) {
        dimView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
        dimView.backgroundColor = [UIColor blackColor];
        dimView.alpha = 0.7f;
        dimView.userInteractionEnabled = NO;
    }
    [self.view addSubview:dimView];
    CGRect newFrame = dimView.frame;
    
    newFrame.size.width = 1200;
    newFrame.size.height = 1500;
    [dimView setFrame:newFrame];
    [dimView setCenter:CGPointMake(187.0f, 333.0f)];
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeLineScale tintColor:[UIColor whiteColor] size:40.0f];
    activityIndicatorView.center = self.view.center;
    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
}

- (void)showErrorMessage:(NSString *)string {
    if (!string) {
        string = @"";
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Character.ly"
                                                    message:string
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void)hideLoader {
    [dimView removeFromSuperview];
    [activityIndicatorView stopAnimating];
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.notificationList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSDictionary *data = self.notificationList[indexPath.row];
    NSString *creationTimeString = data[@"creationTime"];
    cell.textLabel.text = data[@"title"];
    cell.backgroundColor = RGB(217, 217, 255);
    cell.textLabel.textColor = RGB(95, 96, 171);
    
    cell.detailTextLabel.textColor = RGB(107, 124, 147);
    cell.detailTextLabel.text = [self getTimeElapsedString:[self getNSDateFromString:creationTimeString]];
    
    UIFont *myFont = [UIFont fontWithName: @"Helvetica" size: 16.0 ];
    cell.textLabel.font  = myFont;
    cell.detailTextLabel.font = myFont;
    return cell;
    
}

- (NSDate *)getNSDateFromString:(NSString *)isoDateString {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    // Always use this locale when parsing fixed format date strings
    NSLocale *posix = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:posix];
    return [formatter dateFromString:isoDateString];
}

- (NSString *)getTimeElapsedString:(NSDate *)timeToCheck {
    NSDate *now = [NSDate date];
    long timeDiffMilliSecs = [now timeIntervalSince1970]
    - [timeToCheck timeIntervalSince1970];
    long timeDiffSecs = timeDiffMilliSecs / 1000;
    long timeDiffMins = timeDiffSecs / 60;
    long timeDiffHours = timeDiffMins / 60;
    long timeDiffDays = timeDiffHours / 24;
    long timeDiffMonths = timeDiffDays / 30;
    NSString *timeString = @"";
    if (timeDiffSecs <= 60) {
        timeString = [NSString stringWithFormat:@"%@s", @(timeDiffSecs)];;
    } else if (timeDiffMins <= 60) {
        timeString = [NSString stringWithFormat:@"%@m", @(timeDiffMins)];
    } else if (timeDiffHours <= 24) {
        timeString = [NSString stringWithFormat:@"%@h", @(timeDiffHours)];
    } else if (timeDiffDays <= 30) {
        timeString = [NSString stringWithFormat:@"%@d", @(timeDiffDays)];
    } else {
        timeString = [NSString stringWithFormat:@"%@mo", @(timeDiffMonths)];
    }
    
    return timeString;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)openProfile:(id)sender {
    self.profileView.hidden = NO;
    _tblNotifications.hidden = YES;
    _scroller.scrollEnabled = YES;
    [self.btnProfile setBackgroundColor:RGB(255, 255, 255)];
    [self.btnNotifications setBackgroundColor:RGB(96, 96, 171)];
    [self.btnNotifications setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //  [_btnProfile setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //   [_btnNotifications setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnProfile setTitleColor:RGB(96, 96, 171) forState:UIControlStateNormal];
    
}

- (void) openProfile {
    
}

- (IBAction)openNotifications:(id)sender {
    self.profileView.hidden = YES;
    _tblNotifications.hidden = NO;
    _scroller.scrollEnabled = NO;
    [self.btnNotifications setBackgroundColor:RGB(255, 255, 255)];
    [self.btnProfile setBackgroundColor:RGB(96, 96, 171)];
    [self.btnProfile setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnNotifications setTitleColor:RGB(96, 96, 171) forState:UIControlStateNormal];
    
  //  [_btnNotifications setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
   // [_btnProfile setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}
@end
