//
//  ClassTimelineViewController.h
//  Character.ly
//
//  Created by Adeel Nasir on 07/09/2017.
//  Copyright © 2017 Adeel Nasir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface ClassTimelineViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITextView *messageTextView;
@property (strong, nonatomic) IBOutlet UIView *notesButton;
@property (strong, nonatomic) IBOutlet UIView *photosButton;
@property (strong, nonatomic) IBOutlet UIView *audioButton;
@property (strong, nonatomic) IBOutlet UIView *videoButton;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *addPhoto;
@property (strong, nonatomic) IBOutlet UIView *addFile;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *classroomNameLabel;

@property (strong, nonatomic) NSDictionary *data;

@end
